package com.example.vanvy.myapplication.searchLocation;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.register.RegisterActivity_;
import com.example.vanvy.myapplication.searchLocation.location.AddressComponent;
import com.example.vanvy.myapplication.searchLocation.location.Result;
import com.example.vanvy.myapplication.searchLocation.location.ResultLocation;
import com.example.vanvy.myapplication.util.ApiUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/6/16.
 */
@EActivity(R.layout.activity_search_location)
public class SearchLocationActivity extends BaseActivity implements ResultLocationListener, Constants {
    @ViewById()
    EditText mEdtSearch;
    @ViewById()
    RecyclerView mRcvResultSearch;

    private List<Result> mResults = new ArrayList<>();
    private SearchAdapter mAdapter;

    @AfterViews()
    void afterViews() {

        setTitle("Search location", false, true,false);
        View view = findViewById(R.id.mHeaderBar);
        ImageView mIvLeft = (ImageView) view.findViewById(R.id.mIvLeft);
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Click(R.id.mBtnSearch)
    void onSearchClick() {
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.BASE_URL_SEARCH_LOCATION).build();
        ApiConnect mApi = restAdapter.create(ApiConnect.class);
        mApi.getLocation(mEdtSearch.getText().toString(), new Callback<ResultLocation>() {
            @Override
            public void success(final ResultLocation resultLocation, Response response) {
                mResults = resultLocation.getResults();
                Log.d("TAG", "success: " + mResults.size());
                mAdapter = new SearchAdapter(SearchLocationActivity.this, mResults, SearchLocationActivity.this);
                mRcvResultSearch.setLayoutManager(new LinearLayoutManager(SearchLocationActivity.this));
                mRcvResultSearch.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("TAG", "error: " + error.toString());
                mRcvResultSearch.removeAllViewsInLayout();
            }
        });
    }

    @Override
    public void onItemResultLocationClick(List<AddressComponent> address) {
        String city = "";
        String state = "";
        String country = "";
        Log.d("xxx", "onItemResultLocationClick: ." + address);
        for (AddressComponent addressComponent : address) {
            if (addressComponent.getTypes().get(0).equals("locality")) {
                city = addressComponent.getLongName();
            } else if (addressComponent.getTypes().get(0).equals("administrative_area_level_1")) {
                state = addressComponent.getShortName();
            } else if (addressComponent.getTypes().get(0).equals("country")) {
                country = addressComponent.getLongName();
            }
        }
        Intent intent = new Intent();
        intent.putExtra("city", city);
        intent.putExtra("state", state);
        intent.putExtra("country", country);
        setResult(SEARCH_LOCATION, intent);
        finish();
//        RegisterActivity_.intent(this).city(city).state(state).country(country).startForResult(SEARCH_LOCATION);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
