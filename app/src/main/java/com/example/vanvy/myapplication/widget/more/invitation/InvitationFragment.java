package com.example.vanvy.myapplication.widget.more.invitation;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 10/13/2016.
 */
@EFragment(R.layout.fragment_invitation)
public class InvitationFragment extends BaseFragment {
}
