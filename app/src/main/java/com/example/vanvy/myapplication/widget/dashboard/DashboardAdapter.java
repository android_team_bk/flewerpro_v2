package com.example.vanvy.myapplication.widget.dashboard;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.ComingEvent;
import com.example.vanvy.myapplication.util.Helpers;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.example.vanvy.myapplication.util.Helpers.getDateTimeZone;

/**
 * Created by asiantech on 10/7/16.
 */
public class DashboardAdapter extends BaseAdapter {
	private List<ComingEvent> mComingEvents;
	private Context mContext;

	public DashboardAdapter(Context context, List<ComingEvent> comingEvents) {
		super(context);
		mContext = context;
		mComingEvents = comingEvents;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewItemComingHolder(LayoutInflater.from(mContext).inflate(R.layout.item_coming_event, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindItemComingEvent((ViewItemComingHolder) holder, position);
	}

	private void onBindItemComingEvent(ViewItemComingHolder holder, int position) {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
//		String currentDateandTime = sdf.format(new Date());
		String hoursStart = mComingEvents.get(position).getStartTime().substring(0, 2);
		String minuteStart = mComingEvents.get(position).getStartTime().substring(3, 5);
		String secondStart = mComingEvents.get(position).getStartTime().substring(6, 8);
		String hoursEnd = mComingEvents.get(position).getEndTime().substring(0, 2);
		String minuteEnd = mComingEvents.get(position).getEndTime().substring(3, 5);
		String secondEnd = mComingEvents.get(position).getEndTime().substring(6, 8);
		String dateStart = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getStartDatetime())).substring(0, 2);
		String monthStart = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getStartDatetime())).substring(3, 5);
		String yearStart = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getStartDatetime())).substring(6, 10);
		String dateEnd = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getEndDatetime())).substring(0, 2);
		String monthEnd = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getEndDatetime())).substring(3, 5);
		String yearEnd = getDateTimeZone(Long.valueOf(mComingEvents.get(position).getEndDatetime())).substring(6, 10);
		Log.d("xxx", "onBindItemComingEvent: " + System.currentTimeMillis());
		holder.mTvClubName.setText(mComingEvents.get(position).getClubName());
		holder.mTvTimeStart.setText(Helpers.getTimeAMPM(hoursStart, minuteStart, secondStart));
		holder.mTvTimeEnd.setText(Helpers.getTimeAMPM(hoursEnd, minuteEnd, secondEnd));
		holder.mTvDateStart.setText(dateStart + " " + Helpers.convertDateInt(Integer.parseInt(monthStart)) + " " + yearStart);
		holder.mTvDateEnd.setText(dateEnd + " " + Helpers.convertDateInt(Integer.parseInt(monthEnd)) + " " + yearEnd);
		if (Long.valueOf(mComingEvents.get(position).getTimeLeft()) > 0) {
			holder.mTvTimeExist.setText(Helpers.getSecondTime(Long.valueOf(mComingEvents.get(position).getTimeLeft())));
			holder.mTvSetAlarm.setVisibility(View.VISIBLE);
			holder.mRlAlarm.setVisibility(View.VISIBLE);
			holder.mTvLeft.setText("Time left: ");
		} else {
			holder.mTvTimeExist.setText(Helpers.getSecondTime(Long.valueOf(mComingEvents.get(position).getTimeRemain())));
			holder.mTvLeft.setText("End in: ");
			holder.mTvSetAlarm.setVisibility(View.GONE);
			holder.mRlAlarm.setVisibility(View.INVISIBLE);
		}
		Picasso.with(mContext).load(mComingEvents.get(position).getClubAvatar()).into(holder.mIvAvatarClubItem);
	}

	@Override
	public int getItemCount() {
		return mComingEvents.size();
	}

	class ViewItemComingHolder extends RecyclerView.ViewHolder {
		ImageView mIvAvatarClubItem;
		TextView mTvClubName;
		TextView mTvSetAlarm;
		TextView mTvTimeExist;
		TextView mTvTimeEnd;
		TextView mTvTimeStart;
		TextView mTvDateEnd;
		TextView mTvDateStart;
		TextView mTvLeft;
		LinearLayout mRlAlarm;

		public ViewItemComingHolder(View itemView) {
			super(itemView);
			mIvAvatarClubItem = (ImageView) itemView.findViewById(R.id.mIvAvatarClubItem);
			mTvClubName = (TextView) itemView.findViewById(R.id.mTvClubName);
			mTvSetAlarm = (TextView) itemView.findViewById(R.id.mTvSetAlarm);
			mTvTimeExist = (TextView) itemView.findViewById(R.id.mTvTimeExist);
			mTvTimeEnd = (TextView) itemView.findViewById(R.id.mTvTimeEnd);
			mTvTimeStart = (TextView) itemView.findViewById(R.id.mTvTimeStart);
			mTvDateEnd = (TextView) itemView.findViewById(R.id.mTvDateEnd);
			mTvDateStart = (TextView) itemView.findViewById(R.id.mTvDateStart);
			mTvLeft = (TextView) itemView.findViewById(R.id.mTvLeft);
			mRlAlarm = (LinearLayout) itemView.findViewById(R.id.mRlAlarm);
		}
	}
}
