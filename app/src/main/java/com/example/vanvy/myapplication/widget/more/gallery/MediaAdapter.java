package com.example.vanvy.myapplication.widget.more.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.Picture;
import com.example.vanvy.myapplication.object.UrlPicture;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.ScreenSize;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanvy on 4/18/2016.
 */
public class MediaAdapter extends BaseAdapter {
    List<Picture> mPublicPicture;
    List<UrlPicture> mUrlPictures;
    Context mContext;
    private int mItemSize;
    private int mType;
    //	private
    private OnItemMediaListener mMediaListener;

    public MediaAdapter(Context context, @NonNull List<Picture> publicPicture, List<UrlPicture> mUrlPictures, int type, OnItemMediaListener mediaListener) {
        super(context);
        mContext = context;
        mPublicPicture = publicPicture;
        this.mUrlPictures = mUrlPictures;
        mType = type;
        mMediaListener = mediaListener;
        mItemSize = (ScreenSize.getScreenWidth(context)) / 2;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mType == 0) {
            return new ViewMediaHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_media, parent, false));
        } else if (mType == 2) {
            return new ViewPrivatePhotoHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_private_photo, parent, false));
        } else if (mType == 3) {
            return new ViewPrivateVideoHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_private_video, parent, false));
        } else {
            return new ViewPrivateVideoHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_private_video, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (mType == 0) {
            onBindViewMediaHolder((ViewMediaHolder) holder, position);
        } else if (mType == 2) {
            onBindPrivatePhotoHolder((ViewPrivatePhotoHolder) holder, position);
        } else if (mType == 3) {
            onBindPrivateVideoHolder((ViewPrivateVideoHolder) holder, position);
        } else {
            onBindPrivateVideoHolder((ViewPrivateVideoHolder) holder, position);
        }
    }

    private void onBindViewMediaHolder(ViewMediaHolder holder, int position) {
        Picasso.with(mContext)
                .load(mPublicPicture.get(position).getmPhoto())
                .into(holder.mIvPicture);
    }

    private void onBindPrivateVideoHolder(final ViewPrivateVideoHolder holder, final int position) {
        Log.d("xxxVideo", "" + mPublicPicture.get(position).getmBought());
        Uri uri = Uri.parse(mPublicPicture.get(position).getmPhoto());
        Bitmap bitmap= Helpers.getVideoFrame(mContext, uri);
        holder.mIvBlockVideo.setImageBitmap(bitmap);
//        holder.mIvBlockVideo.setBackgroundResource(R.mipmap.ic_pre_video);
        holder.mTvPriceVideo.setVisibility(mType == 1 ? View.GONE : View.VISIBLE);
        holder.mIvPrivateVideo.setVisibility(mType == 1 ? View.GONE : View.VISIBLE);
        holder.mTvPriceVideo.setText(mPublicPicture.get(position).getmPrice() + "credits for unlock");
    }

    private void onBindPrivatePhotoHolder(final ViewPrivatePhotoHolder holder, final int position) {
        Picasso.with(mContext)
                .load(mPublicPicture.get(position).getmPhoto())
                .into(holder.mIvPrivatePhoto);
        holder.mTvPrice.setText(mPublicPicture.get(position).getmPrice() + "credits for unlock");
    }

    private void onBindViewVideoHolder(ViewVideoHolder holder, int position) {
        holder.mIvPicture.setBackgroundResource(R.mipmap.ic_avatar);
    }

    @Override
    public int getItemCount() {
        return mPublicPicture.size();
    }

    class ViewMediaHolder extends RecyclerView.ViewHolder {
        ImageView mIvPicture;

        public ViewMediaHolder(View itemView) {
            super(itemView);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.width = mItemSize;
            params.height = mItemSize;
            itemView.setLayoutParams(params);
            mIvPicture = (ImageView) itemView.findViewById(R.id.mIvPublicPicture);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMediaListener.onItemMediaClick(getLayoutPosition());
                }
            });
        }
    }

    class ViewVideoHolder extends RecyclerView.ViewHolder {
        ImageView mIvPicture;

        public ViewVideoHolder(View itemView) {
            super(itemView);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.width = mItemSize;
            params.height = mItemSize;
            itemView.setLayoutParams(params);
            mIvPicture = (ImageView) itemView.findViewById(R.id.mIvPublicPicture);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMediaListener.onItemMediaClick(getLayoutPosition());
                }
            });
        }
    }

    class ViewPrivatePhotoHolder extends RecyclerView.ViewHolder {
        ImageView mIvPrivatePhoto;
        ImageView mIvOption;
        TextView mTvPrice;

        public ViewPrivatePhotoHolder(View itemView) {
            super(itemView);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.width = mItemSize;
            params.height = mItemSize;
            itemView.setLayoutParams(params);
            mIvPrivatePhoto = (ImageView) itemView.findViewById(R.id.mIvPrivatePhoto);
            mTvPrice = (TextView) itemView.findViewById(R.id.mTvPrice);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMediaListener.onItemMediaClick(getLayoutPosition());
                }
            });
        }
    }

    class ViewPrivateVideoHolder extends RecyclerView.ViewHolder {
        ImageView mIvBlockVideo;
        ImageView mIvOption;
        ImageView mIvPrivateVideo;
        TextView mTvPriceVideo;

        public ViewPrivateVideoHolder(View itemView) {
            super(itemView);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.width = mItemSize;
            params.height = mItemSize;
            itemView.setLayoutParams(params);
            mIvBlockVideo = (ImageView) itemView.findViewById(R.id.mIvBlockVideo);
            mIvPrivateVideo = (ImageView) itemView.findViewById(R.id.mIvPrivateVideo);
            mTvPriceVideo = (TextView) itemView.findViewById(R.id.mTvPrice);
            mIvOption = (ImageView) itemView.findViewById(R.id.mIvOption);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMediaListener.onItemMediaClick(getLayoutPosition());
                }
            });
            mIvOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMediaListener.onItemMediaOptionClick(getLayoutPosition(), v);
                }
            });
        }
    }

    public interface OnItemMediaListener {
        void onItemMediaClick(int position);

        void onItemMediaOptionClick(int position, View view);
    }
}
