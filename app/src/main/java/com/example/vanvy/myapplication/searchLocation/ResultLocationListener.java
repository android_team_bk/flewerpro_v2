package com.example.vanvy.myapplication.searchLocation;

import com.example.vanvy.myapplication.searchLocation.location.AddressComponent;

import java.util.List;

/**
 * Created by asiantech on 9/6/16.
 */
public interface ResultLocationListener {
    void onItemResultLocationClick(List<AddressComponent> address);
}
