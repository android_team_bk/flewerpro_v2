package com.example.vanvy.myapplication.widget.qr;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_qr_code)
public class QrCodeFragment extends BaseFragment {

	@Click(R.id.btnScan)
	void onScanClick() {
		ScanQrCodeActivity_.intent(getActivity()).start();
	}
}
