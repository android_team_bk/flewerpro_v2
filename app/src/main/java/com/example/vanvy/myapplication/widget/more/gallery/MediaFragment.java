package com.example.vanvy.myapplication.widget.more.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.Picture;
import com.example.vanvy.myapplication.object.ProfileDancer;
import com.example.vanvy.myapplication.object.UrlPicture;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.okhttp.Response;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by vanvy on 4/15/2016.
 */
@EFragment(R.layout.fragment_media)
public class MediaFragment extends BaseFragment implements Constants, PopupMenu.OnMenuItemClickListener {

    @ViewById()
    RecyclerView mRecyclerMedia;
    @ViewById()
    ImageView mIvMedia;
    @ViewById()
    TextView mNameMedia;
    @ViewById()
    TextView mTvNoItem;
    @ViewById
    ProgressBar mPrgMedia;
    @ViewById
    ImageView mIvTab1;
    @ViewById
    ImageView mIvTab2;

    @FragmentArg
    int position;

    @FragmentArg
    ProfileDancer profileDancer;

    private MediaAdapter mAdapter;
    private int mPosition;
    private int kind = 0;
    private Picture mPicture = new Picture();
    private Activity mActivity;

    @AfterViews
    void afterView() {
        mActivity = getActivity();
        setUpTab();
        setUpTopRight(true);
    }

    @Click(R.id.mIvTab1)
    void onTab1Click() {
        kind = 0;
        setUpTopRight(true);
    }

    @Click(R.id.mIvTab2)
    void onTab2Click() {
        kind = 1;
        setUpTopRight(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("xxx", "On resume");
        setUpTab();
    }

    private void setUpTab() {
        mTvNoItem.setVisibility(View.GONE);
        if (position == 1) {
            mIvMedia.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_public_video));
            mNameMedia.setText("Public Video");
            initVideo();
        } else if (position == 0) {
            mIvMedia.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_public_picture));
            mNameMedia.setText("Public Picture");
            initPicture();
        } else if (position == 2) {
            mIvMedia.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_private_picture_unselected));
            mNameMedia.setText("Private Picture");
            initPrivatePicture();
        } else {
            mIvMedia.setImageDrawable(getActivity().getResources().getDrawable(R.mipmap.ic_private_video_unselected));
            mNameMedia.setText("Private Video");
            initPrivateVideo();
        }
    }

    private void initVideo() {
        if (profileDancer.getmPublicVideo() != null) {
            mAdapter = new MediaAdapter(getActivity(), profileDancer.getmPublicVideo(), null, 1, new MediaAdapter.OnItemMediaListener() {
                @Override
                public void onItemMediaClick(int position) {
                    VideoViewActivity_.intent(getActivity()).mPath(profileDancer.getmPublicVideo().get(position).getmPhoto()).start();
                }

                @Override
                public void onItemMediaOptionClick(int position, View view) {
                    mPosition = position;
                    onOptionVideo(position, view);
                    mPicture = profileDancer.getmPublicVideo().get(position);
                }

            });
            mRecyclerMedia.setAdapter(mAdapter);
            mRecyclerMedia.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mAdapter.notifyDataSetChanged();
        } else {
            mTvNoItem.setVisibility(View.VISIBLE);
        }
    }

    private void onOptionVideo(int position, View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_option_gallery, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(this);
    }

    private void initPrivatePicture() {
        if (profileDancer.getmPrivatePicture() != null) {
            mAdapter = new MediaAdapter(getActivity(), profileDancer.getmPrivatePicture(), null, 2, new MediaAdapter.OnItemMediaListener() {
                @Override
                public void onItemMediaClick(int position) {
                    mPosition = position;
                    ImageViewActivity_.intent(getActivity()).picture(profileDancer.getmPrivatePicture().get(position)).startForResult(GalleryActivity.REQUEST_DELETE);
                }

                @Override
                public void onItemMediaOptionClick(int position, View view) {

                }

            });
            mRecyclerMedia.setAdapter(mAdapter);
            mRecyclerMedia.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mAdapter.notifyDataSetChanged();
        } else {
            mTvNoItem.setVisibility(View.VISIBLE);
        }
    }

    private void initPrivateVideo() {
        if (profileDancer.getmPrivateVideo() != null && profileDancer.getmPrivateVideo().size() > 0) {
            mAdapter = new MediaAdapter(getActivity(), profileDancer.getmPrivateVideo(), null, 3, new MediaAdapter.OnItemMediaListener() {
                @Override
                public void onItemMediaClick(int position) {
                    mPosition = position;
                    VideoViewActivity_.intent(getActivity()).mPath(profileDancer.getmPrivateVideo().get(position).getmPhoto()).start();
                }

                @Override
                public void onItemMediaOptionClick(int position, View view) {
                    mPosition = position;
                    onOptionVideo(position, view);
                    mPicture = profileDancer.getmPrivateVideo().get(position);
                }

            });
            mRecyclerMedia.setAdapter(mAdapter);
            mRecyclerMedia.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mAdapter.notifyDataSetChanged();
        } else {
            mTvNoItem.setVisibility(View.VISIBLE);
        }
    }

    private void initPicture() {
        if (profileDancer.getmPublicPictures() != null) {
            mAdapter = new MediaAdapter(getActivity(), profileDancer.getmPublicPictures(), null, 0, new MediaAdapter.OnItemMediaListener() {
                @Override
                public void onItemMediaClick(int position) {
                    ImageViewActivity_.intent(getActivity()).picture(profileDancer.getmPublicPictures().get(position)).startForResult(GalleryActivity.REQUEST_DELETE);
                }

                @Override
                public void onItemMediaOptionClick(int position, View view) {

                }
            });
            if (kind == 0) {
                initGridDisplay(getActivity(), mRecyclerMedia);
            } else {
                initListDisplay(getActivity(), mRecyclerMedia);
            }
            mRecyclerMedia.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } else {
            mTvNoItem.setVisibility(View.VISIBLE);
        }
    }

    // Display a list
    private void initListDisplay(Context context, RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    // Display the Grid
    private void initGridDisplay(Context context, RecyclerView recyclerView) {
        GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void setUpTopRight(boolean isSelected) {
        if (isSelected) {
            mIvTab1.setImageResource(R.mipmap.ic_tab_1_selected);
            mIvTab2.setImageResource(R.mipmap.ic_tab_2);
        } else {
            mIvTab2.setImageResource(R.mipmap.ic_tab_2_selected);
            mIvTab1.setImageResource(R.mipmap.ic_tab_1);
        }
    }

    public void reloadData(ProfileDancer profileDancer) {
        this.profileDancer = profileDancer;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_gallery:
                EditVideoGalleryActivity_.intent(getActivity()).picture(mPicture).start();
                return true;
            case R.id.delete_gallery:
                onDeleteVideo();
                return true;
            default:
                return false;
        }
    }

    private void onDeleteVideo() {
        Helpers.disableTouchOnScreen(mActivity, true);
        mPrgMedia.setVisibility(View.VISIBLE);
        RestAdapter adapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect api = adapter.create(ApiConnect.class);
        api.deleteGallery(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmId(), Integer.parseInt(mPicture.getmId()),
                new Callback<Logout>() {
                    @Override
                    public void success(final Logout logout, retrofit.client.Response response) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(mActivity, false);
                                mPrgMedia.setVisibility(View.GONE);
                                if (logout.getmMessage().equals("Success")) {
                                    Toast.makeText(mActivity, "Delete successful.", Toast.LENGTH_SHORT).show();
                                    if (position == 1) {
                                        profileDancer.getmPublicVideo().remove(mPosition);
                                    } else if (position == 3) {
                                        profileDancer.getmPrivateVideo().remove(mPosition);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(mActivity, logout.getData(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(mActivity, false);
                                mPrgMedia.setVisibility(View.GONE);
                                Toast.makeText(mActivity, "Error: " + RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
    }
}
