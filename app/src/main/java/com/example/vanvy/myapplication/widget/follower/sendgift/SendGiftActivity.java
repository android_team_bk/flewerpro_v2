package com.example.vanvy.myapplication.widget.follower.sendgift;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.object.FollowPerformers;
import com.example.vanvy.myapplication.object.Gift;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/16/16.
 */
@EActivity(R.layout.activity_send_gift)
public class SendGiftActivity extends BaseActivity implements Constants, DialogConfirmFragment.OnConfirmSendListener {
    @ViewById()
    ImageView mIvAvatar;
    @ViewById()
    TextView mTvUserNameDancer;
    @ViewById()
    ImageView mIvLogo;
    @ViewById()
    Button mBtnSendRequest;
    @ViewById()
    TextView mTvSumGift;
    List<Gift> mGifts = new ArrayList<>();
    GiftAdapter mAdapter;
    @ViewById()
    RecyclerView mRcvGift;

    @Extra()
    FollowPerformers followPerformers;
    @Extra
    String mUrl;
    @Extra
    String mName;
    @Extra
    int idFollow;
    @Extra
    ArrayList<Gift> gifts;
    private float mSumGift = 0;
    private int mCount = 0;
    private DialogConfirmFragment mDialog;
    private int uId;

    @AfterViews
    void afterViews() {
        if (SharedPreferences.getTypeLogin(this, TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(this, KEY_CLUB_PROFILE).getmId();
        }
        setTitle("Send gift", false, true,false);
        View v = findViewById(R.id.mHeaderBar);
        ImageView mIvLeft = (ImageView) v.findViewById(R.id.mIvLeft);
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Picasso.with(this)
                .load(mUrl)
                .into(mIvAvatar);
        mTvUserNameDancer.setText(mName);
        if (SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClub() != null) {
            if (SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClub().getmAvatar() != null) {
                Picasso.with(this)
                        .load(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClub().getmAvatar())
                        .into(mIvLogo);
            }
        }
        Log.d("xxxx", "afterViews: " + gifts.size());
        mGifts.addAll(gifts);
        mAdapter = new GiftAdapter(this, mGifts, new GiftAdapter.OnItemGiftListener() {
            @Override
            public void onItemGiftClick(int position) {
                mGifts.get(position).setChoose(!mGifts.get(position).isChoose());
                if (mGifts.get(position).isChoose()) {
                    mCount++;
                    mSumGift += Float.valueOf(mGifts.get(position).getmPrice());
                } else {
                    mCount--;
                    mSumGift -= Float.valueOf(mGifts.get(position).getmPrice());
                }
                if (mSumGift > 0) {
                    mTvSumGift.setVisibility(View.VISIBLE);
                    mTvSumGift.setText("You are selecting gift " + mSumGift + " credits");
                } else {
                    mTvSumGift.setVisibility(View.GONE);
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        mRcvGift.setAdapter(mAdapter);
        mRcvGift.setLayoutManager(new GridLayoutManager(this, 4));
        mAdapter.notifyDataSetChanged();
    }

    @Click(R.id.mBtnSendRequest)
    void onSendRequestClick() {

        if (mCount > 0) {
            if (Float.valueOf(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmCredits()) - mSumGift > 0) {
                Log.d("xxx", "send gift: " + SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClubId() + "-" + idFollow);
                RetrofitUtils.buildApiInterface(ApiConnect.class).sendCredit(uId,
                        idFollow, String.valueOf(mSumGift), SharedPreferences.getTypeLogin(this, TYPE_LOGIN), new Callback<Void>() {
                            @Override
                            public void success(Void aVoid, Response response) {
                                SharedPreferences.getUserProfile(SendGiftActivity.this, KEY_USER_PROFILE).setmCredits(String.valueOf(Float.valueOf(SharedPreferences.getUserProfile(SendGiftActivity.this, KEY_USER_PROFILE).getmCredits()) - mSumGift));
                                mDialog = DialogConfirmFragment_.builder().mContentDialog("Successfully send request").isSuccess(true).build();
                                mDialog.setOnDialogListener(SendGiftActivity.this);
                                mDialog.show(getSupportFragmentManager(), "");
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                mDialog = DialogConfirmFragment_.builder().mContentDialog(error.toString()).isSuccess(false).build();
                                mDialog.setOnDialogListener(SendGiftActivity.this);
                                mDialog.show(getSupportFragmentManager(), "");
                            }
                        });
            }
        }
    }

    @Override
    public void onSendClick() {
        mDialog.dismiss();
        finish();
    }

    @Override
    public void onCancel() {
        mDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
//	private void getDataGift() {
//		RetrofitUtils.buildApiInterface(ApiConnect.class).getDataGift(new Callback<List<Gift>>() {
//			@Override
//			public void success(final List<Gift> gifts, Response response) {
//				runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						for (Gift item : gifts) {
//							item.setChoose(false);
//							mGifts.add(item);
//						}
//						mAdapter.notifyDataSetChanged();
//					}
//				});
//			}
//
//			@Override
//			public void failure(RetrofitError error) {
//
//			}
//		});
//	}
}
