package com.example.vanvy.myapplication.util;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.api.rest.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.mime.TypedFile;

import static com.example.vanvy.myapplication.util.ImageUtil.getDirPath;

/**
 * Created by vanvy on 4/3/2016.
 */
public class Helpers {

	public static void disableTouchOnScreen(Activity activity, boolean isDisable) {
		if (activity == null) {
			return;
		}
		if (isDisable) {
			activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
					WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		} else {
			activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
		}
	}

	public static String timeAPM(String time) {
		try {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss", Locale.US);
			Date date = dateFormatter.parse(time);
// Get time from date
			SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a", Locale.US);
			return timeFormatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String getTimeString(long diff) {
		Log.d("xxxx", "getTimeString: " + diff);
		int diffInDays = (int) (diff / (24 * 60 * 60 * 1000));
		int diffInSeconds = (int) (diff / 1000);
		int diffInMinutes = diffInSeconds / 60;
		int diffInHours = diffInMinutes / 60;
		if (diffInSeconds < 60) {
			return diffInSeconds + " seconds ago";
		} else if (diffInMinutes < 60) {
			return diffInMinutes + " minutes ago";
		} else if (diffInHours < 24) {
			return diffInHours + "hours ago";
		} else {
			if (diffInDays < 7) {
				return diffInDays + " days ago";
			} else if (diffInDays >= 7 && diffInDays < 31) {
				int week = diffInDays / 7;
				return week + " weeks ago";
			} else if (diffInDays >= 31 && (diffInDays / 31) < 12) {
				int month = diffInDays / 31;
				return month + " months ago";
			} else {
				int year = diffInDays / 365;
				return year + " years ago";
			}
		}
	}

	public static String getSecondTime(long second) {
		int diffInSeconds = (int) (second);
		int diffInMinutes = (int) second / 60;
		int diffInHours = (int) second / (60 * 60);
		if (diffInSeconds < 60) {
			return diffInSeconds + " seconds ago";
		} else if (diffInMinutes < 60) {
			return diffInMinutes + " minutes ago";
		} else if (diffInHours < 24) {
			return diffInHours + " hours ago";
		}
		return "";
	}

	public static String getDateTimeZone(long timestamp) {
		try {
			Calendar calendar = Calendar.getInstance();
			TimeZone tz = TimeZone.getDefault();
			calendar.setTimeInMillis(timestamp * 1000);
			calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
			SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
			Date currenTimeZone = (Date) calendar.getTime();
			return sdf.format(currenTimeZone);
		} catch (Exception e) {
		}
		return "";
	}

	public static Bitmap getBitmapFromURL(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean isEmailValid(String email) {
		String regExpn =
				"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
						+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
						+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
						+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if (matcher.matches())
			return true;
		else
			return false;
	}

	public static Bitmap decodeBase64(String input) {
		byte[] decodedBytes = Base64.decode(input, 0);
		return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
	}

	/**
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(
				Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				Constants.IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(Constants.IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ Constants.IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == Constants.MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".png");
		} else {
			return null;
		}
		Log.d("xxxFile", "" + mediaFile);
		return mediaFile;
	}

	public static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	public static int getImageOrientation(String imagePath) {
		int rotate = 0;
		try {

			File imageFile = new File(imagePath);
			ExifInterface exif = new ExifInterface(
					imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotate = 270;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotate = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotate = 90;
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rotate;
	}

	public static Uri getImageUri(Context inContext, Bitmap inImage) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		Log.d("xxx", "getImageUri: " + path);
		return Uri.parse(path);
	}

	public static int convertDateStr(String day) {
		switch (day) {
			case "January":
				return 1;
			case "September":
				return 9;
			case "October":
				return 10;
			case "November":
				return 11;
			case "December":
				return 12;
			case "February":
				return 2;
			case "March":
				return 3;
			case "April":
				return 4;
			case "May":
				return 5;
			case "Jule":
				return 6;
			case "July":
				return 7;
			case "August":
				return 8;
			default:
				return 0;
		}
	}

	public static String convertDateInt(int day) {
		switch (day) {
			case 1:
				return "January";
			case 9:
				return "September";
			case 10:
				return "October";
			case 11:
				return "November";
			case 12:
				return "December";
			case 2:
				return "February";
			case 3:
				return "March";
			case 4:
				return "April";
			case 5:
				return "May";
			case 6:
				return "Jule";
			case 7:
				return "July";
			case 8:
				return "August";
			default:
				return "";
		}
	}

	public static String setGender(int idGender) {
		switch (idGender) {
			case 1:
				return "Male";
			case 2:
				return "Female";
			case 3:
				return "Ohter";
			default:
				return "";
		}
	}

	public static String changeDateTime(String time) {
		return "";
	}

	public static int getGender(String gender) {
		switch (gender) {
			case "Male":
				return 1;
			case "Female":
				return 2;
			case "Ohter":
				return 3;
			default:
				return 0;
		}
	}

	public static String getTime(int time) {
		if (time < 10) {
			return "0" + time;
		}
		return String.valueOf(time);
	}

	public static File saveBitmap(String filename) {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		OutputStream outStream = null;

		File file = new File(filename + ".png");
		if (file.exists()) {
			file.delete();
			file = new File(extStorageDirectory, filename + ".png");
			Log.e("file exist", "" + file + ",Bitmap= " + filename);
		}
		try {
			// make a new bitmap from your file
			Bitmap bitmap = BitmapFactory.decodeFile(file.getName());

			outStream = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
			outStream.flush();
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.e("file", "" + file);
		return file;

	}

	public static TypedFile convertToFile(@NonNull String path, @NonNull Bitmap bitmap) {
		FileOutputStream out = null;
		File imageFileName = new File(path);
		try {
			out = new FileOutputStream(imageFileName);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
		} catch (IOException e) {
			Log.e("xxx", "Failed to convert image to JPEG", e);
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				Log.e("xxx", "Failed to close output stream", e);
			}
		}
		return new TypedFile("image/jpeg", imageFileName);
	}

	public static String getTimeAMPM(String hours, String minutes, String seconds) {
		Log.d("xxx", "getTimeAMPM: " + hours + "-" + minutes + ":" + seconds);
		if (Integer.parseInt(hours) > 12) {
			return String.format(Locale.US, "%02d", Integer.parseInt(hours) - 12) + ":" + minutes + ":" + seconds + " PM";
		}
		return String.format(Locale.US, "%02d", Integer.parseInt(hours)) + ":" + minutes + ":" + seconds + " AM";
	}

	public static Bitmap getVideoFrame(Context context, Uri uri) {
		MediaMetadataRetriever retriever = new MediaMetadataRetriever();
		try {
			retriever.setDataSource(uri.toString(), new HashMap<String, String>());
			return retriever.getFrameAtTime(1000000);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		} finally {
			try {
				retriever.release();
			} catch (RuntimeException ex) {
			}
		}
		return null;
	}
	public static Uri getPhotoUri(Context context) {
		long currentTimeMillis = System.currentTimeMillis();
		String title = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date(currentTimeMillis));
		String dirPath = getDirPath(context);
		String fileName = "img_capture_" + title + ".jpg";
		String path = dirPath + "/" + fileName;
		File file = new File(path);
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
		values.put(MediaStore.Images.Media.MIME_TYPE, MediaType.IMAGE_JPEG);
		values.put(MediaStore.Images.Media.DATA, path);
		values.put(MediaStore.Images.Media.DATE_TAKEN, currentTimeMillis);
		if (file.exists()) {
			values.put(MediaStore.Images.Media.SIZE, file.length());
		}
		return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
	}
	public static Bitmap rotateBitmap(Bitmap source, int rotate) {
		Matrix matrix = new Matrix();
		matrix.postRotate(rotate);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
	{
		ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
		image.compress(compressFormat, quality, byteArrayOS);
		return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
	}
	private String convertToBase64(String imagePath)
	{
		Bitmap bm = BitmapFactory.decodeFile(imagePath);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] byteArrayImage = baos.toByteArray();
//		String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
		return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

	}
}
