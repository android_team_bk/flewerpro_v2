package com.example.vanvy.myapplication.widget.ticket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.ETicket;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 9/22/16.
 */
public class ETicketAdapter extends BaseAdapter {

	private List<ETicket> mETickets;
	private Context mContext;
	private ETicketListener mListener;

	public ETicketAdapter(Context context, List<ETicket> eTickets, ETicketListener listener) {
		super(context);
		mContext = context;
		mETickets = eTickets;
		mListener = listener;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ETicketViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_follower, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindETicket((ETicketViewHolder) holder, mETickets.get(position));
	}

	private void onBindETicket(ETicketViewHolder holder, ETicket eTicket) {
		holder.mTvNameFollower.setText(eTicket.getName());
		Picasso.with(mContext).load(eTicket.getAvatar()).into(holder.mIvAvatar);
		if (eTicket.getIsChecked() == 1) {
			holder.mCbChoose.setChecked(true);
		} else {
			holder.mCbChoose.setChecked(false);
		}
	}

	@Override
	public int getItemCount() {
		return mETickets.size();
	}

	class ETicketViewHolder extends RecyclerView.ViewHolder {
		ImageView mIvAvatar;
		TextView mTvNameFollower;
		CheckBox mCbChoose;

		public ETicketViewHolder(View itemView) {
			super(itemView);
			mIvAvatar = (ImageView) itemView.findViewById(R.id.mIvAvatarFollow);
			mTvNameFollower = (TextView) itemView.findViewById(R.id.mTvNameFollower);
			mCbChoose = (CheckBox) itemView.findViewById(R.id.mCbChoose);
			mCbChoose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						mListener.itemTicketCheck(getLayoutPosition(),1);
					}else{
						mListener.itemTicketCheck(getLayoutPosition(),0);
					}
				}
			});
		}
	}

	public interface ETicketListener {
		void itemTicketCheck(int position,int isCheck);
	}
}
