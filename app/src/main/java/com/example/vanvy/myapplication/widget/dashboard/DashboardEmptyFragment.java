package com.example.vanvy.myapplication.widget.dashboard;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by asiantech on 10/28/16.
 */
@EFragment(R.layout.fragment_empty)
public class DashboardEmptyFragment extends BaseFragment{
}
