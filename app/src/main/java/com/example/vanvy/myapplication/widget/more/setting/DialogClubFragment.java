package com.example.vanvy.myapplication.widget.more.setting;

import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.object.UserClub;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanvy on 10/14/2016.
 */
@EFragment(R.layout.fragment_club_dialog)
public class DialogClubFragment extends DialogFragment implements ItemClubAdapter.ItemClubListener {
    @ViewById
    RecyclerView mRcvClub;
    @FragmentArg
    ArrayList<UserClub> mClubs;
    private ItemClubAdapter mApdapter;
    private ClubSettingListener mListener;

    @AfterViews
    void afterViews() {
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        mApdapter = new ItemClubAdapter(getActivity(), mClubs, this);
        mRcvClub.setAdapter(mApdapter);
        mRcvClub.setLayoutManager(new LinearLayoutManager(getActivity()));
        mApdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClubClick(int position) {
        mListener.onClubListener(position);
    }

    public void setupListener(ClubSettingListener listener) {
        mListener = listener;
    }

    public interface ClubSettingListener {
        void onClubListener(int idClub);
    }
}
