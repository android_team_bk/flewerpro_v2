package com.example.vanvy.myapplication.customView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.vanvy.myapplication.R;

/**
 * Created by asiantech on 9/29/16.
 */
public class CustomGalleryTab extends RelativeLayout implements View.OnClickListener {
	private View mRootView;

	public CustomGalleryTab(Context context) {
		super(context);
		afterViews(context);
	}

	public CustomGalleryTab(Context context, AttributeSet attrs) {
		super(context, attrs);
		afterViews(context);
	}

	public CustomGalleryTab(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		afterViews(context);
	}

	@SuppressLint("NewApi")
	public CustomGalleryTab(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		afterViews(context);
	}

	public void afterViews(Context mContext) {
		mRootView = LayoutInflater.from(mContext).inflate(R.layout.menu_bottom_gallery, null);
		addView(mRootView);
	}

	@Override
	public void onClick(View v) {

	}
}
