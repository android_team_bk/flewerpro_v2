package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 10/6/16.
 */
public class ComingEvent implements Parcelable{
	@SerializedName("id")
	private String mId;
	@SerializedName("id_club")
	private String idClub;
	@SerializedName("id_staff")
	private String idStaff;
	@SerializedName("type")
	private String mType;
	@SerializedName("date")
	private String mDate;
	@SerializedName("shift1")
	private String shift1;
	@SerializedName("shift2")
	private String shift2;
	@SerializedName("shift3")
	private String shift3;
	@SerializedName("shift4")
	private String shift4;
	@SerializedName("is_dayoff")
	private String isDayoff;
	@SerializedName("club_name")
	private String clubName;
	@SerializedName("club_avatar")
	private String clubAvatar;
	@SerializedName("start_time")
	private String startTime;
	@SerializedName("end_time")
	private String endTime;
	@SerializedName("gmt")
	private String gmt;
	@SerializedName("start_datetime")
	private String startDatetime;
	@SerializedName("end_datetime")
	private String endDatetime;
	@SerializedName("time_left")
	private String timeLeft;
	@SerializedName("time_remain")
	private String timeRemain;

	protected ComingEvent(Parcel in) {
		mId = in.readString();
		idClub = in.readString();
		idStaff = in.readString();
		mType = in.readString();
		mDate = in.readString();
		shift1 = in.readString();
		shift2 = in.readString();
		shift3 = in.readString();
		shift4 = in.readString();
		isDayoff = in.readString();
		clubName = in.readString();
		clubAvatar = in.readString();
		startTime = in.readString();
		endTime = in.readString();
		gmt = in.readString();
		startDatetime = in.readString();
		endDatetime = in.readString();
		timeLeft = in.readString();
		timeRemain = in.readString();
	}

	public static final Creator<ComingEvent> CREATOR = new Creator<ComingEvent>() {
		@Override
		public ComingEvent createFromParcel(Parcel in) {
			return new ComingEvent(in);
		}

		@Override
		public ComingEvent[] newArray(int size) {
			return new ComingEvent[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(idClub);
		dest.writeString(idStaff);
		dest.writeString(mType);
		dest.writeString(mDate);
		dest.writeString(shift1);
		dest.writeString(shift2);
		dest.writeString(shift3);
		dest.writeString(shift4);
		dest.writeString(isDayoff);
		dest.writeString(clubName);
		dest.writeString(clubAvatar);
		dest.writeString(startTime);
		dest.writeString(endTime);
		dest.writeString(gmt);
		dest.writeString(startDatetime);
		dest.writeString(endDatetime);
		dest.writeString(timeLeft);
		dest.writeString(timeRemain);
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getIdClub() {
		return idClub;
	}

	public void setIdClub(String idClub) {
		this.idClub = idClub;
	}

	public String getIdStaff() {
		return idStaff;
	}

	public void setIdStaff(String idStaff) {
		this.idStaff = idStaff;
	}

	public String getmType() {
		return mType;
	}

	public void setmType(String mType) {
		this.mType = mType;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getShift1() {
		return shift1;
	}

	public void setShift1(String shift1) {
		this.shift1 = shift1;
	}

	public String getShift2() {
		return shift2;
	}

	public void setShift2(String shift2) {
		this.shift2 = shift2;
	}

	public String getShift3() {
		return shift3;
	}

	public void setShift3(String shift3) {
		this.shift3 = shift3;
	}

	public String getShift4() {
		return shift4;
	}

	public void setShift4(String shift4) {
		this.shift4 = shift4;
	}

	public String getIsDayoff() {
		return isDayoff;
	}

	public void setIsDayoff(String isDayoff) {
		this.isDayoff = isDayoff;
	}

	public String getClubName() {
		return clubName;
	}

	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	public String getClubAvatar() {
		return clubAvatar;
	}

	public void setClubAvatar(String clubAvatar) {
		this.clubAvatar = clubAvatar;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getGmt() {
		return gmt;
	}

	public void setGmt(String gmt) {
		this.gmt = gmt;
	}

	public String getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(String startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(String endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(String timeLeft) {
		this.timeLeft = timeLeft;
	}

	public String getTimeRemain() {
		return timeRemain;
	}

	public void setTimeRemain(String timeRemain) {
		this.timeRemain = timeRemain;
	}
}
