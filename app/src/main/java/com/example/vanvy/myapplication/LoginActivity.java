package com.example.vanvy.myapplication;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.register.RegisterActivity_;
import com.example.vanvy.myapplication.util.Connectivity;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 9/4/2016.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity implements Constants {
    @ViewById()
    TextView mLoginForClub;
    @ViewById()
    EditText mEdtEmail;
    @ViewById()
    EditText mEdtPassword;
    @ViewById()
    ProgressBar mPgbLogin;

    private String mToken = "";
    private CallbackManager mCallbackManager;
    ProfileTracker profileTracker;

    @AfterViews
    void afterViews() {
        mCallbackManager = CallbackManager.Factory.create();
        mLoginForClub.setPaintFlags(mLoginForClub.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        SharedPreferences.setDeviceId(this, Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID));
        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyPermission();
        }
    }

    @Click(R.id.mBtnLogin)
    void onClickLogin() {
        mPgbLogin.setVisibility(View.VISIBLE);
        RetrofitUtils.buildApiInterface(ApiConnect.class).getLogin(
                mEdtEmail.getText().toString(), mEdtPassword.getText().toString(), "android",
                SharedPreferences.getDeviceToken(this), SharedPreferences.getString(this, DEVICE_ID), new Callback<UserProfile>() {
                    @Override
                    public void success(final UserProfile userProfile, Response response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SharedPreferences.clear(LoginActivity.this);
                                SharedPreferences.saveInt(LoginActivity.this, TYPE_LOGIN, 0);
                                mPgbLogin.setVisibility(View.GONE);
                                SharedPreferences.saveBoolean(LoginActivity.this, FIRST_TIME, true);
                                SharedPreferences.saveUserProfile(LoginActivity.this, userProfile, KEY_USER_PROFILE);
                                if (userProfile.getmRule() != 1) {
                                    SharedPreferences.saveBoolean(LoginActivity.this, FIRST_TIME, true);
                                    MainActivity_.intent(LoginActivity.this).start();
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Account invalid", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mPgbLogin.setVisibility(View.GONE);
                                Toast.makeText(LoginActivity.this, "error:" + RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
        );
    }

    @Click(R.id.mBtnRegisterAccount)
    void onRegisterClick() {
        RegisterActivity_.intent(this).start();
        finish();
    }

    @Click(R.id.mLoginForClub)
    void onLoginClubClick() {
        LoginClubActivity_.intent(this).start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                // Initial
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                } else if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                } else {
//                    insertDummyPermission();
                    // Permission Denied
                    Toast.makeText(LoginActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Click(R.id.mBtnLoginFacebook)
    void onLoginFacebookClick() {
        Log.d("xxx", "onLoginFacebookClick: ");
        if (!Connectivity.isConnected(this)) {
            Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
            return;
        }
        loginFacebook();
        Log.d("xxx", "onLoginFacebookClick: " + mCallbackManager);
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                // App code
                Log.d("xxxFacebook", "onCurrentProfileChanged: " + currentProfile.getId());
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        profileTracker.stopTracking();
    }

    private void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("xxxxFB_id", "" + loginResult.getAccessToken());
                        // login successful
                        AccessToken accessToken = loginResult.getAccessToken();
                        mToken = accessToken.getToken();
                        Log.d("xxx", "" + accessToken.getUserId());
                        SharedPreferences.setFacebookAccessToken(LoginActivity.this, mToken);
                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("xxx_cancel", "onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("xxxError_fb", "onError: " + exception.getMessage());
                    }
                });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void insertDummyPermission() {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("STORAGE");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }
}
