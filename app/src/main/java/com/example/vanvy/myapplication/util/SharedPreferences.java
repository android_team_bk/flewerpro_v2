package com.example.vanvy.myapplication.util;

import android.content.Context;

import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.object.UserProfile;
import com.google.gson.Gson;

/**
 * Created by vanvy on 4/3/2016.
 */
public class SharedPreferences {
    public static final String SHARE_PREFERENCE_DATA = "share_preference_data";

    /**
     * save String
     */
    public static void saveString(Context context, String key, String value) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveInt(Context context, String key, int value) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void saveUserProfile(Context context, UserProfile userProfile, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userProfile);
        editor.putString(key, json);
        editor.apply();
    }

    public static UserProfile getUserProfile(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        return gson.fromJson(json, UserProfile.class);
    }

    public static void saveClubProfile(Context context, Club club, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(club);
        editor.putString(key, json);
        editor.apply();
    }

    public static Club getClubProfile(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        return gson.fromJson(json, Club.class);
    }

    /**
     * save double
     */
    public static void saveDouble(Context context, String key, double value) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharedPreferences.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, Double.doubleToLongBits(value));
        editor.commit();
    }

    /**
     * get double
     */
    public static double getDouble(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharedPreferences.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return Double.longBitsToDouble(sharedPreferences.getLong(key, 0));
    }

    public static void setDeviceToken(Context context, String deviceToken) {
        SharedPreferences.saveString(context, Constants.DEVICE_TOKEN, deviceToken);
    }

    public static String getDeviceToken(Context context) {
        return SharedPreferences.getString(context, Constants.DEVICE_TOKEN);
    }

    public static void setToken(Context context, String deviceToken) {
        SharedPreferences.saveString(context, Constants.TOKEN, deviceToken);
    }

    public static String getToken(Context context) {
        return SharedPreferences.getString(context, Constants.TOKEN);
    }

    public static int getTypeLogin(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }

    public static void setDeviceId(Context context, String deviceToken) {
        SharedPreferences.saveString(context, Constants.DEVICE_ID, deviceToken);
    }

    public static String getDeviceId(Context context) {
        return SharedPreferences.getString(context, Constants.DEVICE_ID);
    }

    public static String getString(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

    public static boolean getBoolean(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    public static void clear(Context context) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharedPreferences.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }

    public static void remove(Context context, String key) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(
                SharedPreferences.SHARE_PREFERENCE_DATA, Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(key).apply();
    }

    public static String getFacebookAccessToken(Context context) {
        return SharedPreferences.getString(context, Constants.USER_FACEBOOK_ACCESS_TOKEN);
    }

    public static void setFacebookAccessToken(Context context, String accessToken) {
        SharedPreferences.saveString(context, Constants.USER_FACEBOOK_ACCESS_TOKEN, accessToken);
    }
}
