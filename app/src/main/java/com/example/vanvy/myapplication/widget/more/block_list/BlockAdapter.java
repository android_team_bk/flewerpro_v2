package com.example.vanvy.myapplication.widget.more.block_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.Block;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 9/22/16.
 */
public class BlockAdapter extends BaseAdapter {
	private List<Block> mBlocks;
	private Context mContext;
	private UnlockListener mListener;

	public BlockAdapter(Context context, List<Block> blocks, UnlockListener listener) {
		super(context);
		mBlocks = blocks;
		mContext = context;
		mListener = listener;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolderUnlock(LayoutInflater.from(mContext).inflate(R.layout.item_block, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindUnlock((ViewHolderUnlock) holder, mBlocks.get(position));
	}

	private void onBindUnlock(ViewHolderUnlock holder, Block block) {
		holder.mTvName.setText(block.getName());
		Picasso.with(mContext).load(block.getAvatar()).into(holder.mIvAvatar);
	}

	@Override
	public int getItemCount() {
		return mBlocks.size();
	}

	class ViewHolderUnlock extends RecyclerView.ViewHolder {
		ImageView mIvAvatar;
		TextView mTvName;
		Button mBtnUnlock;

		public ViewHolderUnlock(View itemView) {
			super(itemView);
			mIvAvatar = (ImageView) itemView.findViewById(R.id.ivAvatarBlock);
			mTvName = (TextView) itemView.findViewById(R.id.tvNameBlock);
			mBtnUnlock = (Button) itemView.findViewById(R.id.btnUnlock);
			mBtnUnlock.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mListener.itemBlockClick(getLayoutPosition());
				}
			});
		}
	}

	public interface UnlockListener {
		void itemBlockClick(int position);
	}
}
