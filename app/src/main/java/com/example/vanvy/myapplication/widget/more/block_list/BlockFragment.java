package com.example.vanvy.myapplication.widget.more.block_list;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Block;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 9/15/2016.
 */
@EFragment(R.layout.fragment_block)
public class BlockFragment extends BaseFragment implements BlockAdapter.UnlockListener, Constants {

    @ViewById
    RecyclerView mRcvBlock;
    @ViewById
    ProgressBar mPrgBlock;

    private List<Block> mBlocks = new ArrayList<>();
    private BlockAdapter mAdapter;
    private Activity mActivity;
    private int uId;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        mAdapter = new BlockAdapter(mActivity, mBlocks, this);
        mRcvBlock.setAdapter(mAdapter);
        mRcvBlock.setLayoutManager(new LinearLayoutManager(mActivity));
        getBlockList();
    }

    private void getBlockList() {
        mPrgBlock.setVisibility(View.VISIBLE);
        RetrofitUtils.buildApiInterface(ApiConnect.class).getBlockList(uId,
                SharedPreferences.getTypeLogin(mActivity, TYPE_LOGIN), new Callback<List<Block>>() {
                    @Override
                    public void success(final List<Block> blocks, Response response) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mPrgBlock.setVisibility(View.GONE);
                                mBlocks.addAll(blocks);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mPrgBlock.setVisibility(View.GONE);
                            }
                        });
                    }
                });
    }

    @Override
    public void itemBlockClick(int position) {
        mPrgBlock.setVisibility(View.VISIBLE);
        RetrofitUtils.buildApiInterface(ApiConnect.class).unlockUser(String.valueOf(uId),
                String.valueOf(mBlocks.get(position).getTo()), 0, 0, new Callback<Void>() {
            @Override
            public void success(Void aVoid, Response response) {
                Log.d("xxx", "success: ");
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPrgBlock.setVisibility(View.GONE);
                        mBlocks.clear();
                        getBlockList();
                    }
                });
            }

            @Override
            public void failure(final RetrofitError error) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPrgBlock.setVisibility(View.GONE);
                        Toast.makeText(mActivity, RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
