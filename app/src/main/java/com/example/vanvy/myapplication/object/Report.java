package com.example.vanvy.myapplication.object;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanvy on 10/23/2016.
 */

public class Report {
    @SerializedName("id")
    private int id;
    @SerializedName("sender")
    private int sender;
    @SerializedName("sender_type")
    private String senderType;
    @SerializedName("receiver")
    private String receiver;
    @SerializedName("credit")
    private String credit;
    @SerializedName("fee")
    private String fee;
    @SerializedName("item")
    private int item;
    @SerializedName("deleted_at")
    private String deletedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
}
