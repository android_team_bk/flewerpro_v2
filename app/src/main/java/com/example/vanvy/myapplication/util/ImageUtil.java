package com.example.vanvy.myapplication.util;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.util.Log;

import com.example.vanvy.myapplication.R;

import org.androidannotations.api.rest.MediaType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.R.attr.type;

public final class ImageUtil {
    public static final int TYPE_CAKE = 0;
    public static final int TYPE_COOKIE = 1;

    public static String getDirPath(Context context) {
        String dirPath = "";
        File photoDir = null;
        File extStorageDir = Environment.getExternalStorageDirectory();
        if (extStorageDir.canWrite()) {
            photoDir = new File(extStorageDir.getPath() + "/" + context.getString(R.string.app_name));
        }
        if (photoDir == null) {
            return dirPath;
        }
        if (!photoDir.exists()) {
            photoDir.mkdirs();
        }
        if (photoDir.canWrite()) {
            return photoDir.getPath();
        }
        return dirPath;
    }

    public static String getDirLocalPath(Context context) {
        if (context == null) {
            return null;
        }
        String dirPath = "";
        File photoDir = null;
        File extStorageDir = context.getFilesDir();
        if (extStorageDir.canWrite()) {
            photoDir = new File(extStorageDir.getPath() + "/" + context.getString(R.string.app_name));
        }
        if (photoDir == null) {
            return dirPath;
        }
        if (!photoDir.exists()) {
            photoDir.mkdirs();
        }
        if (photoDir.canWrite()) {
            return photoDir.getPath();
        }
        return dirPath;
    }

    public static String getDirSharePath(Context context) {
        if (context == null) {
            return null;
        }
        String dirPath = "";
        File photoDir = null;
        File extStorageDir = Environment.getExternalStorageDirectory();
        if (extStorageDir.canWrite()) {
            photoDir = new File(extStorageDir.getPath() + "/" + context.getString(R.string.share_folder));
        }
        if (photoDir == null) {
            return dirPath;
        }
        if (!photoDir.exists()) {
            photoDir.mkdirs();
        }
        if (photoDir.canWrite()) {
            return photoDir.getPath();
        }
        return dirPath;
    }

    public static Uri getPhotoUri(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        String title = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date(currentTimeMillis));
        String dirPath = getDirPath(context);
        String fileName = "img_capture_" + title + ".jpg";
        String path = dirPath + "/" + fileName;
        File file = new File(path);
        ContentValues values = new ContentValues();
        values.put(Media.DISPLAY_NAME, fileName);
        values.put(Media.MIME_TYPE, MediaType.IMAGE_JPEG);
        values.put(Media.DATA, path);
        values.put(Media.DATE_TAKEN, currentTimeMillis);
        if (file.exists()) {
            values.put(Media.SIZE, file.length());
        }
        return context.getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
    }

    public static String getPathImg() {
        long currentTimeMillis = System.currentTimeMillis();
        String title = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date(currentTimeMillis));
        return "IMG_" + title + ".png";
    }

    public static String getPathImgNoBorder() {
        long currentTimeMillis = System.currentTimeMillis();
        String title = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date(currentTimeMillis));
        return "IMG_" + "no_border_" + title + ".jpg";
    }

    public static Bitmap resize(Bitmap bitmap, float percent) {
        if (bitmap == null || percent < 0.0f) {
            return null;
        }
        return resize(bitmap, (int) (((float) bitmap.getWidth()) * percent), (int) (((float) bitmap.getHeight()) * percent));
    }

    public static Bitmap resize(Bitmap bitmap, int targetWidth, int targetHeight) {
        if (bitmap == null || targetWidth < 0 || targetHeight < 0) {
            return null;
        }
        int pictureWidth = bitmap.getWidth();
        int pictureHeight = bitmap.getHeight();
        float scale = Math.min(((float) targetWidth) / ((float) pictureWidth), ((float) targetHeight) / ((float) pictureHeight));
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        return Bitmap.createBitmap(bitmap, 0, 0, pictureWidth, pictureHeight, matrix, true);
    }

    public static Bitmap loadImage(Context context, Uri uri) throws IOException, OutOfMemoryError {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        if (uri != null) {
            InputStream is = context.getContentResolver().openInputStream(uri);
            assert is != null;
            BitmapFactory.decodeStream(is, null, options);
            is.close();
            int ow = options.outWidth;
            int oh = options.outHeight;
            int width = (int) (((double) ow) * 1.0d);
            int height = (int) (((double) oh) * 1.0d);
            options.inJustDecodeBounds = false;
            options.inSampleSize = Math.max(ow / width, oh / height);
            InputStream is2 = context.getContentResolver().openInputStream(uri);
            Bitmap bm = BitmapFactory.decodeStream(is2, null, options);
            assert is2 != null;
            is2.close();
            bm = Bitmap.createScaledBitmap(bm, width, (int) (((double) width) * (((double) oh) / ((double) ow))), false);
            Bitmap offBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            new Canvas(offBitmap).drawBitmap(bm, 0.0f, (float) ((height - bm.getHeight()) / 2), null);
            return offBitmap;
        }
        return null;
    }

    private static Bitmap resizeBitmap(Bitmap bitmap, int type) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        int ow;
        int oh;

        if (type == TYPE_COOKIE) {
            ow = 1020;
            oh = 1020;
        } else {
            ow = 1050;
            oh = 1050;
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, ow, oh, false);
        return bitmap;
    }

    public static String saveImageToLocal(Context context, Bitmap bitmap, String str, String format) {
        return saveImageToLocal(context, bitmap, str, format, 0);
    }

    public static String saveImageToLocal( Context context, Bitmap bitmap, String str, String format, int type) {
        if (context == null) {
            return null;
        }
        String path = getDirLocalPath(context) + "/" + str;
        File file = new File(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (format.equals(MediaType.IMAGE_PNG)) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, bos);
        } else {
            Bitmap bitmap1 = resizeBitmap(bitmap, type);
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        }
        byte[] bitmapData = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static String saveImageExternal(Context context, Bitmap bitmap, String file, String folder, String format) {
        if (context == null) {
            return null;
        }
        String path = folder + "/" + file;
        File fileImg = new File(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (format.equals(MediaType.IMAGE_PNG)) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, bos);
        } else {
            Bitmap bitmap1 = resizeBitmap(bitmap, type);
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        }
        byte[] bitmapData = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(fileImg);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static void deleteFiles(String path) {
        File file = new File(path);

        if (file.exists()) {
            Log.d("ttt", "deleteFiles: " + path);
            String deleteCmd = "rm -r " + path;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException ignored) {
            }
        }
    }
}
