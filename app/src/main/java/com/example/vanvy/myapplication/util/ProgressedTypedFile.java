package com.example.vanvy.myapplication.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import retrofit.mime.TypedFile;

/**
 * Created by asiantech on 10/24/16.
 */

public class ProgressedTypedFile extends TypedFile {
    /**
     * Define interface listener convert file to type.
     */
    public interface Listener {
        void onUpdateProgress(int percentage);
    }

    private static final int BUFFER_SIZE = 4096;
    private Listener mListener;
    private long mTotalSize = 0;

    public ProgressedTypedFile(String mimeType, File file, Listener mListener) {
        super(mimeType, file);
        this.mListener = mListener;
        mTotalSize = file.length();
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        FileInputStream in = new FileInputStream(super.file());
        long total = 0;
        try {
            int read;
            while ((read = in.read(buffer)) != -1) {
                total += read;
                int percentage = (int) ((total / (float) mTotalSize) * 100);
                out.write(buffer, 0, read);
                this.mListener.onUpdateProgress(percentage);
            }
        } finally {
            in.close();
        }
    }
}
