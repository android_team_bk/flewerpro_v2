package com.example.vanvy.myapplication.widget.more.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.register.ChooseCameraDialog;
import com.example.vanvy.myapplication.register.ChooseCameraDialog_;
import com.example.vanvy.myapplication.register.DialogFragmentGender;
import com.example.vanvy.myapplication.register.DialogFragmentGender_;
import com.example.vanvy.myapplication.register.onGenderListener;
import com.example.vanvy.myapplication.searchLocation.SearchLocationActivity_;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.CameraUtil1;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.ImageUtil;
import com.example.vanvy.myapplication.util.Keyboard;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment_;
import com.example.vanvy.myapplication.widget.more.setting.dialog.DialogConfirmPerformFragment;
import com.example.vanvy.myapplication.widget.more.setting.dialog.DialogConfirmPerformFragment_;
import com.example.vanvy.myapplication.widget.ticket.TicketFragment;
import com.google.android.gms.common.api.Api;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.vanvy.myapplication.util.Helpers.convertDateInt;
import static com.example.vanvy.myapplication.util.Helpers.convertDateStr;
import static com.example.vanvy.myapplication.util.Helpers.encodeToBase64;
import static com.example.vanvy.myapplication.util.Helpers.getGender;
import static com.example.vanvy.myapplication.util.Helpers.setGender;

/**
 * Created by vanvy on 9/13/16.
 */
@EFragment(R.layout.fragment_profile)
public class ProfileFragment extends BaseFragment implements Constants, onGenderListener, ChooseCameraDialog.onDialogDismiss,
        DialogConfirmFragment.OnConfirmSendListener, DialogConfirmPerformFragment.OnConfirmPerFormListener {
    private int yearAge;
    private int month;
    private int day;
    private Activity mContext;
    private String[] date;
    private DialogFragmentGender mDialogGender;
    private int mIdGender;
    private String mCity = "";
    private String mState = "";
    private String mCountry = "";
    private Uri mImageUri;
    private String mAvatar;
    private ChooseCameraDialog mChooseCameraDialog;
    private Bitmap mBitmap;
    private boolean isUpdatePassword;
    @ViewById
    ImageView mIvAvatar;
    @ViewById
    TextView mTvCredit;
    @ViewById
    EditText mEdtUserName;
    @ViewById
    EditText mEdtRealName;
    @ViewById
    EditText mEdtSurName;
    @ViewById
    EditText mEdtEmail;
    @ViewById
    TextView mTvBirthDay;
    @ViewById
    EditText mEdtTelephone;
    @ViewById
    TextView mTvLocation;
    @ViewById
    TextView mTvGender;
    @ViewById
    ProgressBar mPrgUpdateProfile;
    @ViewById
    TextView mTvUpdatePassword;

    private DialogConfirmFragment mDialog;
    private DialogConfirmPerformFragment mDialogUpdatePassword;
    private UpdatePasswordDialogFragment mUpdatePasswordDialog;

    @AfterViews()
    void afterViews() {
        mContext = getActivity();
        mTvUpdatePassword.setPaintFlags(mTvUpdatePassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        date = new String[3];
        Picasso.with(mContext).load(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmAvatar()).into(mIvAvatar);
        mEdtUserName.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmName());
        mEdtRealName.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmRealName());
        mEdtSurName.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmSurName());
        mEdtEmail.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmEmail());
        mTvBirthDay.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmBirthDay());
        mEdtTelephone.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmTelephone());
        mTvGender.setText(setGender(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmGender()));
        mTvLocation.setText(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmCity() + ", " + SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmState() +
                ", " + SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmCountry());
        date = SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmBirthDay().split(" ");
        yearAge = Integer.parseInt(date[2]);
        day = Integer.parseInt(date[1].replace(",", ""));
        month = convertDateStr(date[0]) - 1;
        mCity = SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmCity();
        mState = SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmState();
        mCountry = SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmCountry();
    }


    @Click(R.id.mTvBirthDay)
    void onDateClick() {
        Keyboard.hideKeyboard(getActivity(), mTvBirthDay);
        SelectDateFragment fragment = new SelectDateFragment();
        fragment.setDate(yearAge, month, day);
        fragment.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mIvAvatar)
    void onAvatarClick() {
        mChooseCameraDialog = ChooseCameraDialog_.builder().build();
        mChooseCameraDialog.setUpCameraListener(this);
        mChooseCameraDialog.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mTvUpdatePassword)
    void onUpdatePassword() {
        mDialogUpdatePassword = DialogConfirmPerformFragment_.builder().mContentDialog("Do you want update your password ?").build();
        mDialogUpdatePassword.setOnDialogListener(ProfileFragment.this);
        mDialogUpdatePassword.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mBtnUpdateProfile)
    void onUpdateClick() {
        Helpers.disableTouchOnScreen(mContext, true);
        mPrgUpdateProfile.setVisibility(View.VISIBLE);
        Bitmap bitmap = ((BitmapDrawable) mIvAvatar.getDrawable()).getBitmap();
        mAvatar = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG, 90);
        Log.d("xxx", "base 64: " + mAvatar);
        RetrofitUtils.buildApiInterface(ApiConnect.class).updateProfile(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmId(),
                month, day, yearAge, mCity, mCountry, mState, mIdGender, mEdtTelephone.getText().toString(),
                mEdtSurName.getText().toString(), mEdtRealName.getText().toString(), mAvatar,
                new Callback<UserProfile>() {
                    @Override
                    public void success(final UserProfile userProfile, Response response) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mContext.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Helpers.disableTouchOnScreen(mContext, false);
                                        mPrgUpdateProfile.setVisibility(View.GONE);
                                        SharedPreferences.saveUserProfile(mContext, userProfile, KEY_USER_PROFILE);
                                        mDialog = DialogConfirmFragment_.builder().mContentDialog("Send ticket successfully.").isSuccess(true).build();
                                        mDialog.setOnDialogListener(ProfileFragment.this);
                                        mDialog.show(getChildFragmentManager(), "");
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(mContext, false);
                                mPrgUpdateProfile.setVisibility(View.GONE);
                                mDialog = DialogConfirmFragment_.builder().mContentDialog(RetrofitUtils.getErrorMessage(error)).isSuccess(true).build();
                                mDialog.setOnDialogListener(ProfileFragment.this);
                                mDialog.show(getChildFragmentManager(), "");
                            }
                        });
                    }
                });
    }

    @Click(R.id.mTvLocation)
    void onSearchLocationClick() {
        Keyboard.hideKeyboard(mContext, mTvLocation);
        SearchLocationActivity_.intent(this).startForResult(SEARCH_LOCATION);
    }

    @Click(R.id.mTvGender)
    void onGenderClick() {
        Keyboard.hideKeyboard(getActivity(), mTvBirthDay);
        mDialogGender = DialogFragmentGender_.builder().build();
        mDialogGender.onGenderListener(this);
        mDialogGender.show(mContext.getFragmentManager(), "");
    }

    @OnActivityResult(REQUEST_CODE_DATE_PICKER)
    void resultDate(Intent data) {
        yearAge = data.getExtras().getInt("year");
        month = data.getExtras().getInt("month") + 1;
        day = data.getExtras().getInt("day");
        String birthDay = (convertDateInt(data.getExtras().getInt("month") + 1)) + " " + data.getExtras().getInt("day") + ", " + data.getExtras().getInt("year");
        mTvBirthDay.setText(birthDay);
    }

    @OnActivityResult(SEARCH_LOCATION)
    void onLocationResult(int resultCode, Intent data) {
        if (resultCode == SEARCH_LOCATION) {
            mCity = data.getExtras().getString("city");
            mState = data.getExtras().getString("state");
            mCountry = data.getExtras().getString("country");
            Log.d("TAG", "onLocationResult: ");
            String address = mCity + ", " + mState + ", " + mCountry;
            mTvLocation.setText(address);
        }
    }

    @Override
    public void onGenderClick(String gender) {
        mDialogGender.dismiss();
        mIdGender = getGender(gender);
        mTvGender.setText(gender);
    }

    @OnActivityResult(Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
    void onResult(int resultCode, Intent data) {
        if (resultCode == mContext.RESULT_OK) {
            try {
                int rotateImage = getRotationImage(mImageUri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), mImageUri);
                bitmap = Helpers.rotateBitmap(bitmap, rotateImage);
                mIvAvatar.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.d("xxx", e.getMessage());
            }
        }
    }

    @OnActivityResult(Constants.RESULT_LOAD_IMAGE)
    void onGalleryResult(int resultCode, Intent data) {
        if (resultCode == mContext.RESULT_OK) {
            try {
                int rotateImage = getRotationImage(data.getData());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                bitmap = Helpers.rotateBitmap(bitmap, rotateImage);
                mIvAvatar.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 200, 200, false));
            } catch (IOException e) {
                Log.d("xxx", e.getMessage());
            }
        }
    }

    private void galleryCaptureImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        try {

            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onDismissCameraListener() {
        mChooseCameraDialog.dismiss();
        startTakePhoto();
    }

    @Override
    public void onDismissGalleryListener() {
        mChooseCameraDialog.dismiss();
        galleryCaptureImage();
    }

    private int getRotationImage(Uri uri) {
        int photoRotate = CameraUtil1.getInstance().getOrientationFromMediaStore(getContext(), uri);
        Log.d("xxxx", "resultTakePhoto: " + photoRotate);
        if (photoRotate == 0) {
            String path = CameraUtil1.getInstance().getPath(getContext(), uri);
            photoRotate = CameraUtil1.getInstance().getOrientationFromExif(path);
        }
        return photoRotate;
    }

    public void startTakePhoto() {
        mImageUri = ImageUtil.getPhotoUri(this.getContext());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    public void onSendClick() {
        mDialog.dismiss();
    }

    @Override
    public void onCancel() {
        mDialog.dismiss();
    }

    @Override
    public void onOkClick(int active) {
        mDialogUpdatePassword.dismiss();
        mUpdatePasswordDialog = UpdatePasswordDialogFragment_.builder().build();
        mUpdatePasswordDialog.show(getChildFragmentManager(), "");
    }

    @Override
    public void onNoClick(int active) {
        mDialogUpdatePassword.dismiss();
    }
}
