package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanvy on 9/19/2016.
 */
public class NotificationObject implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private int type;
    @SerializedName("from_type")
    private int fromType;
    @SerializedName("from")
    private int from;
    @SerializedName("to_type")
    private int toType;
    @SerializedName("to")
    private int to;
    @SerializedName("content_id")
    private int contentId;
    @SerializedName("target_id")
    private int target_id;
    @SerializedName("seen")
    private int seen;
    @SerializedName("date")
    private String date;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updatedAt")
    private String updatedAt;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("display")
    private String display;

    protected NotificationObject(Parcel in) {
        id = in.readInt();
        type = in.readInt();
        fromType = in.readInt();
        from = in.readInt();
        toType = in.readInt();
        to = in.readInt();
        contentId = in.readInt();
        target_id = in.readInt();
        seen = in.readInt();
        date = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        userId = in.readString();
        name = in.readString();
        avatar = in.readString();
        display = in.readString();
    }

    public static final Creator<NotificationObject> CREATOR = new Creator<NotificationObject>() {
        @Override
        public NotificationObject createFromParcel(Parcel in) {
            return new NotificationObject(in);
        }

        @Override
        public NotificationObject[] newArray(int size) {
            return new NotificationObject[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeInt(fromType);
        dest.writeInt(from);
        dest.writeInt(toType);
        dest.writeInt(to);
        dest.writeInt(contentId);
        dest.writeInt(target_id);
        dest.writeInt(seen);
        dest.writeString(date);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(avatar);
        dest.writeString(display);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFromType() {
        return fromType;
    }

    public void setFromType(int fromType) {
        this.fromType = fromType;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getToType() {
        return toType;
    }

    public void setToType(int toType) {
        this.toType = toType;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }
}
