package com.example.vanvy.myapplication.register;

import android.app.Dialog;
import android.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by asiantech on 9/5/16.
 */
@EFragment(R.layout.fragment_dialog_job)
public class DialogFragmentJob extends DialogFragment implements View.OnClickListener,Constants {
    private onJobClickListener mListener;
    @ViewById()
    TextView mTvPerformer;
    @ViewById()
    TextView mTvManager;
    @ViewById()
    TextView mTvDriver;
    @ViewById()
    TextView mTvDj;
    @ViewById()
    TextView mTvWaiter;
    @ViewById()
    TextView mTvSecurity;
    @ViewById()
    TextView mTvPromoter;
    @ViewById()
    TextView mTvBartender;
    @ViewById()
    TextView mTvOther;

    @AfterViews
    void afterViews() {
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        mTvPerformer.setOnClickListener(this);
        mTvManager.setOnClickListener(this);
        mTvDriver.setOnClickListener(this);
        mTvWaiter.setOnClickListener(this);
        mTvDj.setOnClickListener(this);
        mTvSecurity.setOnClickListener(this);
        mTvPromoter.setOnClickListener(this);
        mTvBartender.setOnClickListener(this);
        mTvOther.setOnClickListener(this);
    }

    public void onJobDialogListener(onJobClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View view) {
        String job = "";
        int id = 0;
        switch (view.getId()) {
            case R.id.mTvPerformer:
                job = mTvPerformer.getText().toString();
                id=DANCER;
                break;
            case R.id.mTvManager:
                job = mTvManager.getText().toString();
                id=MANAGER;
                break;
            case R.id.mTvDriver:
                job = mTvDriver.getText().toString();
                id=DRIVER;
                break;
            case R.id.mTvWaiter:
                job = mTvWaiter.getText().toString();
                id=WAITER;
                break;
            case R.id.mTvDj:
                job = mTvDj.getText().toString();
                id=DJ;
                break;
            case R.id.mTvSecurity:
                job = mTvSecurity.getText().toString();
                id=SECURITY;
                break;
            case R.id.mTvPromoter:
                job = mTvPromoter.getText().toString();
                id=PROMOTER;
                break;
            case R.id.mTvBartender:
                job = mTvBartender.getText().toString();
                id=BARTENDER;
                break;
            case R.id.mTvOther:
                job = mTvOther.getText().toString();
                id=OTHER;
                break;
        }
        mListener.onJobClick(job,id);
    }
}
