package com.example.vanvy.myapplication;

import android.provider.Settings;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 10/12/2016.
 */
@EActivity(R.layout.activity_login_club)
public class LoginClubActivity extends BaseActivity implements Constants {
    @ViewById
    EditText mEdtUserName;
    @ViewById
    EditText mEdtPassword;
    @ViewById
    Button mBtnLogin;
    private String deviceId;

    @AfterViews
    void afterViews() {
        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    @Click(R.id.mBtnLogin)
    void onLoginClubClick() {
        RetrofitUtils.buildApiInterface(ApiConnect.class).loginClub(mEdtUserName.getText().toString(),
                mEdtPassword.getText().toString(), "android", SharedPreferences.getDeviceToken(LoginClubActivity.this),
                deviceId, new Callback<List<Club>>() {
                    @Override
                    public void success(final List<Club> club, Response response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SharedPreferences.clear(LoginClubActivity.this);
                                SharedPreferences.saveInt(LoginClubActivity.this, TYPE_LOGIN, 1);
//                                mPgbLogin.setVisibility(View.GONE);
                                SharedPreferences.saveBoolean(LoginClubActivity.this, FIRST_TIME, true);
                                SharedPreferences.saveClubProfile(LoginClubActivity.this, club.get(0), KEY_CLUB_PROFILE);
                                MainActivity_.intent(LoginClubActivity.this).start();
                                finish();
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginClubActivity.this, "" + error.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
    }
}
