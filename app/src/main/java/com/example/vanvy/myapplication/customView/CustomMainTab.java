package com.example.vanvy.myapplication.customView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;

/**
 * Created by vanvy on 6/3/2016.
 */
public class CustomMainTab extends RelativeLayout implements View.OnClickListener {

    private View mRootView;

    private LinearLayout mViewTabOne;
    private LinearLayout mViewTabTwo;
    private LinearLayout mViewTabThreeStaff;
    private LinearLayout mViewTabThreeOther;
    private LinearLayout mViewTabFour;
    private LinearLayout mViewTabFive;
    private LinearLayout mViewTabSix;
    private RelativeLayout view_tab_three;
    private OnItemTabMainClickListener mListten;
    private ImageView mImgTabOne;
    private ImageView mImgTabOTwo;
    //    private ImageView mImgTabThree;
    private ImageView mImgTabFour;
    private ImageView mImgTabFive;
    private TextView mTextTabThree;

    public CustomMainTab(Context context) {
        super(context);
        afterViews(context);
    }

    public CustomMainTab(Context context, OnItemTabMainClickListener mListten) {
        super(context);
        this.mListten = mListten;
    }

    public CustomMainTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        afterViews(context);
    }

    public CustomMainTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        afterViews(context);
    }

    @SuppressLint("NewApi")
    public CustomMainTab(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        afterViews(context);
    }


    public void afterViews(Context mContext) {
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_main, null);
        addView(mRootView);
        //initialize

        mViewTabOne = (LinearLayout) mRootView.findViewById(R.id.view_tab_one);
        mViewTabTwo = (LinearLayout) mRootView.findViewById(R.id.view_tab_two);
        mViewTabThreeStaff = (LinearLayout) mRootView.findViewById(R.id.view_tab_three_staff);
        mViewTabThreeOther = (LinearLayout) mRootView.findViewById(R.id.view_tab_three_other);
        mViewTabFour = (LinearLayout) mRootView.findViewById(R.id.view_tab_four);
        mViewTabFive = (LinearLayout) mRootView.findViewById(R.id.view_tab_five);
        mViewTabSix = (LinearLayout) mRootView.findViewById(R.id.view_tab_six);

        mImgTabOne = (ImageView) mRootView.findViewById(R.id.img_tab_one);
        mImgTabOTwo = (ImageView) mRootView.findViewById(R.id.img_tab_two);
//        mImgTabThree = (ImageView) mRootView.findViewById(R.id.img_tab_three);
        mImgTabFour = (ImageView) mRootView.findViewById(R.id.img_tab_four);
        mImgTabFive = (ImageView) mRootView.findViewById(R.id.img_tab_five);
        view_tab_three = (RelativeLayout) mRootView.findViewById(R.id.view_tab_three);
        setEvent();
//        onItemClickEvent(0);
    }

    public void setDefault() {
        //Tab One
        mViewTabOne.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
        mViewTabTwo.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
        view_tab_three.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
        mViewTabFour.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
        mViewTabFive.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
        mViewTabSix.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
    }

    public void setEvent() {
        mViewTabOne.setOnClickListener(this);
        mViewTabTwo.setOnClickListener(this);
        view_tab_three.setOnClickListener(this);
        mViewTabFour.setOnClickListener(this);
        mViewTabFive.setOnClickListener(this);
        mViewTabSix.setOnClickListener(this);
    }

    public void onItemClickEvent(int position) {
        setDefault();
        if (position == 0) {
            mViewTabOne.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        } else if (position == 1) {
            mViewTabTwo.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        } else if (position == 2) {
            view_tab_three.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        } else if (position == 3) {
            mViewTabFour.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        } else if (position == 4) {
            mViewTabFive.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        } else if (position == 5) {
            mViewTabSix.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
        }
    }

    public void setupTab(boolean rule,int uId) {
        if (rule && uId==0) {
            mViewTabThreeStaff.setVisibility(VISIBLE);
            mViewTabThreeOther.setVisibility(GONE);
        } else {
            mViewTabThreeOther.setVisibility(VISIBLE);
            mViewTabThreeStaff.setVisibility(GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_tab_one:
                mListten.ClickItemTabHost(0);
                break;
            case R.id.view_tab_two:
                mListten.ClickItemTabHost(1);
                break;
            case R.id.view_tab_three:
                mListten.ClickItemTabHost(2);
                break;
            case R.id.view_tab_four:
                mListten.ClickItemTabHost(3);
                break;
            case R.id.view_tab_five:
                mListten.ClickItemTabHost(4);
                break;
            case R.id.view_tab_six:
                mListten.ClickItemTabHost(5);
                break;
            default:
                break;

        }
    }

    public void setItemClickListener(OnItemTabMainClickListener l) {
        this.mListten = l;
    }

    public OnItemTabMainClickListener getItemClickListten() {
        return mListten;
    }

    public interface OnItemTabMainClickListener {
        void ClickItemTabHost(int position);
    }
}