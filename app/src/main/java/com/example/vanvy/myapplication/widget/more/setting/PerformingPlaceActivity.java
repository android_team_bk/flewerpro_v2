package com.example.vanvy.myapplication.widget.more.setting;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.eventBus.BusProvider;
import com.example.vanvy.myapplication.eventBus.SettingEvent;
import com.example.vanvy.myapplication.object.ClubPerforming;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.UserClub;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.more.setting.dialog.DialogConfirmPerformFragment;
import com.example.vanvy.myapplication.widget.more.setting.dialog.DialogConfirmPerformFragment_;
import com.squareup.otto.Produce;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 10/15/2016.
 */
@EActivity(R.layout.activity_performing)
public class PerformingPlaceActivity extends BaseActivity implements Constants, PerformingAdapter.ItemPerformingListener, DialogConfirmPerformFragment.OnConfirmPerFormListener {

	@ViewById
	RecyclerView mRcvClub;
	@ViewById
	TextView mTitleHeader;
	@ViewById
	ImageView mIvLeft;
	@ViewById
	ImageView mIvRight;
	@ViewById
	TextView mTvRight;
	@ViewById
	ProgressBar mPrgPerform;

	@Extra
	int keyRequestCode;
	private PerformingAdapter mAdapter;
	private List<ClubPerforming> mClubPerformings = new ArrayList<>();
	private UserClub mClub;
	private ArrayList<Integer> mIdClubs = new ArrayList<>();
	private DialogConfirmPerformFragment confirmPerformFragment;
	private int mPosition;
	private UserProfile mUserProfile = new UserProfile();

	@AfterViews
	void afterView() {
		init();
		mAdapter = new PerformingAdapter(this, mClubPerformings, this);
		mRcvClub.setAdapter(mAdapter);
		mRcvClub.setLayoutManager(new LinearLayoutManager(this));
		getClubPerforming();
	}

	private void init() {
		mTvRight.setVisibility(View.GONE);
		mIvRight.setVisibility(View.VISIBLE);
		mIvLeft.setVisibility(View.VISIBLE);
		mTitleHeader.setText("Performing Places");
		mIvRight.setBackgroundResource(R.drawable.ic_add_club);
	}

	@Click(R.id.mIvLeft)
	void onBackClick() {
		mClub = new UserClub();
		for (ClubPerforming clubPerforming : mClubPerformings) {
			if (clubPerforming.getIsActive() == 1) {
				mClub = clubPerforming.getClub();
			}
		}
		if (keyRequestCode == REQUEST_CODE_CHANGE_CLUB) {
			mUserProfile=SharedPreferences.getUserProfile(PerformingPlaceActivity.this,KEY_USER_PROFILE);
			mUserProfile.setmClub(mClub);
			SharedPreferences.remove(PerformingPlaceActivity.this, KEY_USER_PROFILE);
			SharedPreferences.saveUserProfile(PerformingPlaceActivity.this, mUserProfile, KEY_USER_PROFILE);
				Intent intent = new Intent();
				intent.putExtra(KEY_SETTING, mClub);
				setResult(3, intent);
		} else {
			if (mClub != null) {
				Intent intent = new Intent();
				intent.putExtra(KEY_SETTING, mClub);
				setResult(2, intent);
			}
		}
		finish();
	}

	@Click(R.id.mIvRight)
	void onRightClick() {
		for (ClubPerforming clubPerforming : mClubPerformings) {
			mIdClubs.add(clubPerforming.getClub().getmId());
		}
		AddNewPlaceActivity_.intent(this).idClubs(mIdClubs).startForResult(RESULT_NEW_PLACE);
	}

	@OnActivityResult(RESULT_NEW_PLACE)
	void onResultNewPlace() {
		getClubPerforming();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

	}

	private void getClubPerforming() {
		mPrgPerform.setVisibility(View.VISIBLE);
		Helpers.disableTouchOnScreen(this, true);
		if (mClubPerformings.size() > 0) {
			mClubPerformings.clear();
		}
		RetrofitUtils.buildApiInterface(ApiConnect.class).getListClubPerforming(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
				new Callback<List<ClubPerforming>>() {
					@Override
					public void success(final List<ClubPerforming> clubPerformings, Response response) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mPrgPerform.setVisibility(View.GONE);
								Helpers.disableTouchOnScreen(PerformingPlaceActivity.this, false);
								mClubPerformings.addAll(clubPerformings);
								mAdapter.notifyDataSetChanged();
							}
						});
					}

					@Override
					public void failure(final RetrofitError error) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mPrgPerform.setVisibility(View.GONE);
								Helpers.disableTouchOnScreen(PerformingPlaceActivity.this, false);
								Toast.makeText(PerformingPlaceActivity.this, "error: " + error.toString(), Toast.LENGTH_LONG).show();
							}
						});
					}
				});
	}

	private void updateRemove(final int position) {
		RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
		ApiConnect mApi = restAdapter.create(ApiConnect.class);
		mApi.updateRemove(mClubPerformings.get(position).getId(), new Callback<Logout>() {
			@Override
			public void success(final Logout logout, Response response) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mClubPerformings.remove(position);
						mAdapter.notifyDataSetChanged();
					}
				});
			}

			@Override
			public void failure(final RetrofitError error) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(PerformingPlaceActivity.this, "error: " + error.toString(), Toast.LENGTH_LONG).show();
					}
				});
			}
		});
	}

	private void setActive(final int position) {
		mPrgPerform.setVisibility(View.VISIBLE);
		Helpers.disableTouchOnScreen(PerformingPlaceActivity.this, true);
		RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
		ApiConnect mApi = restAdapter.create(ApiConnect.class);
		mApi.setActive(mClubPerformings.get(position).getId(), new Callback<Logout>() {
			@Override
			public void success(final Logout logout, Response response) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mPrgPerform.setVisibility(View.GONE);
						Helpers.disableTouchOnScreen(PerformingPlaceActivity.this, false);
						if (logout.getmMessage().equals("Success")) {
							getClubPerforming();
						}
					}
				});
			}

			@Override
			public void failure(final RetrofitError error) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mPrgPerform.setVisibility(View.GONE);
						Helpers.disableTouchOnScreen(PerformingPlaceActivity.this, false);
						Toast.makeText(PerformingPlaceActivity.this, "error: " + error.toString(), Toast.LENGTH_LONG).show();
					}
				});
			}
		});
	}

	private void initDialog(int active, String detail) {
		confirmPerformFragment = DialogConfirmPerformFragment_.builder().active(active)
				.mContentDialog(detail).build();
		confirmPerformFragment.setOnDialogListener(this);
		confirmPerformFragment.show(getSupportFragmentManager(), "");
	}

	@Override
	public void removeItemClick(int position) {
		mPosition = position;
		initDialog(REMOVE, "Do you want to remove " + mClubPerformings.get(mPosition).getClub().getmName() + " as your " +
				"active performing place");
	}

	@Override
	public void setActiveItemClick(int position) {
		mPosition = position;
		initDialog(ACTIVE, "Do you want to set " + mClubPerformings.get(mPosition).getClub().getmName() + " as your" +
				" active performing place");
	}

	@Override
	public void onOkClick(int active) {
		confirmPerformFragment.dismiss();
		if (active == ACTIVE) {
			setActive(mPosition);
		} else if (active == REMOVE) {
			updateRemove(mPosition);
		}
	}

	@Override
	public void onNoClick(int active) {
		confirmPerformFragment.dismiss();
	}
}
