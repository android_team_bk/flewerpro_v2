package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/26/16.
 */
public class Message implements Parcelable {
	@SerializedName("id")
	private int id;
	@SerializedName("last_session")
	private String lastSession;
	@SerializedName("last_active")
	private String lastActive;
	@SerializedName("unseen")
	private int unseen;
	@SerializedName("is_club")
	private int isClub;
	@SerializedName("sender_type")
	private String senderType;
	@SerializedName("sender_id")
	private int senderId;
	@SerializedName("receiver_id")
	private int receiverId;
	@SerializedName("receiver_type")
	private String receiverType;
	@SerializedName("max_session")
	private String maxSession;
	@SerializedName("sender")
	private Sender sender;
	@SerializedName("receiver")
	private Sender receiver;

	protected Message(Parcel in) {
		id = in.readInt();
		lastSession = in.readString();
		lastActive = in.readString();
		unseen = in.readInt();
		isClub = in.readInt();
		senderType = in.readString();
		senderId = in.readInt();
		receiverId = in.readInt();
		receiverType = in.readString();
		maxSession = in.readString();
		sender = in.readParcelable(Sender.class.getClassLoader());
		receiver = in.readParcelable(Sender.class.getClassLoader());
	}

	public static final Creator<Message> CREATOR = new Creator<Message>() {
		@Override
		public Message createFromParcel(Parcel in) {
			return new Message(in);
		}

		@Override
		public Message[] newArray(int size) {
			return new Message[size];
		}
	};

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastSession() {
		return lastSession;
	}

	public void setLastSession(String lastSession) {
		this.lastSession = lastSession;
	}

	public String getLastActive() {
		return lastActive;
	}

	public void setLastActive(String lastActive) {
		this.lastActive = lastActive;
	}

	public int getUnseen() {
		return unseen;
	}

	public void setUnseen(int unseen) {
		this.unseen = unseen;
	}

	public int getIsClub() {
		return isClub;
	}

	public void setIsClub(int isClub) {
		this.isClub = isClub;
	}

	public String getSenderType() {
		return senderType;
	}

	public void setSenderType(String senderType) {
		this.senderType = senderType;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverType() {
		return receiverType;
	}

	public void setReceiverType(String receiverType) {
		this.receiverType = receiverType;
	}

	public String getMaxSession() {
		return maxSession;
	}

	public void setMaxSession(String maxSession) {
		this.maxSession = maxSession;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Sender getReceiver() {
		return receiver;
	}

	public void setReceiver(Sender receiver) {
		this.receiver = receiver;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(lastSession);
		dest.writeString(lastActive);
		dest.writeInt(unseen);
		dest.writeInt(isClub);
		dest.writeString(senderType);
		dest.writeInt(senderId);
		dest.writeInt(receiverId);
		dest.writeString(receiverType);
		dest.writeString(maxSession);
		dest.writeParcelable(sender, flags);
		dest.writeParcelable(receiver, flags);
	}
}
