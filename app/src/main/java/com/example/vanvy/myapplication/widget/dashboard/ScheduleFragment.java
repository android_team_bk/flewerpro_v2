package com.example.vanvy.myapplication.widget.dashboard;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.ComingEvent;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 10/7/16.
 */
@EFragment(R.layout.fragment_schedule)
public class ScheduleFragment extends BaseFragment implements Constants {
    @ViewById
    Button mBtnPast;
    @ViewById
    Button mBtnComing;
    @ViewById
    RecyclerView mRcvEvent;
    @ViewById
    ProgressBar mPrgSchedule;
    private Activity mContext;
    private ScheduleAdapter mScheduleAdapter;
    private List<ComingEvent> mComingEvents = new ArrayList<>();
    private int uId;

    @AfterViews
    void afterViews() {
        mContext = getActivity();
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        mScheduleAdapter = new ScheduleAdapter(mContext, mComingEvents);
        mRcvEvent.setAdapter(mScheduleAdapter);
        mRcvEvent.setLayoutManager(new LinearLayoutManager(mContext));
        mBtnPast.setBackground(getResources().getDrawable(R.drawable.left_report_selected));
        getDataEvent(0);
    }

    @Click(R.id.mBtnPast)
    void onPastClick() {
        setBackgroundDefault();
        mBtnPast.setBackground(getResources().getDrawable(R.drawable.left_report_selected));
        getDataEvent(0);
    }

    @Click(R.id.mBtnComing)
    void onComingClick() {
        setBackgroundDefault();
        mBtnComing.setBackground(getResources().getDrawable(R.drawable.right_report_selected));
        getDataEvent(1);
    }

    private void setBackgroundDefault() {
        mBtnPast.setBackground(getResources().getDrawable(R.drawable.left_report));
        mBtnComing.setBackground(getResources().getDrawable(R.drawable.right_report));
    }

    private void getDataEvent(int type) {
        if (mComingEvents.size() > 0) {
            mComingEvents.clear();
        }
        mPrgSchedule.setVisibility(View.VISIBLE);
        Helpers.disableTouchOnScreen(mContext, true);
        RetrofitUtils.buildApiInterface(ApiConnect.class).getComingAlarm(uId, type, new Callback<List<ComingEvent>>() {
            @Override
            public void success(final List<ComingEvent> comingEvents, Response response) {
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPrgSchedule.setVisibility(View.GONE);
                        Helpers.disableTouchOnScreen(mContext, false);
                        mComingEvents.addAll(comingEvents);
                        mScheduleAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                mPrgSchedule.setVisibility(View.GONE);
                Helpers.disableTouchOnScreen(mContext, false);
            }
        });
    }
}
