package com.example.vanvy.myapplication.widget.ticket;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.object.ClubPerforming;
import com.example.vanvy.myapplication.widget.more.setting.PerformingAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by vanvy on 11/6/2016.
 */
@EFragment(R.layout.fragment_choose_club)
public class ChooseClubDialogFragment extends DialogFragment implements ClubAdapter.ItemClubListener {

    @ViewById
    RecyclerView mRcvClub;
    @ViewById
    TextView mTvDismiss;

    @FragmentArg
    ArrayList<ClubPerforming> performings;

    private ClubAdapter mAdapter;
    private ItemClubChoose mListener;

    @AfterViews
    void afterViews() {
        Log.d("xxx", "club: " + performings.size());
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        mAdapter = new ClubAdapter(getActivity(), performings, this);
        mRcvClub.setAdapter(mAdapter);
        mRcvClub.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Click(R.id.mTvDismiss)
    void onCancel() {
        getDialog().dismiss();
    }

    public void setUpListener(ItemClubChoose listener) {
        mListener = listener;
    }

    @Override
    public void itemClubClick(String name) {
        getDialog().dismiss();
        mListener.itemClubChoose(name);
    }

    public interface ItemClubChoose {
        void itemClubChoose(String clubName);
    }
}
