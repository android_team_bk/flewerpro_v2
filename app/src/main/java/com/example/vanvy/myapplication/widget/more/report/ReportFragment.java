package com.example.vanvy.myapplication.widget.more.report;

import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by vanvy on 10/23/2016.
 */
@EFragment(R.layout.fragment_report)
public class ReportFragment extends BaseFragment implements Constants, ViewPager.OnPageChangeListener {
    @ViewById
    TextView mTvDaily;
    @ViewById
    TextView mTvWeekly;
    @ViewById
    TextView mTvMonthly;
    @ViewById
    ViewPager mViewPage;
    PageReportAdapter mPageAdapter;

    @AfterViews
    void afterViews() {
        init();
    }

    private void init() {
        ArrayList<BaseFragment> tabItems = new ArrayList<>();
        tabItems.add(new ItemReportFragment_());
        tabItems.add(new ItemReportFragment_());
        tabItems.add(new ItemReportFragment_());
        mPageAdapter = new PageReportAdapter(getChildFragmentManager(), tabItems);
        mViewPage.setAdapter(mPageAdapter);
        mViewPage.addOnPageChangeListener(this);
        setupBackgroundBottom(0);
    }

    @Click(R.id.mTvDaily)
    void onDailyClick() {
        mViewPage.setCurrentItem(0);
    }

    @Click(R.id.mTvWeekly)
    void onWeeklyClick() {
        mViewPage.setCurrentItem(1);
    }

    @Click(R.id.mTvMonthly)
    void onMonthlyClick() {
        mViewPage.setCurrentItem(2);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mViewPage.setCurrentItem(position);
        setupBackgroundBottom(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void defaultBottom() {
        mTvDaily.setBackground(getResources().getDrawable(R.drawable.left_report));
        mTvWeekly.setBackground(getResources().getDrawable(R.drawable.mid_report));
        mTvMonthly.setBackground(getResources().getDrawable(R.drawable.right_report));
    }

    private void setupBackgroundBottom(int position) {
        defaultBottom();
        if (position == 0) {
            mTvDaily.setBackground(getResources().getDrawable(R.drawable.left_report_selected));
        } else if (position == 1) {
            mTvWeekly.setBackground(getResources().getDrawable(R.drawable.mid_report_selected));
        } else {
            mTvMonthly.setBackground(getResources().getDrawable(R.drawable.right_report_selected));
        }
    }
}
