package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vanvy on 4/17/2016.
 */
public class ProfileDancer implements Parcelable {
	@SerializedName("id")
	private String mId;
	@SerializedName("name")
	private String mName;
	@SerializedName("email")
	private String mEmail;
	@SerializedName("age")
	private String mAge;
	@SerializedName("birthday")
	private String mBirthDay;
	@SerializedName("city")
	private String mCity;
	@SerializedName("country")
	private String mCountry;
	@SerializedName("state")
	private String mState;
	@SerializedName("gender")
	private String mGender;
	@SerializedName("lat")
	private Double mLat;
	@SerializedName("lng")
	private Double mLng;
	@SerializedName("credits")
	private String mCredits;
	@SerializedName("last_access")
	private String mLastAccess;
	@SerializedName("app_id")
	private int mAppId;
	@SerializedName("facebook_id")
	private String mFacebookId;
	@SerializedName("looking")
	private String mLooking;
	@SerializedName("verified")
	private String mVerified;
	@SerializedName("lang")
	private String mLang;
	@SerializedName("admin")
	private String mAdmin;
	@SerializedName("rule")
	private String rule;
	@SerializedName("profile")
	private String mProfile;
	@SerializedName("private")
	private String mPrivate;
	@SerializedName("min_gift")
	private String mMinGift;
	@SerializedName("max_session")
	private String mMaxSession;
	@SerializedName("approve")
	private String mApprove;
	@SerializedName("free_ticket_sent")
	private String mFreeTicketSent;
	@SerializedName("clubId")
	private String mClubId;
	@SerializedName("fee_ticket_used")
	private String mFeeTicketUsed;
	@SerializedName("inout")
	private String mInout;
	@SerializedName("relation_status")
	private String mRelationStatus;
	@SerializedName("avatar")
	private String mAvatar;
	@SerializedName("public_picture")
	private List<Picture> mPublicPictures;
	@SerializedName("public_video")
	private List<Picture> mPublicVideo;
	@SerializedName("private_video")
	private List<Picture> mPrivateVideo;
	@SerializedName("private_picture")
	private List<Picture> mPrivatePicture;
	@SerializedName("club")
	private Club mClub;

	public ProfileDancer() {

	}

	protected ProfileDancer(Parcel in) {
		mId = in.readString();
		mName = in.readString();
		mEmail = in.readString();
		mAge = in.readString();
		mBirthDay = in.readString();
		mCity = in.readString();
		mCountry = in.readString();
		mState = in.readString();
		mGender = in.readString();
		mCredits = in.readString();
		mLastAccess = in.readString();
		mAppId = in.readInt();
		mFacebookId = in.readString();
		mLooking = in.readString();
		mVerified = in.readString();
		mLang = in.readString();
		mAdmin = in.readString();
		rule = in.readString();
		mProfile = in.readString();
		mPrivate = in.readString();
		mMinGift = in.readString();
		mMaxSession = in.readString();
		mApprove = in.readString();
		mFreeTicketSent = in.readString();
		mClubId = in.readString();
		mFeeTicketUsed = in.readString();
		mInout = in.readString();
		mRelationStatus = in.readString();
		mAvatar = in.readString();
		mPublicPictures = in.createTypedArrayList(Picture.CREATOR);
		mPublicVideo = in.createTypedArrayList(Picture.CREATOR);
		mPrivateVideo = in.createTypedArrayList(Picture.CREATOR);
		mPrivatePicture = in.createTypedArrayList(Picture.CREATOR);
	}

	public static final Creator<ProfileDancer> CREATOR = new Creator<ProfileDancer>() {
		@Override
		public ProfileDancer createFromParcel(Parcel in) {
			return new ProfileDancer(in);
		}

		@Override
		public ProfileDancer[] newArray(int size) {
			return new ProfileDancer[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(mName);
		dest.writeString(mEmail);
		dest.writeString(mAge);
		dest.writeString(mBirthDay);
		dest.writeString(mCity);
		dest.writeString(mCountry);
		dest.writeString(mState);
		dest.writeString(mGender);
		dest.writeString(mCredits);
		dest.writeString(mLastAccess);
		dest.writeInt(mAppId);
		dest.writeString(mFacebookId);
		dest.writeString(mLooking);
		dest.writeString(mVerified);
		dest.writeString(mLang);
		dest.writeString(mAdmin);
		dest.writeString(rule);
		dest.writeString(mProfile);
		dest.writeString(mPrivate);
		dest.writeString(mMinGift);
		dest.writeString(mMaxSession);
		dest.writeString(mApprove);
		dest.writeString(mFreeTicketSent);
		dest.writeString(mClubId);
		dest.writeString(mFeeTicketUsed);
		dest.writeString(mInout);
		dest.writeString(mRelationStatus);
		dest.writeString(mAvatar);
		dest.writeTypedList(mPublicPictures);
		dest.writeTypedList(mPublicVideo);
		dest.writeTypedList(mPrivateVideo);
		dest.writeTypedList(mPrivatePicture);
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmEmail() {
		return mEmail;
	}

	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getmAge() {
		return mAge;
	}

	public void setmAge(String mAge) {
		this.mAge = mAge;
	}

	public String getmBirthDay() {
		return mBirthDay;
	}

	public void setmBirthDay(String mBirthDay) {
		this.mBirthDay = mBirthDay;
	}

	public String getmCity() {
		return mCity;
	}

	public void setmCity(String mCity) {
		this.mCity = mCity;
	}

	public String getmCountry() {
		return mCountry;
	}

	public void setmCountry(String mCountry) {
		this.mCountry = mCountry;
	}

	public String getmState() {
		return mState;
	}

	public void setmState(String mState) {
		this.mState = mState;
	}

	public String getmGender() {
		return mGender;
	}

	public void setmGender(String mGender) {
		this.mGender = mGender;
	}

	public Double getmLat() {
		return mLat;
	}

	public void setmLat(Double mLat) {
		this.mLat = mLat;
	}

	public Double getmLng() {
		return mLng;
	}

	public void setmLng(Double mLng) {
		this.mLng = mLng;
	}

	public String getmCredits() {
		return mCredits;
	}

	public void setmCredits(String mCredits) {
		this.mCredits = mCredits;
	}

	public String getmLastAccess() {
		return mLastAccess;
	}

	public void setmLastAccess(String mLastAccess) {
		this.mLastAccess = mLastAccess;
	}

	public int getmAppId() {
		return mAppId;
	}

	public void setmAppId(int mAppId) {
		this.mAppId = mAppId;
	}

	public String getmFacebookId() {
		return mFacebookId;
	}

	public void setmFacebookId(String mFacebookId) {
		this.mFacebookId = mFacebookId;
	}

	public String getmLooking() {
		return mLooking;
	}

	public void setmLooking(String mLooking) {
		this.mLooking = mLooking;
	}

	public String getmVerified() {
		return mVerified;
	}

	public void setmVerified(String mVerified) {
		this.mVerified = mVerified;
	}

	public String getmLang() {
		return mLang;
	}

	public void setmLang(String mLang) {
		this.mLang = mLang;
	}

	public String getmAdmin() {
		return mAdmin;
	}

	public void setmAdmin(String mAdmin) {
		this.mAdmin = mAdmin;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getmProfile() {
		return mProfile;
	}

	public void setmProfile(String mProfile) {
		this.mProfile = mProfile;
	}

	public String getmPrivate() {
		return mPrivate;
	}

	public void setmPrivate(String mPrivate) {
		this.mPrivate = mPrivate;
	}

	public String getmMinGift() {
		return mMinGift;
	}

	public void setmMinGift(String mMinGift) {
		this.mMinGift = mMinGift;
	}

	public String getmMaxSession() {
		return mMaxSession;
	}

	public void setmMaxSession(String mMaxSession) {
		this.mMaxSession = mMaxSession;
	}

	public String getmApprove() {
		return mApprove;
	}

	public void setmApprove(String mApprove) {
		this.mApprove = mApprove;
	}

	public String getmFreeTicketSent() {
		return mFreeTicketSent;
	}

	public void setmFreeTicketSent(String mFreeTicketSent) {
		this.mFreeTicketSent = mFreeTicketSent;
	}

	public String getmClubId() {
		return mClubId;
	}

	public void setmClubId(String mClubId) {
		this.mClubId = mClubId;
	}

	public String getmFeeTicketUsed() {
		return mFeeTicketUsed;
	}

	public void setmFeeTicketUsed(String mFeeTicketUsed) {
		this.mFeeTicketUsed = mFeeTicketUsed;
	}

	public String getmInout() {
		return mInout;
	}

	public void setmInout(String mInout) {
		this.mInout = mInout;
	}

	public String getmRelationStatus() {
		return mRelationStatus;
	}

	public void setmRelationStatus(String mRelationStatus) {
		this.mRelationStatus = mRelationStatus;
	}

	public String getmAvatar() {
		return mAvatar;
	}

	public void setmAvatar(String mAvatar) {
		this.mAvatar = mAvatar;
	}

	public List<Picture> getmPublicPictures() {
		return mPublicPictures;
	}

	public void setmPublicPictures(List<Picture> mPublicPictures) {
		this.mPublicPictures = mPublicPictures;
	}

	public List<Picture> getmPublicVideo() {
		return mPublicVideo;
	}

	public void setmPublicVideo(List<Picture> mPublicVideo) {
		this.mPublicVideo = mPublicVideo;
	}

	public List<Picture> getmPrivateVideo() {
		return mPrivateVideo;
	}

	public void setmPrivateVideo(List<Picture> mPrivateVideo) {
		this.mPrivateVideo = mPrivateVideo;
	}

	public List<Picture> getmPrivatePicture() {
		return mPrivatePicture;
	}

	public void setmPrivatePicture(List<Picture> mPrivatePicture) {
		this.mPrivatePicture = mPrivatePicture;
	}

	public Club getmClub() {
		return mClub;
	}

	public void setmClub(Club mClub) {
		this.mClub = mClub;
	}
}
