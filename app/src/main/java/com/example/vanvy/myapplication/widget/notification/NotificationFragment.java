package com.example.vanvy.myapplication.widget.notification;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.NotificationObject;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_notification)
public class NotificationFragment extends BaseFragment implements Constants, NotificationAdapter.LoadMoreListener {
	@ViewById
	RecyclerView mRcvNotification;
	@ViewById
	ProgressBar mPrgNotification;

	private NotificationAdapter mNotificationAdapter;
	private List<NotificationObject> mNotification;
	private Activity mActivity;
	private int uId;
	private int isClub;
	private NotificationAdapter.LoadMoreListener mOnLoadMoreListener;
	private boolean isLoading;
	private int visibleItemCount;
	private int lastVisibleItem;
	private int totalItemCount;
	private LinearLayoutManager mLinearLayoutManager;
	private int page = 1;

	@AfterViews
	void afterViews() {
		mActivity = getActivity();
		onLoadMoreListener(this);
		if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
			uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
			isClub = 0;
		} else {
			uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
			isClub = 1;
		}
		mNotification = new ArrayList<>();
		mNotificationAdapter = new NotificationAdapter(getActivity(), mNotification);
		mRcvNotification.setAdapter(mNotificationAdapter);
		mLinearLayoutManager = new LinearLayoutManager(getActivity());
		mRcvNotification.setLayoutManager(mLinearLayoutManager);
		getDataListNotification(page);
		mRcvNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				visibleItemCount = mLinearLayoutManager.getChildCount();
				totalItemCount = mLinearLayoutManager.getItemCount();
				lastVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
				if (!isLoading) {
					if ((visibleItemCount + lastVisibleItem) >= totalItemCount) {
						mOnLoadMoreListener.onLoadMore();
					}
				}
			}
		});
	}

	private void getDataListNotification(int pages) {
		isLoading = true;
		Helpers.disableTouchOnScreen(mActivity, true);
		mPrgNotification.setVisibility(View.VISIBLE);
		RetrofitUtils.buildApiInterface(ApiConnect.class).getNotification(uId, isClub, pages, new Callback<List<NotificationObject>>() {
			@Override
			public void success(final List<NotificationObject> notifications, Response response) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Helpers.disableTouchOnScreen(getActivity(), false);
						mPrgNotification.setVisibility(View.GONE);
						if (notifications.size() > 0) {
							mNotification.addAll(notifications);
							mNotificationAdapter.notifyDataSetChanged();
							page++;
							isLoading = false;
						} else {
							isLoading = true;
						}
					}
				});
			}

			@Override
			public void failure(RetrofitError error) {
				Helpers.disableTouchOnScreen(getActivity(), false);
				mPrgNotification.setVisibility(View.GONE);
				isLoading = true;
			}
		});
	}

	public void onLoadMoreListener(NotificationAdapter.LoadMoreListener l) {
		mOnLoadMoreListener = l;
	}

	@Override
	public void onLoadMore() {
		getDataListNotification(page);
	}
}
