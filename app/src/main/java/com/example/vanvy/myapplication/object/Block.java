package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/22/16.
 */
public class Block implements Parcelable {

	@SerializedName("id")
	private int id;
	@SerializedName("from")
	private int from;
	@SerializedName("from_type")
	private int from_type;
	@SerializedName("to")
	private int to;
	@SerializedName("to_type")
	private int to_type;
	@SerializedName("created_at")
	private String createdAt;
	@SerializedName("updated_at")
	private String updatedAt;
	@SerializedName("name")
	private String name;
	@SerializedName("avatar")
	private String avatar;

	protected Block(Parcel in) {
		id = in.readInt();
		from = in.readInt();
		from_type = in.readInt();
		to = in.readInt();
		to_type = in.readInt();
		createdAt = in.readString();
		updatedAt = in.readString();
		name = in.readString();
		avatar = in.readString();
	}

	public static final Creator<Block> CREATOR = new Creator<Block>() {
		@Override
		public Block createFromParcel(Parcel in) {
			return new Block(in);
		}

		@Override
		public Block[] newArray(int size) {
			return new Block[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(from);
		dest.writeInt(from_type);
		dest.writeInt(to);
		dest.writeInt(to_type);
		dest.writeString(createdAt);
		dest.writeString(updatedAt);
		dest.writeString(name);
		dest.writeString(avatar);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getFrom_type() {
		return from_type;
	}

	public void setFrom_type(int from_type) {
		this.from_type = from_type;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getTo_type() {
		return to_type;
	}

	public void setTo_type(int to_type) {
		this.to_type = to_type;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
}
