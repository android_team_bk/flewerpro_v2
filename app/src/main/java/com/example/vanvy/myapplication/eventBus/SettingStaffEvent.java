package com.example.vanvy.myapplication.eventBus;

/**
 * Created by vanvy on 10/15/2016.
 */
public class SettingStaffEvent {
    private boolean isClicked;

    public SettingStaffEvent(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }
}
