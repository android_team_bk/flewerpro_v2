package com.example.vanvy.myapplication.widget.more.setting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.UserClub;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by vanvy on 10/15/2016.
 */
public class NewPlaceAdapter extends BaseAdapter {
    private List<UserClub> mUserClubs;
    private Context mContext;
    private ItemNewPlaceListener mListener;

    protected NewPlaceAdapter(Context context, List<UserClub> userClubs, ItemNewPlaceListener listener) {
        super(context);
        mContext = context;
        mUserClubs = userClubs;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemNewPlaceHolder(LayoutInflater.from(mContext).inflate(R.layout.item_new_place, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindItemNewPlaceHolder((ItemNewPlaceHolder) holder, position);
    }

    private void onBindItemNewPlaceHolder(ItemNewPlaceHolder holder, int position) {
        holder.mTvAddressClub.setText(mUserClubs.get(position).getmAddress());
        holder.mTvClubName.setText(mUserClubs.get(position).getmName());
        Picasso.with(mContext).load(mUserClubs.get(position).getmAvatar()).into(holder.mIvAvatarClub);
    }

    @Override
    public int getItemCount() {
        return mUserClubs.size();
    }

    class ItemNewPlaceHolder extends RecyclerView.ViewHolder {
        ImageView mIvAvatarClub;
        TextView mTvClubName;
        TextView mTvAddressClub;
        TextView mTvAddClub;

        public ItemNewPlaceHolder(View itemView) {
            super(itemView);
            mIvAvatarClub = (ImageView) itemView.findViewById(R.id.mIvAvatarClub);
            mTvClubName = (TextView) itemView.findViewById(R.id.mTvClubName);
            mTvAddressClub = (TextView) itemView.findViewById(R.id.mTvAddressClub);
            mTvAddClub = (TextView) itemView.findViewById(R.id.mTvAddClub);
            mTvAddClub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemNewPlaceClick(getLayoutPosition());
                }
            });
        }
    }

    public interface ItemNewPlaceListener {
        void onItemNewPlaceClick(int position);
    }
}
