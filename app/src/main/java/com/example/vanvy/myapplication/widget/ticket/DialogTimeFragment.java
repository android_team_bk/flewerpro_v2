package com.example.vanvy.myapplication.widget.ticket;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

import java.util.Calendar;

import static com.example.vanvy.myapplication.util.Helpers.getTime;

/**
 * Created by vanvy on 9/22/2016.
 */
public class DialogTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private int mHour;
    private int mMin;
    private OnTimeListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        mHour = c.getTime().getHours();
        mMin = c.getTime().getMinutes();
        return new TimePickerDialog(getActivity(), this, mHour, mMin, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String ampm = "";
        if (hourOfDay > 12) {
            ampm = "PM";
        } else {
            ampm = "AM";
        }
        mListener.onTimeListener(getTime(hourOfDay) + ":" + getTime(minute) + " " + ampm, hourOfDay, minute);
    }

    public void setUpTimeListener(OnTimeListener listener) {
        mListener = listener;
    }

    public interface OnTimeListener {
        void onTimeListener(String time, int hour, int min);
    }
}
