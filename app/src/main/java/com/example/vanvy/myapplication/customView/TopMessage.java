package com.example.vanvy.myapplication.customView;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.vanvy.myapplication.R;

/**
 * Created by asiantech on 9/26/16.
 */
public class TopMessage extends LinearLayout implements OnClickListener {
	private View mRootView;
	private LinearLayout mLlLocation;
	private LinearLayout mLlVideo;
	private LinearLayout mLlPhoto;
	private LinearLayout mLlAudio;

	public TopMessage(Context context) {
		super(context);
		afterViews(context);
	}

	public TopMessage(Context context, AttributeSet attrs) {
		super(context, attrs);
		afterViews(context);
	}

	public TopMessage(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		afterViews(context);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TopMessage(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		afterViews(context);
	}

	@Override
	public void onClick(View v) {

	}

	public void afterViews(Context mContext) {
		mRootView = LayoutInflater.from(mContext).inflate(R.layout.top_message, null);
		addView(mRootView);

		mLlAudio = (LinearLayout) mRootView.findViewById(R.id.llAudio);
		mLlLocation = (LinearLayout) mRootView.findViewById(R.id.llLocation);
		mLlPhoto = (LinearLayout) mRootView.findViewById(R.id.llPhoto);
		mLlVideo = (LinearLayout) mRootView.findViewById(R.id.llVideo);
	}
}
