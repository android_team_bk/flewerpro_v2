package com.example.vanvy.myapplication.widget.more.gallery;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.object.ProfileDancer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanvy on 4/15/2016.
 */
public class PageAdapter extends FragmentStatePagerAdapter {
	private List<BaseFragment> mTabItems = new ArrayList<>();
	private ProfileDancer mProfileDancer;

	public PageAdapter(FragmentManager fragmentManager,
	                   List<BaseFragment> fragments, ProfileDancer profileDancer) {
		super(fragmentManager);
		mTabItems = fragments;
		mProfileDancer = profileDancer;
	}

	@Override
	public int getCount() {
		return mTabItems.size();
	}

	@Override
	public Fragment getItem(int position) {
		return MediaFragment_.builder().position(position).profileDancer(mProfileDancer).build();
	}

}
