package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by asiantech on 9/16/16.
 */
public class ItemSendRequest implements Parcelable {
    private int id;
    private boolean isChoose;

    protected ItemSendRequest(Parcel in) {
        id = in.readInt();
        isChoose = in.readByte() != 0;
    }

    public static final Creator<ItemSendRequest> CREATOR = new Creator<ItemSendRequest>() {
        @Override
        public ItemSendRequest createFromParcel(Parcel in) {
            return new ItemSendRequest(in);
        }

        @Override
        public ItemSendRequest[] newArray(int size) {
            return new ItemSendRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (isChoose ? 1 : 0));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }
}
