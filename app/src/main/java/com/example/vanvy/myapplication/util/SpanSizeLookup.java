package com.example.vanvy.myapplication.util;

import android.support.v7.widget.GridLayoutManager;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by vynv on 7/11/16.
 */
public class SpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    int spanPos, spanCnt1, spanCnt2;

    public SpanSizeLookup(int spanPos, int spanCnt1, int spanCnt2) {
        super();
        this.spanPos = spanPos;
        this.spanCnt1 = spanCnt1;
        this.spanCnt2 = spanCnt2;
    }

    @Override
    public int getSpanSize(int position) {
        return (position % spanPos == 0 ? spanCnt1 : spanCnt2);
    }
}