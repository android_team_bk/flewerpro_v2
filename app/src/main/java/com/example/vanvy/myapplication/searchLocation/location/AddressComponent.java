package com.example.vanvy.myapplication.searchLocation.location;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asiantech on 9/6/16.
 */
public class AddressComponent implements Parcelable {
    @SerializedName("long_name")
    private String longName;
    @SerializedName("short_name")
    private String shortName;
    @SerializedName("types")
    private List<String> types;

    protected AddressComponent(Parcel in) {
        longName = in.readString();
        shortName = in.readString();
        types = in.createStringArrayList();
    }

    public static final Creator<AddressComponent> CREATOR = new Creator<AddressComponent>() {
        @Override
        public AddressComponent createFromParcel(Parcel in) {
            return new AddressComponent(in);
        }

        @Override
        public AddressComponent[] newArray(int size) {
            return new AddressComponent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(longName);
        parcel.writeString(shortName);
        parcel.writeStringList(types);
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
