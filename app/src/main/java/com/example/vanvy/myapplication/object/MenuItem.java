package com.example.vanvy.myapplication.object;

import android.graphics.drawable.Drawable;

/**
 * Created by asiantech on 9/12/16.
 */
public class MenuItem {
    Class<?> clazz;
    private String title;
    private Drawable mImgItem;
    public MenuItem(String title, Class<?> clazz,Drawable mImgItem) {
        this.title = title;
        this.clazz = clazz;
        this.mImgItem=mImgItem;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getImgItem() {
        return mImgItem;
    }
}
