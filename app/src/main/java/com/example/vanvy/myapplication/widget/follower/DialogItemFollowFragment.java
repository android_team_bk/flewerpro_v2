package com.example.vanvy.myapplication.widget.follower;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.object.FollowPerformers;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/15/16.
 */
@EFragment(R.layout.fragment_dialog_item_follow)
public class DialogItemFollowFragment extends DialogFragment implements Constants, PopupMenu.OnMenuItemClickListener,
        DialogConfirmFragment.OnConfirmSendListener {

    @ViewById
    ImageView mIvAvatar;
    @ViewById
    ImageView mIvReport;
    @ViewById
    TextView mTvName;
    @ViewById
    TextView mTvAge;
    @ViewById
    TextView mTvPlace;
    @ViewById
    Button mBtnSendGift;
    @ViewById
    Button mBtnChat;
    @ViewById
    Button mBtnSendTicket;

    @FragmentArg
    FollowPerformers userProfile;

    private Activity mActivity;
    private String mAgeGender;
    private onItemFollowListener mListener;
    private onBlockListener mBlockListener;
    private DialogConfirmFragment mDialog;
    private onReportListener mReportListener;
    private int uId;

    @AfterViews
    void afterViews() {
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        mActivity = getActivity();
        Picasso.with(mActivity).load(userProfile.getAvatar()).into(mIvAvatar);
        mTvName.setText(userProfile.getName());
        mAgeGender = userProfile.getAge() + "- Other";
        mTvAge.setText(mAgeGender);
        mTvPlace.setText(userProfile.getCity());
    }

    @Click(R.id.mBtnSendGift)
    void onSendGiftClick() {
        mListener.onSendGift();
    }

    @Click(R.id.mBtnSendTicket)
    void onSendTicketClick() {
        mListener.onSendTicket();
    }

    @Click(R.id.mIvReport)
    void onReportClick(View v) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.report_item, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(this);
    }

    public void setUpListener(onItemFollowListener listener) {
        mListener = listener;
    }

    public void setUpBlockListener(onBlockListener blockListener) {
        mBlockListener = blockListener;
    }

    public void setUpReportListener(onReportListener reportListener) {
        mReportListener = reportListener;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.report:
                dismiss();
                report();
                return true;
            case R.id.block:
                block();
                return true;
            default:
                return false;
        }

    }

    @Override
    public void onSendClick() {
        mDialog.dismiss();
    }

    @Override
    public void onCancel() {
        mDialog.dismiss();
    }

    public interface onItemFollowListener {
        void onSendGift();

        void onChat();

        void onSendTicket();
    }

    private void block() {
        Log.d("xxx", "block");
        RetrofitUtils.buildApiInterface(ApiConnect.class).sendBock(uId,
                userProfile.getId(), 0, 0, new Callback<Void>() {
                    @Override
                    public void success(Void aVoid, Response response) {
                        mBlockListener.onBlock();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void report() {
        Log.d("xxx", "report");
        dismiss();
        mReportListener.onReport();
    }

    public interface onBlockListener {
        void onBlock();
    }

    public interface onReportListener {
        void onReport();
    }
}
