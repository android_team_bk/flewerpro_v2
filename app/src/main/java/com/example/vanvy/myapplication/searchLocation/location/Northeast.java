package com.example.vanvy.myapplication.searchLocation.location;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/6/16.
 */
public class Northeast implements Parcelable{
    @SerializedName("lat")
    private Double lat;
    @SerializedName("lng")
    private Double lng;

    protected Northeast(Parcel in) {
    }

    public static final Creator<Northeast> CREATOR = new Creator<Northeast>() {
        @Override
        public Northeast createFromParcel(Parcel in) {
            return new Northeast(in);
        }

        @Override
        public Northeast[] newArray(int size) {
            return new Northeast[size];
        }
    };

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
