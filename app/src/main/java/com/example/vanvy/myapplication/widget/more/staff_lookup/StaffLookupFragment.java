package com.example.vanvy.myapplication.widget.more.staff_lookup;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 10/13/2016.
 */
@EFragment(R.layout.fragment_staff_lookup)
public class StaffLookupFragment extends BaseFragment {
}
