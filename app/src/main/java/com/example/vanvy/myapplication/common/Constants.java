package com.example.vanvy.myapplication.common;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by vanvy on 4/3/2016.
 */
public interface Constants {
	String FIRST_TIME = "";
	String LNG = "long";
	String LAT = "lat";
	String DEVICE_TOKEN = "device_token";
	String TOKEN = "token";
	String DEVICE_ID = "device_id";
	// Google project id
	String SENDER_ID = "727077861981";
	String USER_FACEBOOK_ACCESS_TOKEN = "user_facebook_access_token";
	int RESULT_CAMERA = 1000;
	// Activity request codes
	int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	int REQUEST_WRITE_STORAGE = 112;
	int REQUEST_READ_STORAGE = 113;
	int RESULT_LOAD_IMAGE = 200;
	int MEDIA_TYPE_IMAGE = 1;
	int MEDIA_TYPE_VIDEO = 2;
	String IMAGE_DIRECTORY_NAME = "Flewer";
	String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
	String REGISTRATION_COMPLETE = "registrationComplete";
	int DATE_DIALOG_ID = 999;
	int TIME_DIALOG_ID = 888;

	//    User Profile
	String KEY_USER_PROFILE = "user_profile";
	String KEY_CLUB_PROFILE = "user_profile";
	String ID = "id";
	String USER_NAME = "name";
	String SUR_NAME = "surname";
	String REAL_NAME = "realname";
	String EMAIL = "email";
	String AGE = "age";
	String TELEPHONE = "phone";
	String BIRTHDAY = "birthday";
	String CITY = "city";
	String COUNTRY = "country";
	String STATE = "state";
	String GENDER = "gender";
	String APPID = "appid";
	String FACEBOOKID = "facebook_id";
	String LOOKING = "looking";
	String VERIFIED = "verified";
	String CREDIT = "credit";
	String LANG = "lang";
	String ADMIN = "admin";
	String RULE = "rule";
	String PROFILE = "profile";
	String PRIVATE = "private";
	String MINGIFT = "mingift";
	String MAX_SESSION = "max_session";
	String APP_PROVE = "app_prove";
	String CLUB_ID = "club_id";
	String RELATION_STATUS = "relation_status";
	String AVATAR = "avatar";
	String CLUB = "club";

	int REQUEST_CODE_CREDIT_PHOTO = 1000;
	int REQUEST_CODE_CHANGE_CLUB = 1100;
	int REQUEST_CODE_GALLERY = 1122;
	int REQUEST_CODE_GALLERY_IMAGE = 1212;
	int REQUEST_CODE_GALLERY_VIDEO = 2323;
	int REQUEST_CODE_CREDIT_VIDEO = 3000;
	int RESULT_CODE_CREDIT = 2000;
	int RESULT_CODE_SETTING = 1231;
	int SEARCH_LOCATION = 1111;
	int REQUEST_CODE_DATE_PICKER = 2222;
	int RESULT_CODE_DATE_PICKER = 3333;
	int RESULT_NEW_PLACE = 4444;
	int RESULT_SETTING = 3213;
	String KEY_SETTING = "club";
	/**
	 * rule login
	 */
	String TYPE_LOGIN = "type_login";
	int DANCER = 2;
	int OTHER = 4;
	int DRIVER = 5;
	int DJ = 6;
	int WAITER = 7;
	int PROMOTER = 9;
	int BARTENDER = 10;

	int MANAGER = 3;
	int SECURITY = 8;
	//------
	int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
	int REQUEST_CODE_MORE = 999;
	int ACTIVE = 11;
	int REMOVE = 22;
	public static final String [] METADATA_KEYS = {
			FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM,
			FFmpegMediaMetadataRetriever.METADATA_KEY_ALBUM_ARTIST,
			FFmpegMediaMetadataRetriever.METADATA_KEY_ARTIST,
			FFmpegMediaMetadataRetriever.METADATA_KEY_COMMENT,
			FFmpegMediaMetadataRetriever.METADATA_KEY_COMPOSER,
			FFmpegMediaMetadataRetriever.METADATA_KEY_COPYRIGHT,
			FFmpegMediaMetadataRetriever.METADATA_KEY_CREATION_TIME,
			FFmpegMediaMetadataRetriever.METADATA_KEY_DATE,
			FFmpegMediaMetadataRetriever.METADATA_KEY_DISC,
			FFmpegMediaMetadataRetriever.METADATA_KEY_ENCODER,
			FFmpegMediaMetadataRetriever.METADATA_KEY_ENCODED_BY,
			FFmpegMediaMetadataRetriever.METADATA_KEY_FILENAME,
			FFmpegMediaMetadataRetriever.METADATA_KEY_GENRE,
			FFmpegMediaMetadataRetriever.METADATA_KEY_LANGUAGE,
			FFmpegMediaMetadataRetriever.METADATA_KEY_PERFORMER,
			FFmpegMediaMetadataRetriever.METADATA_KEY_PUBLISHER,
			FFmpegMediaMetadataRetriever.METADATA_KEY_SERVICE_NAME,
			FFmpegMediaMetadataRetriever.METADATA_KEY_SERVICE_PROVIDER,
			FFmpegMediaMetadataRetriever.METADATA_KEY_TITLE,
			FFmpegMediaMetadataRetriever.METADATA_KEY_TRACK,
			FFmpegMediaMetadataRetriever.METADATA_KEY_VARIANT_BITRATE,
			FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION,
			FFmpegMediaMetadataRetriever.METADATA_KEY_AUDIO_CODEC,
			FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_CODEC,
			FFmpegMediaMetadataRetriever.METADATA_KEY_ICY_METADATA,
			FFmpegMediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION,
			FFmpegMediaMetadataRetriever.METADATA_KEY_FRAMERATE
	};
}
