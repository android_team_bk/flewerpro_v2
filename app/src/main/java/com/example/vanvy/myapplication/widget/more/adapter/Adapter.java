package com.example.vanvy.myapplication.widget.more.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.HeaderSetting;
import com.example.vanvy.myapplication.object.MenuItem;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 9/12/16.
 */
public class Adapter extends BaseAdapter implements Constants {
    private Context mContext;
    private List<MenuItem> mItems;
    private HeaderSetting mHeader;
    private OnItemSettingClickListener mListener;
    private int mCurrent = 0;

    private enum Type {
        ITEM(0),
        EMPTY(1),
        HEADER_(2);

        private final int value;

        Type(int val) {
            value = val;
        }

        public int getValue() {
            return value;
        }
    }

    //, @NonNull OnItemSettingClickListener listener
    public Adapter(Context context, List<MenuItem> items, HeaderSetting headerSetting, OnItemSettingClickListener listener) {
        super(context);
        mContext = context;
        mItems = items;
        mListener = listener;
        mHeader = headerSetting;
    }

    @Override
    public int getItemViewType(int position) {
        if(SharedPreferences.getTypeLogin(mContext,TYPE_LOGIN)==1){
            return position == 0 ? Type.HEADER_.getValue() : (position - 1) == 7 || (position - 1) == 11 ? Type.EMPTY.getValue() : Type.ITEM.getValue();
        }
            return position == 0 ? Type.HEADER_.getValue() : (position - 1) == 6 || (position - 1) == 10 ? Type.EMPTY.getValue() : Type.ITEM.getValue();
    }

    public MenuItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Type.HEADER_.getValue()) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.header_setting, parent, false);
            return new HeaderHolder(v);

        } else if (viewType == Type.EMPTY.getValue()) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.setting_empty, parent, false);
            return new EmptyHolder(v);
        }
        View v = LayoutInflater.from(getContext()).inflate(R.layout.item_setting, parent, false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == Type.ITEM.getValue()) {
            onBindItemHolder((ItemHolder) holder, mItems.get(position - 1), position);
        } else if (getItemViewType(position) == Type.HEADER_.getValue()) {
            onBindHeaderHolder((HeaderHolder) holder, mHeader);
        } else {
            onBindEmptyHolder((EmptyHolder) holder, position);
        }
    }

    private void onBindItemHolder(ItemHolder holder, MenuItem menuItem, int pos) {
        String credit = String.valueOf(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmCredits());
        holder.mIvLeft.setImageDrawable(menuItem.getImgItem());
        holder.mTvItemName.setText(menuItem.getTitle());
        holder.mTvCredit.setText(credit);
        if (pos == 1) {
            holder.mTvCredit.setVisibility(View.VISIBLE);
        } else {
            holder.mTvCredit.setVisibility(View.GONE);
        }
    }

    private void onBindHeaderHolder(HeaderHolder holder, HeaderSetting headerSetting) {
        if (headerSetting.getUrlIvAvatar() != null) {
            Picasso.with(mContext)
                    .load(headerSetting.getUrlIvAvatar())
                    .into(holder.mIvAvatar);
        }
        holder.mTvAccount.setText(headerSetting.getName());
    }

    private void onBindEmptyHolder(EmptyHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return mItems.size() + 1;
    }

    public interface OnItemSettingClickListener {
        void onItemSettingClick(int position);
    }

    class EmptyHolder extends RecyclerView.ViewHolder {
        public EmptyHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        ImageView mIvAvatar;
        TextView mTvAccount;

        public HeaderHolder(View itemView) {
            super(itemView);
            mIvAvatar = (ImageView) itemView.findViewById(R.id.mIvAvatar);
            mTvAccount = (TextView) itemView.findViewById(R.id.mTvName);
            mIvAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemSettingClick(getLayoutPosition());
                }
            });
        }
    }

    class ItemHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlMenuItem;
        TextView mTvItemName;
        TextView mTvCredit;
        ImageView mIvLeft;

        public ItemHolder(View itemView) {
            super(itemView);
            mTvItemName = (TextView) itemView.findViewById(R.id.tvMenuItemName);
            mTvCredit = (TextView) itemView.findViewById(R.id.mTvCredit);
            mIvLeft = (ImageView) itemView.findViewById(R.id.imgLeft);
            rlMenuItem = (RelativeLayout) itemView.findViewById(R.id.rlMenuItem);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemSettingClick(getLayoutPosition());
                }
            });
        }
    }
}
