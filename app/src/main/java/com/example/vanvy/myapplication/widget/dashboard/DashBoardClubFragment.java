package com.example.vanvy.myapplication.widget.dashboard;
import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 10/16/2016.
 */
@EFragment(R.layout.fragment_dashboard_club)
public class DashboardClubFragment extends BaseFragment {
}
