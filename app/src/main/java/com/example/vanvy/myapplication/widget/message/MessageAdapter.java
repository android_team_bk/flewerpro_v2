package com.example.vanvy.myapplication.widget.message;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Message;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asiantech on 9/26/16.
 */
public class MessageAdapter extends BaseAdapter implements Constants {
	private List<Message> mMessage = new ArrayList<>();
	private Context mContext;
	private OnItemMessageListener mListener;

	public MessageAdapter(Context context, List<Message> message, OnItemMessageListener listener) {
		super(context);
		mMessage = message;
		mContext = context;
		mListener = listener;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new MessageViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_list_chat, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindMessageHolder((MessageViewHolder) holder, mMessage.get(position));
	}

	private void onBindMessageHolder(MessageViewHolder holder, Message message) {
		if (message.getReceiver() != null) {
			String datetime = message.getLastActive().substring(5, 16);
			if (SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmId() == message.getSenderId()) {
				if (message.getReceiver().getmAvatar() != null) {
					Picasso.with(mContext).load(message.getReceiver().getmAvatar()).into(holder.mIvAvatar);
				}
				holder.mTvName.setText(message.getReceiver().getmName());
			} else {
				if (message.getSender().getmAvatar() != null) {
					Picasso.with(mContext).load(message.getSender().getmAvatar()).into(holder.mIvAvatar);
				}
				holder.mTvName.setText(message.getSender().getmName());
			}
			holder.mTvLastActive.setText("Laste Active: " + datetime);
			if (message.getUnseen() != 0) {
				holder.mTvUnseen.setText(String.valueOf(message.getUnseen()));
				holder.rlRight.setVisibility(View.VISIBLE);
			} else {
				holder.rlRight.setVisibility(View.GONE);
			}
		}
	}

	@Override
	public int getItemCount() {
		return mMessage.size();
	}

	class MessageViewHolder extends RecyclerView.ViewHolder {
		private ImageView mIvAvatar;
		private TextView mTvName;
		private TextView mTvLastActive;
		private TextView mTvUnseen;
		private RelativeLayout rlRight;

		public MessageViewHolder(View itemView) {
			super(itemView);
			mIvAvatar = (ImageView) itemView.findViewById(R.id.mIvAvatar);
			mTvName = (TextView) itemView.findViewById(R.id.mTvName);
			mTvLastActive = (TextView) itemView.findViewById(R.id.mTvLastActive);
			mTvUnseen = (TextView) itemView.findViewById(R.id.mTvUnseen);
			rlRight = (RelativeLayout) itemView.findViewById(R.id.rlRight);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mListener.onItemMessageClick(getLayoutPosition());
				}
			});
		}
	}

	public interface OnItemMessageListener {
		void onItemMessageClick(int position);
	}
}
