package com.example.vanvy.myapplication.widget.more.ticket;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 9/15/2016.
 */
@EFragment(R.layout.fragment_ticket_more)
public class TicketFragmentMore extends BaseFragment {
}
