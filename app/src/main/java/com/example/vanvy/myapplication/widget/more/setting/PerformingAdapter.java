package com.example.vanvy.myapplication.widget.more.setting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.ClubPerforming;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by vanvy on 10/15/2016.
 */
public class PerformingAdapter extends BaseAdapter {
    private List<ClubPerforming> mClubPerformings;
    private Context mContext;
    private ItemPerformingListener mListener;

    public PerformingAdapter(Context context, List<ClubPerforming> clubPerformings, ItemPerformingListener listener) {
        super(context);
        mContext = context;
        mClubPerformings = clubPerformings;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemClubPerformingHolder(LayoutInflater.from(mContext).inflate(R.layout.item_performing, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindItemPerformingHolder((ItemClubPerformingHolder) holder, position);
    }

    private void onBindItemPerformingHolder(ItemClubPerformingHolder holder, int position) {
        holder.mTvClubName.setText(mClubPerformings.get(position).getClub().getmName());
        Picasso.with(mContext).load(mClubPerformings.get(position).getClub().getmAvatar()).into(holder.mIvAvatarClub);
        if (mClubPerformings.get(position).getIsActive() == 1) {
            holder.mTvApprove.setBackground(getResources().getDrawable(R.drawable.bg_active));
            holder.mTvApprove.setText("Approved");
            holder.mTvSetActive.setText("Active");
            holder.mTvSetActive.setVisibility(View.VISIBLE);
            holder.mIvCancel.setVisibility(View.GONE);
        } else {
            holder.mIvCancel.setVisibility(View.VISIBLE);
            if (mClubPerformings.get(position).getApprove() == 3) {
                holder.mTvApprove.setText("Approved");
                holder.mTvSetActive.setVisibility(View.VISIBLE);
                holder.mTvApprove.setBackground(getResources().getDrawable(R.drawable.bg_active));
            } else if (mClubPerformings.get(position).getApprove() == 1) {
                holder.mTvApprove.setText("Pending");
                holder.mTvSetActive.setVisibility(View.GONE);
                holder.mTvApprove.setBackground(getResources().getDrawable(R.drawable.bg_pending));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mClubPerformings.size();
    }

    class ItemClubPerformingHolder extends RecyclerView.ViewHolder {
        ImageView mIvCancel;
        TextView mTvSetActive;
        TextView mTvClubName;
        TextView mTvApprove;
        ImageView mIvAvatarClub;

        public ItemClubPerformingHolder(View itemView) {
            super(itemView);
            mIvCancel = (ImageView) itemView.findViewById(R.id.mIvCancel);
            mIvAvatarClub = (ImageView) itemView.findViewById(R.id.mIvAvatarClub);
            mTvSetActive = (TextView) itemView.findViewById(R.id.mTvSetActive);
            mTvClubName = (TextView) itemView.findViewById(R.id.mTvClubName);
            mTvApprove = (TextView) itemView.findViewById(R.id.mTvApprove);
            mIvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.removeItemClick(getLayoutPosition());
                }
            });
            mTvSetActive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.setActiveItemClick(getLayoutPosition());
                }
            });
        }
    }

    public interface ItemPerformingListener {
        void removeItemClick(int position);

        void setActiveItemClick(int position);
    }
}
