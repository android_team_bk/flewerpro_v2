package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanvy on 10/15/2016.
 */
public class ClubPerforming implements Parcelable{
    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("club_id")
    private int clubId;
    @SerializedName("is_active")
    private int isActive;
    @SerializedName("approve")
    private int approve;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("club")
    private UserClub club;

    protected ClubPerforming(Parcel in) {
        id = in.readInt();
        userId = in.readInt();
        clubId = in.readInt();
        isActive = in.readInt();
        approve = in.readInt();
        createdAt = in.readString();
        updatedAt = in.readString();
        club = in.readParcelable(UserClub.class.getClassLoader());
    }

    public static final Creator<ClubPerforming> CREATOR = new Creator<ClubPerforming>() {
        @Override
        public ClubPerforming createFromParcel(Parcel in) {
            return new ClubPerforming(in);
        }

        @Override
        public ClubPerforming[] newArray(int size) {
            return new ClubPerforming[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(userId);
        dest.writeInt(clubId);
        dest.writeInt(isActive);
        dest.writeInt(approve);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeParcelable(club, flags);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getApprove() {
        return approve;
    }

    public void setApprove(int approve) {
        this.approve = approve;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserClub getClub() {
        return club;
    }

    public void setClub(UserClub club) {
        this.club = club;
    }
}
