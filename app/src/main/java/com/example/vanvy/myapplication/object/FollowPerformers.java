package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanyv on 9/15/16.
 */
public class FollowPerformers implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("city")
    private String city;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("age")
    private int age;
    protected FollowPerformers(Parcel in) {
        id = in.readInt();
        name = in.readString();
        city = in.readString();
        avatar = in.readString();
        age = in.readInt();
    }
    public FollowPerformers(){}
    public static final Creator<FollowPerformers> CREATOR = new Creator<FollowPerformers>() {
        @Override
        public FollowPerformers createFromParcel(Parcel in) {
            return new FollowPerformers(in);
        }

        @Override
        public FollowPerformers[] newArray(int size) {
            return new FollowPerformers[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(age);
        dest.writeString(name);
        dest.writeString(city);
        dest.writeString(avatar);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
