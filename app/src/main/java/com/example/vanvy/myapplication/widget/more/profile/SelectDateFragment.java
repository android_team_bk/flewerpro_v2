package com.example.vanvy.myapplication.widget.more.profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import com.example.vanvy.myapplication.common.Constants;

import java.util.Calendar;

/**
 * Created by vanvy on 9/14/16.
 */
public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener, Constants {

    private int mYear;
    private int mMonth;
    private int mDay;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new DatePickerDialog(getActivity(), this, mYear, mMonth, mDay);
    }

    public void setDate(int year, int month, int day) {
        mYear = year;
        mMonth = month;
        mDay = day;
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        dismiss();
        Intent intent = new Intent();
        intent.putExtra("year", yy);
        intent.putExtra("month", mm);
        intent.putExtra("day", dd);
        getParentFragment().onActivityResult(REQUEST_CODE_DATE_PICKER, RESULT_CODE_DATE_PICKER, intent);
    }

}
