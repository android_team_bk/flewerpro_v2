package com.example.vanvy.myapplication.widget.follower;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.object.FollowPerformers;
import com.example.vanvy.myapplication.widget.follower.sendTicket.SendTicketActivity_;
import com.example.vanvy.myapplication.widget.follower.sendgift.SendGiftActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_follower)
public class FollowerFragment extends BaseFragment implements Constants, DialogItemFollowFragment.onItemFollowListener, DialogItemFollowFragment.onBlockListener,
        DialogItemFollowFragment.onReportListener, DialogReportFragment.SendReportListener, DialogReportOtherFragment.SendReportOtherListener {
    FollowAdapter mAdapter;
    @ViewById()
    RecyclerView mRecyclerFollow;
    @ViewById()
    ProgressBar mPrgFollow;
    @FragmentArg
    ArrayList<Gift> mGifts1;

    private List<FollowPerformers> mFollowPerformers;
    private Activity mActivity;
    private DialogItemFollowFragment itemFollowFragment;
    private FollowPerformers followers = new FollowPerformers();
    private DialogReportFragment mDialogReport;
    private DialogReportOtherFragment mDialogReportOther;
    private int uId;

    @AfterViews
    public void afterViews() {
        mActivity = getActivity();
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        Toast.makeText(mActivity, "id: " + uId, Toast.LENGTH_SHORT).show();
        mPrgFollow.setVisibility(View.VISIBLE);
        mFollowPerformers = new ArrayList<>();
        Helpers.disableTouchOnScreen(mActivity, true);
        mAdapter = new FollowAdapter(mActivity, mFollowPerformers, new FollowAdapter.OnFollowItemListener() {
            @Override
            public void onFollowClick(int position) {
                followers = mFollowPerformers.get(position);
                itemFollowFragment = DialogItemFollowFragment_.builder().userProfile(mFollowPerformers.get(position)).build();
                itemFollowFragment.setUpListener(FollowerFragment.this);
                itemFollowFragment.setUpBlockListener(FollowerFragment.this);
                itemFollowFragment.setUpReportListener(FollowerFragment.this);
                itemFollowFragment.show(getChildFragmentManager(), "");
            }
        });
        mRecyclerFollow.setAdapter(mAdapter);
        mRecyclerFollow.setLayoutManager(new LinearLayoutManager(mActivity));
        getData();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSendGift() {
        itemFollowFragment.dismiss();
        SendGiftActivity_.intent(mActivity).mName(followers.getName()).mUrl(followers.getAvatar()).idFollow(followers.getId()).gifts(mGifts1).start();
    }

    @Override
    public void onChat() {

    }

    @Override
    public void onSendTicket() {
        itemFollowFragment.dismiss();
        SendTicketActivity_.intent(getActivity()).idFollow(followers.getId()).avatar(followers.getAvatar()).nameFollow(followers.getName()).start();
    }

    @Override
    public void onBlock() {
        itemFollowFragment.dismiss();
        getData();
    }

    @Override
    public void onReport() {
        itemFollowFragment.dismiss();
        mDialogReport = DialogReportFragment_.builder().build();
        mDialogReport.setUpSendReportListener(this);
        mDialogReport.show(getChildFragmentManager(), "");
    }

    @Override
    public void sendReport(String reason) {
        mDialogReport.dismiss();
        getSentReport(reason);
    }

    @Override
    public void sendReportOhter() {
        mDialogReport.dismiss();
        mDialogReportOther = DialogReportOtherFragment_.builder().build();
        mDialogReportOther.setUpdateReportOtherListener(this);
        mDialogReportOther.show(getChildFragmentManager(), "");
    }

    @Override
    public void sentReportOther(String reason) {
        mDialogReportOther.dismiss();
        getSentReport(reason);
    }

    private void getSentReport(String reason) {
        RetrofitUtils.buildApiInterface(ApiConnect.class).sendReport(uId,
                0, followers.getId(), reason, 1, new Callback<String>() {
                    @Override
                    public void success(final String result, Response response) {
                        Log.d("xxxx", "success: ");
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!result.equals("")) {
                                    getData();
                                    DialogReportConfirmFragment_.builder().stringNoteConfirm("User Reported").build().show(getChildFragmentManager(), "");
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("xxx", "failure: " + error.toString());
                                DialogReportConfirmFragment_.builder().stringNoteConfirm(RetrofitUtils.getErrorMessage(error)).build().show(getChildFragmentManager(), "");
                            }
                        });
                    }
                });
    }

    private void getData() {
        if (mFollowPerformers.size() > 0) {
            mFollowPerformers.clear();
        }
        RetrofitUtils.buildApiInterface(ApiConnect.class).getFollow(uId, new Callback<List<FollowPerformers>>() {
            @Override
            public void success(final List<FollowPerformers> followPerformers, Response response) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mFollowPerformers.addAll(followPerformers);
                        mAdapter.notifyDataSetChanged();
                        mPrgFollow.setVisibility(View.GONE);
                        Helpers.disableTouchOnScreen(mActivity, false);
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
