package com.example.vanvy.myapplication.customView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by Vynv on 7/6/15.
 */

public class TabHost extends RelativeLayout implements View.OnClickListener {

    private View mRootView;

    private RelativeLayout mViewTabOne;
    private RelativeLayout mViewTabTwo;
    private RelativeLayout mViewTabThree;
    private RelativeLayout mViewTabFour;
    private OnItemClickListtener mListten;
    private ImageView mImgTabOne;
    private ImageView mImgTabOTwo;
    private ImageView mImgTabThree;
    private ImageView mImgTabFour;

    public TabHost(Context context) {
        super(context);
        afterViews(context);
    }

    public TabHost(Context context, OnItemClickListtener mListten) {
        super(context);
        this.mListten = mListten;
    }

    public TabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
        afterViews(context);
    }

    public TabHost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        afterViews(context);
    }

    @SuppressLint("NewApi")
    public TabHost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        afterViews(context);
    }


    public void afterViews(Context mContext) {
        mRootView = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_profile_dancer, null);
        addView(mRootView);
        //initialize

        mViewTabOne = (RelativeLayout) mRootView.findViewById(R.id.view_tab_one);
        mViewTabTwo = (RelativeLayout) mRootView.findViewById(R.id.view_tab_two);
        mViewTabThree = (RelativeLayout) mRootView.findViewById(R.id.view_tab_three);
        mViewTabFour = (RelativeLayout) mRootView.findViewById(R.id.view_tab_four);

        mImgTabOne = (ImageView) mRootView.findViewById(R.id.img_tab_one);
        mImgTabOTwo = (ImageView) mRootView.findViewById(R.id.img_tab_two);
        mImgTabThree = (ImageView) mRootView.findViewById(R.id.img_tab_three);
        mImgTabFour = (ImageView) mRootView.findViewById(R.id.img_tab_four);

        setEvent();
        onItemClickEvent(0);
    }

    public void setDefault() {
        //Tab One
        mViewTabOne.setBackground(getResources().getDrawable(R.drawable.background_tab_unselected));
        mImgTabOne.setImageResource(R.mipmap.ic_public_picture);

        mViewTabTwo.setBackground(getResources().getDrawable(R.drawable.background_tab_unselected));
        mImgTabOTwo.setImageResource(R.mipmap.ic_public_video);

        mViewTabThree.setBackground(getResources().getDrawable(R.drawable.background_tab_unselected));
        mImgTabThree.setImageResource(R.mipmap.ic_private_picture_unselected);

        mViewTabFour.setBackground(getResources().getDrawable(R.drawable.background_tab_unselected));
        mImgTabFour.setImageResource(R.mipmap.ic_private_video_unselected);
    }

    public void setEvent() {
        mViewTabOne.setOnClickListener(this);
        mViewTabTwo.setOnClickListener(this);
        mViewTabThree.setOnClickListener(this);
        mViewTabFour.setOnClickListener(this);
    }

    public void onItemClickEvent(int position) {
        setDefault();
        if (position == 0) {
            mViewTabOne.setBackground(getResources().getDrawable(R.drawable.background_tab_selected));
            mImgTabOne.setImageResource(R.mipmap.ic_public_picture);
        } else if (position == 1) {
            mViewTabTwo.setBackground(getResources().getDrawable(R.drawable.background_tab_selected));
            mImgTabOTwo.setImageResource(R.mipmap.ic_public_video);
        } else if (position == 2) {
            mViewTabThree.setBackground(getResources().getDrawable(R.drawable.background_tab_selected));
            mImgTabThree.setImageResource(R.mipmap.ic_private_picture);
        } else if (position == 3) {
            mViewTabFour.setBackground(getResources().getDrawable(R.drawable.background_tab_selected));
            mImgTabFour.setImageResource(R.mipmap.ic_private_video);
        } else {
            Toast.makeText(getContext(), "Tab Default ", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_tab_one:
                mListten.ClickItemTabHost(0);
                break;
            case R.id.view_tab_two:
                mListten.ClickItemTabHost(1);
                break;
            case R.id.view_tab_three:
                mListten.ClickItemTabHost(2);
                break;
            case R.id.view_tab_four:
                mListten.ClickItemTabHost(3);
                break;
            default:
                break;

        }
    }

    public void setItemClickListten(OnItemClickListtener l) {
        this.mListten = l;
    }

    public OnItemClickListtener getItemClickListten() {
        return mListten;
    }

    public interface OnItemClickListtener {
        public void ClickItemTabHost(int position);
    }

}
