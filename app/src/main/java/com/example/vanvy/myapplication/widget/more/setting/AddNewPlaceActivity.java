package com.example.vanvy.myapplication.widget.more.setting;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.UserClub;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.Keyboard;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EditorAction;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 10/15/2016.
 */
@EActivity(R.layout.activity_new_place)
public class AddNewPlaceActivity extends BaseActivity implements Constants, NewPlaceAdapter.ItemNewPlaceListener {
    @ViewById
    RecyclerView mRcvClub;
    @ViewById
    EditText mEdtSearchClub;
    @ViewById
    ProgressBar mPrgClub;
    @Extra
    ArrayList<Integer> idClubs;
    @ViewById
    TextView mTitleHeader;
    @ViewById
    TextView mTvRight;
    @ViewById
    ImageView mIvLeft;
    @ViewById
    ImageView mIvRight;

    private List<UserClub> mUserClubs = new ArrayList<>();
    private NewPlaceAdapter mAdapter;
    private List<UserClub> mUserClubTamps = new ArrayList<>();

    @AfterViews
    void afterViews() {
        mTitleHeader.setText("Add new place");
        mTvRight.setVisibility(View.GONE);
        mAdapter = new NewPlaceAdapter(this, mUserClubs, this);
        mRcvClub.setAdapter(mAdapter);
        mRcvClub.setLayoutManager(new LinearLayoutManager(this));
        getListClub();

    }

    @Click(R.id.mIvLeft)
    void onBackClick() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @TextChange(R.id.mEdtSearchClub)
    void onChangeTextSearch(CharSequence text) {
        searchClub();
    }

    @EditorAction(R.id.mEdtSearchClub)
    void onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            Keyboard.hideKeyboard(this, v);
            searchClub();
        }
    }

    private void searchClub() {
        String search = mEdtSearchClub.getText().toString();
        List<UserClub> clubTmps = new ArrayList<>();
        for (UserClub club : mUserClubTamps) {
            if (club.getmName().toLowerCase().contains(search) || club.getmAddress().toLowerCase().contains(search)) {
                clubTmps.add(club);
            }
        }
        if (mUserClubs.size() > 0) {
            mUserClubs.clear();
        }
        mUserClubs.addAll(clubTmps);
        mAdapter.notifyDataSetChanged();
    }

    private void getListClub() {
        mPrgClub.setVisibility(View.VISIBLE);
        final String search = mEdtSearchClub.getText().toString();
        List<UserClub> clubTmps = new ArrayList<>();
        RetrofitUtils.buildApiInterface(ApiConnect.class).getListClub(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
                new Callback<ArrayList<UserClub>>() {
                    @Override
                    public void success(final ArrayList<UserClub> clubs, Response response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mUserClubs.size() > 0) {
                                    mUserClubs.clear();
                                }
                                mPrgClub.setVisibility(View.GONE);
                                for (int i = 0; i < clubs.size(); i++) {
                                    for (int ii = 0; ii < idClubs.size(); ii++) {
                                        if (clubs.get(i).getmId() == idClubs.get(ii)) {
                                            clubs.remove(i);
                                        }
                                    }
                                }
                                mUserClubs.addAll(clubs);
                                mUserClubTamps.addAll(clubs);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mPrgClub.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onItemNewPlaceClick(int position) {
        updateNew(position);
    }

    private void updateNew(final int position) {
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect mApi = restAdapter.create(ApiConnect.class);
        mApi.updateNewPlace(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
                mUserClubs.get(position).getmId(), new Callback<Logout>() {
                    @Override
                    public void success(final Logout logout, Response response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mUserClubs.remove(position);
                                mUserClubTamps.remove(position);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AddNewPlaceActivity.this, "error: " + error.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
    }
}

