package com.example.vanvy.myapplication.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

/**
 * Utilities for getting ResultLocation
 */
public class LocationHelper implements LocationListener {
    /**
     * This class will callback after finish get Location.
     */
    public interface OnLocationUpdate {
        void onLocationUpdated(Location location);
    }

    // Time out request GPS, default is 1 seconds
    private static final int LOCATION_TIME_GPS_REQUEST = 500;
    // Time out request Network, default is 10 seconds
    private static final int LOCATION_TIME_NETWORK_REQUEST = 10 * 1000;
    // Time out request Passive, default is 10 seconds
    private static final int LOCATION_TIME_PASSIVE_REQUEST = 10 * 1000;
    // 0 seconds change will request again
    private static final int LOCATION_MINTIME_REQUEST = 0;
    // 0 meter change will request again
    private static final int LOCATION_DISTANCE_BETWEEN = 0;
    // 60 seconds will request again
    private static final int LOCATION_REFRESH_REQUEST = 60000;
    private static final String TAG = LocationHelper.class.getName();
    private OnLocationUpdate mListener;
    private LocationManager mLocationManagerService;
    private String mBestProvider;
    private boolean mIsListening = false;
    private Context mContext;
    private static Location sLastLocation;
    private static long sTimeRequest;

    // Handler When running request ResultLocation
    private Handler mHandlerTracker;
    private Runnable mRunnableTracker;

    public LocationHelper(Context context) {
        // Preparing param for get Location
        this.mContext = context;
        this.mLocationManagerService = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Used to check lat lng length, length limit is 12
     */
    private void callLocationUpdated(Location rootLocation) {
        final int maxLength = 12;
        double lat = rootLocation.getLatitude();
        double lng = rootLocation.getLongitude();

        String strLat = String.valueOf(lat);
        String strLng = String.valueOf(lng);

        try {
            // cut length, max is 12
            if (strLat.length() > maxLength) {
                strLat = strLat.substring(0, maxLength);
            }
            if (strLng.length() > maxLength) {
                strLng = strLng.substring(0, maxLength);
            }

            rootLocation.setLatitude(Double.parseDouble(strLat));
            rootLocation.setLongitude(Double.parseDouble(strLng));
        } catch (Exception e) {
            Log.e(TAG, "ERROR while parsing lat lng");
        }

        mListener.onLocationUpdated(rootLocation);
    }

    public void addRequestListener(OnLocationUpdate listener) {
        mListener = listener;
        mRunnableTracker = new Runnable() {
            @Override
            public void run() {
                switch (mBestProvider) {
                    case LocationManager.GPS_PROVIDER:
                        stopTracking();
                        mBestProvider = LocationManager.NETWORK_PROVIDER;
                        startTracking();
                        break;
                    case LocationManager.NETWORK_PROVIDER:
                        stopTracking();
                        mBestProvider = LocationManager.PASSIVE_PROVIDER;
                        startTracking();
                        break;
                    case LocationManager.PASSIVE_PROVIDER:
                    default:
                        stopTracking();
                        if (sLastLocation != null) {
                            callLocationUpdated(sLastLocation);
                        }
                        break;
                }
            }
        };
        mHandlerTracker = new Handler();
        startTrackingFirstTime();
    }

    /**
     * Make call request GPS first time
     */
    public void startTrackingFirstTime() {
        if ((System.currentTimeMillis() - sTimeRequest - LOCATION_REFRESH_REQUEST <= 0) && sLastLocation != null) {
            callLocationUpdated(sLastLocation);
            return;
        }
        sTimeRequest = System.currentTimeMillis();
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
        mBestProvider = mLocationManagerService.getBestProvider(criteria, true);
        Log.d(TAG, "startTrackingFirstTime: " + mBestProvider);
        if (TextUtils.isEmpty(mBestProvider)) {
            mBestProvider = LocationManager.NETWORK_PROVIDER;
        }
        // call Request first time
        startTracking();
    }

    /**
     * Make the trackers start listening for ResultLocation update
     */
    public void startTracking() {
        // Check Gps Enabled
        if (!GpsUtil.isGpsEnabled(mContext)) {
            return;
        }
        // Check permission
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.mIsListening = true;
        this.mLocationManagerService.requestLocationUpdates(mBestProvider, LOCATION_MINTIME_REQUEST,
                LOCATION_DISTANCE_BETWEEN, this);
        
        Log.d(TAG, "startTracking: " + mBestProvider);
        switch (mBestProvider) {
            case LocationManager.GPS_PROVIDER:
                mHandlerTracker.postDelayed(mRunnableTracker, LOCATION_TIME_GPS_REQUEST);
                break;
            case LocationManager.NETWORK_PROVIDER:
                sLastLocation = mLocationManagerService.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                mHandlerTracker.postDelayed(mRunnableTracker, LOCATION_TIME_NETWORK_REQUEST);
                break;
            default:
                mHandlerTracker.postDelayed(mRunnableTracker, LOCATION_TIME_PASSIVE_REQUEST);
                break;
        }
    }

    /**
     * While app listening.
     *
     * @return: true/false
     */
    public boolean isListening() {
        return mIsListening;
    }

    /**
     * Make the tracker stops listening for mLocation updates
     */
    public void stopTracking() {
        this.mIsListening = false;
        if (mHandlerTracker != null) {
            mHandlerTracker.removeCallbacks(mRunnableTracker);
        }
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManagerService.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: " + mBestProvider);
        sLastLocation = location;
        if (mHandlerTracker != null) {
            mHandlerTracker.removeCallbacks(mRunnableTracker);
        }
        callLocationUpdated(location);
        mIsListening = false;
        // Remove Update
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocationManagerService.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
