package com.example.vanvy.myapplication.widget.notification.profile;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.ProfileDancer;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 10/27/16.
 */
@EActivity(R.layout.activity_profile)
public class ProfileActivity extends BaseActivity implements Constants {
	@ViewById
	ImageView mIvAvatarDancer;
	@ViewById
	TextView mTvAddressDancer;
	@ViewById
	TextView mNameDancer;
	@ViewById
	ImageView mIvLeft;
	@ViewById
	ImageView mIvRight;
	@ViewById
	TextView mTitleHeader;
	@ViewById
	TextView mTvRight;
	@Extra
	int idStaff;

	@AfterViews
	void afterViews() {
		initHeader();
		getDataProfileStaff();
	}

	private void initHeader() {
		mTvRight.setVisibility(View.GONE);
		mIvRight.setVisibility(View.VISIBLE);
		mIvLeft.setVisibility(View.VISIBLE);
		mIvRight.setImageResource(R.mipmap.ic_menu);
	}

	private void getDataProfileStaff() {
		RetrofitUtils.buildApiInterface(ApiConnect.class).getDancerProfile(idStaff, SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(), new Callback<ProfileDancer>() {
			@Override
			public void success(ProfileDancer profileDancer, Response response) {
				mTitleHeader.setText(profileDancer.getmName());
				mNameDancer.setText(profileDancer.getmName());
				mTvAddressDancer.setText(profileDancer.getmCity() + ", " + profileDancer.getmCountry());
			}

			@Override
			public void failure(RetrofitError error) {

			}
		});
	}
}
