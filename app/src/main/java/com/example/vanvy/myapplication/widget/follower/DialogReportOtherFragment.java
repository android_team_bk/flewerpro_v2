package com.example.vanvy.myapplication.widget.follower;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.vanvy.myapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by asiantech on 9/21/16.
 */
@EFragment(R.layout.fragment_dialog_report_other)
public class DialogReportOtherFragment extends DialogFragment {
	@ViewById
	EditText mEdtReport;

	private SendReportOtherListener mListener;

	@AfterViews
	void afterViews() {
		Dialog dialog = getDialog();
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
	}
	@Click(R.id.mBtnReportOther)
	void onReportOtherClick() {
		mListener.sentReportOther(mEdtReport.getText().toString());
	}

	public void setUpdateReportOtherListener(SendReportOtherListener listener) {
		mListener = listener;
	}

	public interface SendReportOtherListener {
		void sentReportOther(String reason);
	}
}
