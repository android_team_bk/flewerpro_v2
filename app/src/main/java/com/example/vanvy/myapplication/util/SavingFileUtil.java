package com.example.vanvy.myapplication.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.mime.TypedFile;

public final class SavingFileUtil {
    /**
     * Suffix of photo
     */
    public enum PhotoSuffix {
        PNG(".png"), JPEG(".jpg");

        private final String value;

        PhotoSuffix(String paramValue) {
            this.value = paramValue;
        }

        public String getValue() {
            return value;
        }

    }

    public static final String TAG = SavingFileUtil.class.getSimpleName();
    public static final String APP_NAME = "Vivo";
    public static final String TAKE_APP_NAME = "Vivo_camera";

    private SavingFileUtil() {
        // no instance
    }

    /**
     * Create a file Uri for saving an image
     */
    public static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    public static File getOutputMediaFile() {
        return getOutputMediaFile(APP_NAME);
    }

    public static File getOutputMediaFileCamera() {
        return getOutputMediaFile(TAKE_APP_NAME);
    }

    /**
     * Create a File for saving an image
     */
    public static File getOutputMediaFile(String folderName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), folderName);

        // This ResultLocation works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.getDefault()).format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + folderName + ".jpg");
    }

    public static void writeBitmapToFile(Bitmap bitmap, File file) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bitmapData = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapData);
        fos.flush();
        fos.close();
    }

    public static TypedFile convertToFile( String path, Bitmap bitmap) {
        FileOutputStream out = null;
        File imageFileName = new File(path);
        try {
            out = new FileOutputStream(imageFileName);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
        } catch (IOException e) {
            Log.e(TAG, "Failed to convert image to JPEG", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "Failed to close output stream", e);
            }
        }
        return new TypedFile("image/jpeg", imageFileName);
    }

    public static void deleteFile(Context context, String path) {
        File fileDelete = new File(path);
        if (fileDelete.exists()) {
            if (path.contains(APP_NAME + ".jpg")) {
                if (fileDelete.delete()) {
                    Log.d(TAG, "file Deleted :" + path);
                    String where = MediaStore.Images.Media.DATA + "=?";
                    String[] selectionArgs = {path};
                    context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            where, selectionArgs);
                } else {
                    Log.d(TAG, "file not Deleted :" + path);
                }
            }
        }
    }

    /**
     * This method is used to save a bitmap to file.
     *
     * @param bitmap to save.
     * @return return file path.
     */
    public static String writeBitmapToFile(Context context,Bitmap bitmap, PhotoSuffix photoSuffix) {

        File file = getOutputMediaFile();
        if (file == null) {
            return null;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (photoSuffix == PhotoSuffix.PNG) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 95, bos);
        } else {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 99, bos);
        }
        byte[] bitmapData = bos.toByteArray();

        try {
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            Log.e(TAG, "Save bitmap to file error: " + e);
            return null;
        }

        // Mounted this photo to media
        // MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, null);

        return file.getAbsolutePath();
    }
}
