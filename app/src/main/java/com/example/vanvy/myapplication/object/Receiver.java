package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/26/16.
 */
public class Receiver implements Parcelable {
	@SerializedName("id")
	private String mId;
	@SerializedName("crm_ID")
	private String mCrmId;
	@SerializedName("name")
	private String mName;
	@SerializedName("logo")
	private String mLogo;
	@SerializedName("description")
	private String mDescription;
	@SerializedName("email")
	private String mEmail;
	@SerializedName("address")
	private String mAddress;
	@SerializedName("birthday")
	private String mBirthDay;
	@SerializedName("city")
	private String mCity;
	@SerializedName("country")
	private String mCountry;
	@SerializedName("state")
	private String mState;
	@SerializedName("lat")
	private Double mLat;
	@SerializedName("lng")
	private Double mLng;
	@SerializedName("zipcode")
	private String mZipcode;
	@SerializedName("tel")
	private String mTelephone;
	@SerializedName("site")
	private String mSite;
	@SerializedName("open_time")
	private String mOpenTime;
	@SerializedName("close_time")
	private String mCloseTime;
	@SerializedName("credits")
	private String mCredits;
	@SerializedName("active")
	private String mActive;
	@SerializedName("is_default")
	private boolean mIsDefault;
	@SerializedName("start_shift1")
	private String mStartShift1;
	@SerializedName("end_shift1")
	private String mEndShift1;
	@SerializedName("start_shift2")
	private String mStartShift2;
	@SerializedName("end_shift2")
	private String mEndShift2;
	@SerializedName("start_shift3")
	private String mStartShift3;
	@SerializedName("end_shift3")
	private String mEndShift3;
	@SerializedName("start_shift4")
	private String mStartShift4;
	@SerializedName("end_shift4")
	private String mEndShift4;
	@SerializedName("avatar")
	private String mAvatar;

	public Receiver(Parcel in) {
		mId = in.readString();
		mCrmId = in.readString();
		mName = in.readString();
		mLogo = in.readString();
		mDescription = in.readString();
		mEmail = in.readString();
		mAddress = in.readString();
		mBirthDay = in.readString();
		mCity = in.readString();
		mCountry = in.readString();
		mState = in.readString();
		mZipcode = in.readString();
		mTelephone = in.readString();
		mSite = in.readString();
		mOpenTime = in.readString();
		mCloseTime = in.readString();
		mCredits = in.readString();
		mActive = in.readString();
		mIsDefault = in.readByte() != 0;
		mStartShift1 = in.readString();
		mEndShift1 = in.readString();
		mStartShift2 = in.readString();
		mEndShift2 = in.readString();
		mStartShift3 = in.readString();
		mEndShift3 = in.readString();
		mStartShift4 = in.readString();
		mEndShift4 = in.readString();
		mAvatar = in.readString();
	}

	public static final Creator<Receiver> CREATOR = new Creator<Receiver>() {
		@Override
		public Receiver createFromParcel(Parcel in) {
			return new Receiver(in);
		}

		@Override
		public Receiver[] newArray(int size) {
			return new Receiver[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(mCrmId);
		dest.writeString(mName);
		dest.writeString(mLogo);
		dest.writeString(mDescription);
		dest.writeString(mEmail);
		dest.writeString(mAddress);
		dest.writeString(mBirthDay);
		dest.writeString(mCity);
		dest.writeString(mCountry);
		dest.writeString(mState);
		dest.writeString(mZipcode);
		dest.writeString(mTelephone);
		dest.writeString(mSite);
		dest.writeString(mOpenTime);
		dest.writeString(mCloseTime);
		dest.writeString(mCredits);
		dest.writeString(mActive);
		dest.writeByte((byte) (mIsDefault ? 1 : 0));
		dest.writeString(mStartShift1);
		dest.writeString(mEndShift1);
		dest.writeString(mStartShift2);
		dest.writeString(mEndShift2);
		dest.writeString(mStartShift3);
		dest.writeString(mEndShift3);
		dest.writeString(mStartShift4);
		dest.writeString(mEndShift4);
		dest.writeString(mAvatar);
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmCrmId() {
		return mCrmId;
	}

	public void setmCrmId(String mCrmId) {
		this.mCrmId = mCrmId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmLogo() {
		return mLogo;
	}

	public void setmLogo(String mLogo) {
		this.mLogo = mLogo;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public String getmEmail() {
		return mEmail;
	}

	public void setmEmail(String mEmail) {
		this.mEmail = mEmail;
	}

	public String getmAddress() {
		return mAddress;
	}

	public void setmAddress(String mAddress) {
		this.mAddress = mAddress;
	}

	public String getmBirthDay() {
		return mBirthDay;
	}

	public void setmBirthDay(String mBirthDay) {
		this.mBirthDay = mBirthDay;
	}

	public String getmCity() {
		return mCity;
	}

	public void setmCity(String mCity) {
		this.mCity = mCity;
	}

	public String getmCountry() {
		return mCountry;
	}

	public void setmCountry(String mCountry) {
		this.mCountry = mCountry;
	}

	public String getmState() {
		return mState;
	}

	public void setmState(String mState) {
		this.mState = mState;
	}

	public Double getmLat() {
		return mLat;
	}

	public void setmLat(Double mLat) {
		this.mLat = mLat;
	}

	public Double getmLng() {
		return mLng;
	}

	public void setmLng(Double mLng) {
		this.mLng = mLng;
	}

	public String getmZipcode() {
		return mZipcode;
	}

	public void setmZipcode(String mZipcode) {
		this.mZipcode = mZipcode;
	}

	public String getmTelephone() {
		return mTelephone;
	}

	public void setmTelephone(String mTelephone) {
		this.mTelephone = mTelephone;
	}

	public String getmSite() {
		return mSite;
	}

	public void setmSite(String mSite) {
		this.mSite = mSite;
	}

	public String getmOpenTime() {
		return mOpenTime;
	}

	public void setmOpenTime(String mOpenTime) {
		this.mOpenTime = mOpenTime;
	}

	public String getmCloseTime() {
		return mCloseTime;
	}

	public void setmCloseTime(String mCloseTime) {
		this.mCloseTime = mCloseTime;
	}

	public String getmCredits() {
		return mCredits;
	}

	public void setmCredits(String mCredits) {
		this.mCredits = mCredits;
	}

	public String getmActive() {
		return mActive;
	}

	public void setmActive(String mActive) {
		this.mActive = mActive;
	}

	public boolean ismIsDefault() {
		return mIsDefault;
	}

	public void setmIsDefault(boolean mIsDefault) {
		this.mIsDefault = mIsDefault;
	}

	public String getmStartShift1() {
		return mStartShift1;
	}

	public void setmStartShift1(String mStartShift1) {
		this.mStartShift1 = mStartShift1;
	}

	public String getmEndShift1() {
		return mEndShift1;
	}

	public void setmEndShift1(String mEndShift1) {
		this.mEndShift1 = mEndShift1;
	}

	public String getmStartShift2() {
		return mStartShift2;
	}

	public void setmStartShift2(String mStartShift2) {
		this.mStartShift2 = mStartShift2;
	}

	public String getmEndShift2() {
		return mEndShift2;
	}

	public void setmEndShift2(String mEndShift2) {
		this.mEndShift2 = mEndShift2;
	}

	public String getmStartShift3() {
		return mStartShift3;
	}

	public void setmStartShift3(String mStartShift3) {
		this.mStartShift3 = mStartShift3;
	}

	public String getmEndShift3() {
		return mEndShift3;
	}

	public void setmEndShift3(String mEndShift3) {
		this.mEndShift3 = mEndShift3;
	}

	public String getmStartShift4() {
		return mStartShift4;
	}

	public void setmStartShift4(String mStartShift4) {
		this.mStartShift4 = mStartShift4;
	}

	public String getmEndShift4() {
		return mEndShift4;
	}

	public void setmEndShift4(String mEndShift4) {
		this.mEndShift4 = mEndShift4;
	}

	public String getmAvatar() {
		return mAvatar;
	}

	public void setmAvatar(String mAvatar) {
		this.mAvatar = mAvatar;
	}
}
