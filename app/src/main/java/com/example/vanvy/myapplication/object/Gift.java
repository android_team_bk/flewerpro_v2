package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/16/16.
 */
public class Gift implements Parcelable {
    @SerializedName("id")
    private int mId;
    @SerializedName("gift")
    private String mGift;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("icon")
    private String mUrl;
    private boolean isChoose;

    protected Gift(Parcel in) {
        mId = in.readInt();
        mGift = in.readString();
        mPrice = in.readString();
        mUrl = in.readString();
    }

    public static final Creator<Gift> CREATOR = new Creator<Gift>() {
        @Override
        public Gift createFromParcel(Parcel in) {
            return new Gift(in);
        }

        @Override
        public Gift[] newArray(int size) {
            return new Gift[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mGift);
        dest.writeString(mPrice);
        dest.writeString(mUrl);
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmGift() {
        return mGift;
    }

    public void setmGift(String mGift) {
        this.mGift = mGift;
    }

    public String getmPrice() {
        return mPrice;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public boolean isChoose() {
        return isChoose;
    }

    public void setChoose(boolean choose) {
        isChoose = choose;
    }
}
