package com.example.vanvy.myapplication.base;

import android.support.v4.app.Fragment;

/**
 * Created by asiantech on 9/12/16.
 */
public class BaseFragment extends Fragment {

    public void finish() {
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.finish();
        }
    }

    public void setTitle(String title, boolean isShowRight, boolean isShowLeft, boolean isShowTitleRight) {
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.setTitle(title, isShowRight, isShowLeft, isShowTitleRight);
        }
    }
}
