package com.example.vanvy.myapplication.widget.more.staff;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 10/13/2016.
 */
@EFragment(R.layout.fragment_staff)
public class StaffFragment extends BaseFragment {
}
