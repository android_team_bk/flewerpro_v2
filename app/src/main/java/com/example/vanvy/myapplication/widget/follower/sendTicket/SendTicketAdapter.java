package com.example.vanvy.myapplication.widget.follower.sendTicket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.FollowPerformers;

/**
 * Created by asiantech on 9/19/16.
 */
public class SendTicketAdapter extends BaseAdapter {
    private FollowPerformers mFollowPerformer;
    private Context mContext;

    protected SendTicketAdapter(Context context, FollowPerformers followPerformer) {
        super(context);
        mContext = context;
        mFollowPerformer = followPerformer;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class ItemFollowerViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvAvatar;
        TextView mTvNameFollower;
        CheckBox mCbChoose;

        public ItemFollowerViewHolder(View itemView) {
            super(itemView);
            mIvAvatar = (ImageView) itemView.findViewById(R.id.mIvAvatarFollow);
            mTvNameFollower = (TextView) itemView.findViewById(R.id.mTvNameFollower);
            mCbChoose = (CheckBox) itemView.findViewById(R.id.mCbChoose);
        }
    }
}
