package com.example.vanvy.myapplication.widget.more.gallery;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.ProgressedTypedFile;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by vanvy on 10/19/2016.
 */
@EActivity(R.layout.activity_add_gallery)
public class AddGalleryActivity extends BaseActivity implements Constants {

    @ViewById
    ImageView mIvLeft;
    @ViewById
    ImageView mIvRight;
    @ViewById
    TextView mTitleHeader;
    @ViewById
    TextView mTvRight;
    @ViewById
    ImageView mIvGallery;
    @ViewById
    TextView mBtnPhoto;
    @ViewById
    TextView mBtnVideo;
    @ViewById
    AppCompatEditText mEdtPrice;
    @ViewById
    TextView mTvModify;
    @ViewById
    ProgressBar mPrgUploadFile;
    private String mSelectedImagePath;
    private ProgressedTypedFile mMedia;
    private int mType;
    private int mPrice;
    private boolean mTypeMedia;
    private long mDuration;

    @AfterViews
    void afterViews() {
        initHeader();
        mEdtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mType = s.length() == 0 ? 0 : 1;
                mTvModify.setText(s.length() == 0 ? "Public" : "Private");
            }
        });
    }

    @Click(R.id.mIvLeft)
    void onBackClick() {
        onBackPressed();
    }

    @Click(R.id.mBtnPhoto)
    void onPhotoClick() {
        mTypeMedia = true;
        gallery("image");
    }

    @Click(R.id.mBtnVideo)
    void onVideoClick() {
        mTypeMedia = false;
        gallery("video");
    }

    @Click(R.id.mBtnSubmit)
    void onSubmitClick() {
        if (mIvGallery.getDrawable() != null) {
            mPrice = mEdtPrice.getText().toString().length() == 0 ? 0 : Integer.parseInt(mEdtPrice.getText().toString());
            ProgressedTypedFile.Listener listener = new ProgressedTypedFile.Listener() {
                @Override
                public void onUpdateProgress(int percentage) {
                    Log.v("update", "progress update " + percentage + "/100%");
                    // todo show dialog here and inside method uploadFile for user waiting
                }
            };
            if (mTypeMedia) {
                mMedia = new ProgressedTypedFile("image/jpeg", new File(mSelectedImagePath), listener);
            } else {
                mMedia = new ProgressedTypedFile("video/mp4", new File(mSelectedImagePath), listener);
            }
            uploadFile();
        } else {
            Toast.makeText(this, "You must choose image or video", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void initHeader() {
        mTvRight.setVisibility(View.GONE);
        mIvRight.setVisibility(View.INVISIBLE);
        mIvLeft.setVisibility(View.VISIBLE);
        mTitleHeader.setText("Gallery");
        mIvRight.setImageResource(R.drawable.ic_add_club);
    }

    private void gallery(String typeGallery) {
        if (typeGallery.equals("image")) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_CODE_GALLERY_IMAGE);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_CODE_GALLERY_VIDEO);
        }
    }

    @OnActivityResult(REQUEST_CODE_GALLERY_VIDEO)
    void onVideoResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri thumnailUri = data.getData();
            getDuration(data.getData());
            mSelectedImagePath = getPathVideo(thumnailUri);
            Picasso.with(this).load(thumnailUri).resize(300, 300).into(mIvGallery);
        }
    }

    @OnActivityResult(REQUEST_CODE_GALLERY_IMAGE)
    void onImageResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            Picasso.with(this).load(selectedImageUri).resize(300, 300).into(mIvGallery);
            mSelectedImagePath = getPath(selectedImageUri);
        }
    }

    private void uploadFile() {

        if (!mTypeMedia && (mDuration / 1000) >= 30) {
            Toast.makeText(this, "The maximum time is 30s", Toast.LENGTH_SHORT).show();
        } else {
            sentFile();
        }
    }

    private void sentFile() {
        Helpers.disableTouchOnScreen(this, true);
        mPrgUploadFile.setVisibility(View.VISIBLE);
        RestAdapter adapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint("https://flewer.site/flewer_live/api_android/1.0").build();
        ApiConnect api = adapter.create(ApiConnect.class);
        api.getAddGallery(SharedPreferences.getUserProfile(AddGalleryActivity.this, KEY_USER_PROFILE).getmId(),
                mMedia, mType, mPrice, new Callback<Logout>() {
                    @Override
                    public void success(final Logout logout, Response response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(AddGalleryActivity.this, false);
                                mPrgUploadFile.setVisibility(View.GONE);
                                if (logout.getmMessage().equals("Success")) {
                                    onBackPressed();
                                    Toast.makeText(AddGalleryActivity.this, "Add gallery successful", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(AddGalleryActivity.this, false);
                                mPrgUploadFile.setVisibility(View.GONE);
                                Toast.makeText(AddGalleryActivity.this, "error: " + RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPathVideo(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA, MediaStore.Video.VideoColumns.DURATION, MediaStore.Video.Thumbnails.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void getDuration(Uri data) {
        Cursor cursor = MediaStore.Video.query(getContentResolver(), data,
                new String[]{MediaStore.Video.VideoColumns.DURATION});
        cursor.moveToFirst();
        mDuration = cursor.getLong(cursor.getColumnIndex("duration"));
    }
}
