package com.example.vanvy.myapplication.util;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.mime.TypedByteArray;

/**
 * Created By topcbl on 3/14/16.
 * Call Me: +84 1644450063
 * Contact Me: topcbl@gmail.com
 * Who Am I: http://topcbl.xyz
 */
public final class RetrofitUtils {

    /**
     * Create a api interface to do a restful webservice request which use with mock server.
     * @param serviceInterface
     * @param <T>
     * @return
     */
//    public static <T> T buildMockApiInterface(final Class<T> serviceInterface) {
//        // Create OkHttpClient
//        OkHttpClient okHttpClient = new OkHttpClient();
//        okHttpClient.setConnectTimeout(20, TimeUnit.SECONDS);
//        okHttpClient.setReadTimeout(20, TimeUnit.SECONDS);
//
//        // Add Cache-Control Interceptor
//        okHttpClient.networkInterceptors().add(new ApiInterceptor());
//
//        // Create Executor
//        Executor executor = Executors.newCachedThreadPool();
//
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setEndpoint(Constants.MOCK_SERVER)
//                .setExecutors(executor, executor)
//                .setClient(new OkClient(okHttpClient))
//                .setLogLevel(RestAdapter.LogLevel.FULL)
//                .build();
//        return restAdapter.create(serviceInterface);
//    }

    /**
     * Create a api interface to do a restful webservice request
     * @param serviceInterface
     * @param <T>
     * @return
     */
    public static <T> T buildApiInterface(final Class<T> serviceInterface) {
        // Create OkHttpClient
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLSv1");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            okHttpClient.setSslSocketFactory(sslSocketFactory);

            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // Add Cache-Control Interceptor
        okHttpClient.networkInterceptors().add(new ApiInterceptor());

        // Create Executor
        Executor executor = Executors.newCachedThreadPool();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ApiUtils.HOST)
                .setExecutors(executor, executor)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        return restAdapter.create(serviceInterface);
    }

    /**
     * Get message from body data in RetrofitError callback
     * @param error Get this error from <b>public void failure </b> method in callback
     * @return error message
     */
    public static String getErrorMessage(RetrofitError error) {
        if (error.getResponse() != null) {
            return new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
        } else {
            return "Error with: " + error.getUrl();
        }
    }

}
