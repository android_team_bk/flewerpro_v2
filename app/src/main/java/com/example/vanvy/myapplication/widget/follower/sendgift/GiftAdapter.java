package com.example.vanvy.myapplication.widget.follower.sendgift;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.util.ScreenSize;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.ItemSendRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 9/16/16.
 */
public class GiftAdapter extends BaseAdapter<GiftAdapter.GiftViewHolder> {

    List<Gift> mGifts;
    OnItemGiftListener mListener;
    int mItemSize;
    List<ItemSendRequest> mItemSendRequests;

    public GiftAdapter(Context context, @NonNull List<Gift> gifts, OnItemGiftListener listener) {
        super(context);
        mGifts = gifts;
        mListener = listener;
        mItemSize = (ScreenSize.getScreenWidth(context) - ScreenSize.convertDPToPixels(context, 25)) / 4;
    }

    @Override
    public GiftViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GiftViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_gift, parent, false));
    }

    @Override
    public void onBindViewHolder(GiftViewHolder holder, int position) {
        onBindGiftViewHolder(holder, mGifts.get(position));
    }

    private void onBindGiftViewHolder(GiftViewHolder holder, Gift gift) {
        holder.mTvPrice.setText(gift.getmPrice());
        holder.mLlGift.setBackgroundResource(gift.isChoose() ? R.color.color_request : R.color.color_white);
        Picasso.with(getContext())
                .load(gift.getmUrl())
                .into(holder.mIvGift);
    }

    @Override
    public int getItemCount() {
        return mGifts.size();
    }

    class GiftViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvGift;
        TextView mTvPrice;
        LinearLayout mLlGift;

        public GiftViewHolder(View itemView) {
            super(itemView);
            ViewGroup.LayoutParams params = itemView.getLayoutParams();
            params.width = mItemSize;
            mIvGift = (ImageView) itemView.findViewById(R.id.mIvGift);
            mTvPrice = (TextView) itemView.findViewById(R.id.mTvPrice);
            mLlGift = (LinearLayout) itemView.findViewById(R.id.mLlGift);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemGiftClick(getLayoutPosition());
                }
            });
        }
    }

    public interface OnItemGiftListener {
        void onItemGiftClick(int position);
    }
}
