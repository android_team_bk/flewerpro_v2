package com.example.vanvy.myapplication.util;

/**
 * Created by vanvy on 4/3/2016.
 */
public class ApiUtils {

//    public static final String HOST="https://flewer.site/flewer/api/1.0";//test
    public static final String HOST="https://flewer.site/flewer_live/api_android/1.0";//product
    public static final String REGISTER="/register";
    public static final String LOGIN="/login";
    public static final String LOGIN_FACEBOOK="/login-facebook";
    public static final String LOGOUT="/logout";
    public static final String NEARBY="/dancer";
    public static final String FOLLOW="/matches";
    public static final String INVITE="/invite/list";
    public static final String CLUB="/clubs";
    public static final String PROFILE_DANCER="/profile/dancer";
    public static final String PURCHASE="/gallery/purchase";
    public static final String FOLLOW_CLUB="/club-feed/toggle-follow";
    public static final String STAFF="/club-manage";
    public static final String FAVORITE="/club-feed/toggle-like";
    public static final String START_CHAT="/chat/start";
    public static final String BASE_URL_SEARCH_LOCATION="https://maps.google.com";
}
