package com.example.vanvy.myapplication.widget.ticket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.ClubPerforming;

import java.util.ArrayList;

/**
 * Created by vanvy on 11/6/2016.
 */

public class ClubAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ClubPerforming> mClubPerformings;
    private ItemClubListener mListener;

    public ClubAdapter(Context context, ArrayList<ClubPerforming> clubPerformings, ItemClubListener listener) {
        super(context);
        mContext = context;
        mClubPerformings = clubPerformings;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemClubViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_job, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindItemClubHolder((ItemClubViewHolder) holder, position);
    }

    private void onBindItemClubHolder(ItemClubViewHolder holder, int position) {
        holder.mTvJob.setText(mClubPerformings.get(position).getClub().getmName());
    }

    @Override
    public int getItemCount() {
        return mClubPerformings.size();
    }

    private class ItemClubViewHolder extends RecyclerView.ViewHolder {
        TextView mTvJob;

        public ItemClubViewHolder(View itemView) {
            super(itemView);
            mTvJob = (TextView) itemView.findViewById(R.id.mTvJob);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.itemClubClick(mClubPerformings.get(getLayoutPosition()).getClub().getmName());
                }
            });
        }
    }

    public interface ItemClubListener {
        void itemClubClick(String clubName);
    }
}
