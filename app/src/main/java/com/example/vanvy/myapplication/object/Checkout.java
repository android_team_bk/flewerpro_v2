package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanvy on 11/8/2016.
 */

public class Checkout implements Parcelable {
    @SerializedName("id")
    private int id;
    @SerializedName("club_id")
    private int clubId;
    @SerializedName("time_past")
    private String timePast;
    @SerializedName("club_name")
    private String clubName;
    @SerializedName("club_avatar")
    private String clubAvatar;

    public Checkout() {
    }

    protected Checkout(Parcel in) {
        id = in.readInt();
        clubId = in.readInt();
        timePast = in.readString();
        clubName = in.readString();
        clubAvatar = in.readString();
    }

    public static final Creator<Checkout> CREATOR = new Creator<Checkout>() {
        @Override
        public Checkout createFromParcel(Parcel in) {
            return new Checkout(in);
        }

        @Override
        public Checkout[] newArray(int size) {
            return new Checkout[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClubId() {
        return clubId;
    }

    public void setClubId(int clubId) {
        this.clubId = clubId;
    }

    public String getTimePast() {
        return timePast;
    }

    public void setTimePast(String timePast) {
        this.timePast = timePast;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubAvatar() {
        return clubAvatar;
    }

    public void setClubAvatar(String clubAvatar) {
        this.clubAvatar = clubAvatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(clubId);
        parcel.writeString(timePast);
        parcel.writeString(clubName);
        parcel.writeString(clubAvatar);
    }
}
