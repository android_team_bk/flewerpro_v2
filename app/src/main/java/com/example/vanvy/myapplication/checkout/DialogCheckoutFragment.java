package com.example.vanvy.myapplication.checkout;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.object.Checkout;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vanvy on 10/16/2016.
 */
@EFragment(R.layout.fragment_dialog_checkout)
public class DialogCheckoutFragment extends DialogFragment {

    @FragmentArg
    Checkout checkout;
    @ViewById
    TextView mTvClubCheckout;
    @ViewById
    ImageView mIvClub;

    @AfterViews
    void afterViews() {
        String text = "Our system detects that you haven't been checked out by " + checkout.getClubName() +
                ". If your work time ended, you can checkout.";
        mTvClubCheckout.setText(text, TextView.BufferType.SPANNABLE);
        Spannable s = (Spannable) mTvClubCheckout.getText();
        s.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.color_selector)), 56, 56 + checkout.getClubName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        Picasso.with(getActivity()).load(checkout.getClubAvatar()).into(mIvClub);
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
    }
}
