package com.example.vanvy.myapplication.object;

/**
 * Created by asiantech on 9/12/16.
 */
public class HeaderSetting {
    String name;
    String urlIvAvatar;


    public HeaderSetting(String name, String urlIvAvatar){
        this.name=name;
        this.urlIvAvatar=urlIvAvatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlIvAvatar() {
        return urlIvAvatar;
    }

    public void setUrlIvAvatar(String urlIvAvatar) {
        this.urlIvAvatar = urlIvAvatar;
    }
}
