package com.example.vanvy.myapplication.widget.notification;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.NotificationObject;
import com.example.vanvy.myapplication.util.Helpers;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by vanvy on 7/1/2016.
 */
public class NotificationAdapter extends BaseAdapter {
    List<NotificationObject> mNotifications;
    private Context mContext;
    public NotificationAdapter(Context context, List<NotificationObject> notifications) {
        super(context);
        mNotifications = notifications;
        mContext=context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindNotificationViewHolder((NotificationViewHolder) holder, position);
    }

    private void onBindNotificationViewHolder(NotificationViewHolder holder, int position) {
        if (position == (mNotifications.size() - 1)) {
            holder.mViewBottom.setVisibility(View.GONE);
        } else {
            holder.mViewBottom.setVisibility(View.VISIBLE);
        }
        holder.mTvTitleNotification.setText(mNotifications.get(position).getDisplay());
        Calendar mCalendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String mCurrentDate = sdf.format(new Date());
        String mCreatedDate = mNotifications.get(position).getCreatedAt();
        long diff = 0;
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = sdf.parse(mCreatedDate);
            Calendar mCreatedAt = Calendar.getInstance();
            //Change to Calendar Date
            mCreatedAt.setTime(startDate);

            endDate = sdf.parse(mCurrentDate);
            Calendar mCurrent = Calendar.getInstance();
            //Change to Calendar Date
            mCurrent.setTime(endDate);
            //get Time in milli seconds
            long ms1 = mCreatedAt.getTimeInMillis();
            long ms2 = mCurrent.getTimeInMillis();
            //get difference in milli seconds
            diff = ms2 - ms1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mTvTime.setText(Helpers.getTimeString(diff));
        Picasso.with(mContext)
                .load(mNotifications.get(position).getAvatar())
                .into(holder.mIvNotification);
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvNotification;
        TextView mTvTitleNotification;
        TextView mTvTime;
        View mViewBottom;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            mIvNotification = (ImageView) itemView.findViewById(R.id.mIvNotificationTab);
            mTvTitleNotification = (TextView) itemView.findViewById(R.id.mTvTitleNotificationTab);
            mTvTime = (TextView) itemView.findViewById(R.id.mTvTimeNotificationTab);
            mViewBottom = itemView.findViewById(R.id.mViewBottom);

        }
    }
    public interface LoadMoreListener {
        void onLoadMore();
    }
}
