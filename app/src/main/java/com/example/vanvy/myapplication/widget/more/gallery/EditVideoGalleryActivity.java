package com.example.vanvy.myapplication.widget.more.gallery;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.object.Picture;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.ScreenSize;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 10/28/16.
 */
@EActivity(R.layout.activity_edit_gallery)
public class EditVideoGalleryActivity extends BaseActivity {
	@ViewById
	ImageView mIvLeft;
	@ViewById
	ImageView mIvRight;
	@ViewById
	TextView mTitleHeader;
	@ViewById
	TextView mTvRight;
	@ViewById
	ImageView mIvGallery;
	@ViewById
	EditText mEdtPrice;
	@ViewById
	TextView mTvModify;

	@Extra()
	Picture picture;

	private int mType;
	private int mPrice;

	@TargetApi(Build.VERSION_CODES.KITKAT)
	@AfterViews
	void afterViews() {
		initHeader();
		Uri uri = Uri.parse(picture.getmPhoto());
		mIvGallery.setImageBitmap(Helpers.getVideoFrame(this, uri));
		mEdtPrice.setText(picture.getmPrice());
		mTvModify.setText(Integer.parseInt(picture.getmPrivate()) == 0 ? "Public" : "Private");
		mEdtPrice.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				mType = s.length() == 0 ? 0 : 1;
				mTvModify.setText(s.length() == 0 || Integer.parseInt(s.toString()) == 0 ? "Public" : "Private");
			}
		});
	}

	private void initHeader() {
		mTvRight.setVisibility(View.GONE);
		mIvRight.setVisibility(View.INVISIBLE);
		mIvLeft.setVisibility(View.VISIBLE);
		mTitleHeader.setText("Edit");
	}

	@Click(R.id.mBtnUpdate)
	void onUpdateGallery() {
		mPrice = mEdtPrice.getText().toString().length() == 0 ? 0 : Integer.parseInt(mEdtPrice.getText().toString());
		RetrofitUtils.buildApiInterface(ApiConnect.class).getUpdateGallery(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
				Integer.parseInt(picture.getmId()), mType, mPrice, new Callback<String>() {
					@Override
					public void success(final String success, Response response) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(EditVideoGalleryActivity.this, success, Toast.LENGTH_SHORT).show();
							}
						});
					}

					@Override
					public void failure(RetrofitError error) {

					}
				});
	}

	@Click(R.id.mIvLeft)
	void onBackClick() {
		onBackPressed();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
}
