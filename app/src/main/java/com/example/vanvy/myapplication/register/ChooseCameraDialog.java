package com.example.vanvy.myapplication.register;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.activity_choose_camera)
public class ChooseCameraDialog extends DialogFragment {
    @ViewById()
    TextView mTvDismiss;
    @ViewById()
    TextView mTvLibs;
    @ViewById()
    TextView mTvCamera;
    private onDialogDismiss mOnDialogDismiss;

    @AfterViews
    void afterViews() {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Click(R.id.mViewRoot)
    void onViewRootClick() {
    }

    @Click(R.id.mTvCamera)
    void onCameraClick() {
        mOnDialogDismiss.onDismissCameraListener();
    }

    @Click(R.id.mTvLibs)
    void onGallery() {
        mOnDialogDismiss.onDismissGalleryListener();
    }

    @Click(R.id.mTvDismiss)
    void onDismissClick() {
        getDialog().dismiss();
    }

    public void setUpCameraListener(onDialogDismiss listener) {
        mOnDialogDismiss = listener;
    }

    public interface onDialogDismiss {
        void onDismissCameraListener();
        void onDismissGalleryListener();
    }
}
