package com.example.vanvy.myapplication.register;

import android.app.Dialog;
import android.app.DialogFragment;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by asiantech on 9/7/16.
 */
@EFragment(R.layout.fragment_dialog_gender)
public class DialogFragmentGender extends DialogFragment {
    @ViewById()
    TextView mTvMale;
    @ViewById()
    TextView mTvFemale;
    @ViewById()
    TextView mTvOther;
    private onGenderListener mListener;
    private String mGender = "";

    @AfterViews
    void afterViews() {
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
    }

    public void onGenderListener(onGenderListener listener) {
        this.mListener = listener;
    }

    @Click(R.id.mTvMale)
    void onMaleClick() {
        mListener.onGenderClick(mTvMale.getText().toString());
    }

    @Click(R.id.mTvFemale)
    void onFemalelick() {
        mListener.onGenderClick(mTvFemale.getText().toString());
    }

    @Click(R.id.mTvOther)
    void onOtherClick() {
        mListener.onGenderClick(mTvOther.getText().toString());
    }
}
