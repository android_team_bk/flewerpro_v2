package com.example.vanvy.myapplication.eventBus;

import com.example.vanvy.myapplication.object.UserClub;

/**
 * Created by vanvy on 10/16/2016.
 */
public class SettingEvent {
    private UserClub club;

    public SettingEvent(UserClub club){
        this.club=club;
    }

    public UserClub getClub() {
        return club;
    }

    public void setClub(UserClub club) {
        this.club = club;
    }
}
