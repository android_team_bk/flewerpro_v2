package com.example.vanvy.myapplication.widget.more.new_request;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.NewRequest;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 9/15/2016.
 */
@EFragment(R.layout.fragment_request)
public class NewRequestFragment extends BaseFragment implements Constants {
    @ViewById
    RecyclerView mRcvRequest;

    @FragmentArg
    ArrayList<Gift> gifts;

    private List<NewRequest> mNewRequest = new ArrayList<>();
    private NewRequestAdapter mNewRequestAdapter;
    private Activity mActivity;
    private int uId;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        mNewRequestAdapter = new NewRequestAdapter(getActivity(), mNewRequest, gifts);
        mRcvRequest.setAdapter(mNewRequestAdapter);
        mRcvRequest.setLayoutManager(new LinearLayoutManager(getActivity()));
        getRequestPending();
    }

    private void getRequestPending() {
        RetrofitUtils.buildApiInterface(ApiConnect.class).getRequestPending(uId, new Callback<List<NewRequest>>() {
            @Override
            public void success(final List<NewRequest> newRequest, Response response) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mNewRequest.addAll(newRequest);
                        mNewRequestAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void failure(final RetrofitError error) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("xxxx", "" + error.toString());
                        Toast.makeText(getActivity(), "Error: " + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
