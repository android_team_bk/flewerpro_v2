package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserClub implements Parcelable {
    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("crm_ID")
    private String mCrmId;
    @SerializedName("logo")
    private String mLogo;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("address")
    private String mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("state")
    private String mState;
    @SerializedName("gmt")
    private String gmt;
    @SerializedName("slight")
    private String slight;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("lng")
    private String mLng;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("zipcode")
    private String mZipCode;
    @SerializedName("tel")
    private String mTel;
    @SerializedName("site")
    private String mSite;
    @SerializedName("open_time")
    private String mOpenTime;
    @SerializedName("close_time")
    private String mCloseTime;
    @SerializedName("credits")
    private Integer mCredits;
    @SerializedName("active")
    private int mActive;
    @SerializedName("is_default")
    private int mIs;
    @SerializedName("start_shift1")
    private String startShift1;
    @SerializedName("end_shift1")
    private String endShift1;
    @SerializedName("start_shift2")
    private String startShift2;
    @SerializedName("end_shift2")
    private String endShift2;
    @SerializedName("start_shift3")
    private String startShift3;
    @SerializedName("end_shift3")
    private String endShift3;
    @SerializedName("start_shift4")
    private String startShift4;
    @SerializedName("end_shift4")
    private String endShift4;
    @SerializedName("blocked")
    private int blocked;
    @SerializedName("background_image")
    private String backgroundImage;
    @SerializedName("backend")
    private int backend;
    @SerializedName("item1")
    private int item1;
    @SerializedName("item2")
    private int item2;
    @SerializedName("item3")
    private int item3;
    @SerializedName("item4")
    private int item4;
    @SerializedName("item5")
    private int item5;
    @SerializedName("item6")
    private int item6;
    @SerializedName("featured")
    private String featured;
    @SerializedName("superadmin")
    private String superadmin;
    @SerializedName("feetrack")
    private String feetrack;
    @SerializedName("avatar")
    private String mAvatar;

    public UserClub() {
    }

    protected UserClub(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mCrmId = in.readString();
        mLogo = in.readString();
        mDescription = in.readString();
        mEmail = in.readString();
        mAddress = in.readString();
        mCity = in.readString();
        mState = in.readString();
        gmt = in.readString();
        slight = in.readString();
        mCountry = in.readString();
        mLng = in.readString();
        mLat = in.readString();
        mZipCode = in.readString();
        mTel = in.readString();
        mSite = in.readString();
        mOpenTime = in.readString();
        mCloseTime = in.readString();
        mActive = in.readInt();
        mIs = in.readInt();
        startShift1 = in.readString();
        endShift1 = in.readString();
        startShift2 = in.readString();
        endShift2 = in.readString();
        startShift3 = in.readString();
        endShift3 = in.readString();
        startShift4 = in.readString();
        endShift4 = in.readString();
        blocked = in.readInt();
        backgroundImage = in.readString();
        backend = in.readInt();
        item1 = in.readInt();
        item2 = in.readInt();
        item3 = in.readInt();
        item4 = in.readInt();
        item5 = in.readInt();
        item6 = in.readInt();
        featured = in.readString();
        superadmin = in.readString();
        feetrack = in.readString();
        mAvatar = in.readString();
    }

    public static final Creator<UserClub> CREATOR = new Creator<UserClub>() {
        @Override
        public UserClub createFromParcel(Parcel in) {
            return new UserClub(in);
        }

        @Override
        public UserClub[] newArray(int size) {
            return new UserClub[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mCrmId);
        dest.writeString(mLogo);
        dest.writeString(mDescription);
        dest.writeString(mEmail);
        dest.writeString(mAddress);
        dest.writeString(mCity);
        dest.writeString(mState);
        dest.writeString(gmt);
        dest.writeString(slight);
        dest.writeString(mCountry);
        dest.writeString(mLng);
        dest.writeString(mLat);
        dest.writeString(mZipCode);
        dest.writeString(mTel);
        dest.writeString(mSite);
        dest.writeString(mOpenTime);
        dest.writeString(mCloseTime);
        dest.writeInt(mActive);
        dest.writeInt(mIs);
        dest.writeString(startShift1);
        dest.writeString(endShift1);
        dest.writeString(startShift2);
        dest.writeString(endShift2);
        dest.writeString(startShift3);
        dest.writeString(endShift3);
        dest.writeString(startShift4);
        dest.writeString(endShift4);
        dest.writeInt(blocked);
        dest.writeString(backgroundImage);
        dest.writeInt(backend);
        dest.writeInt(item1);
        dest.writeInt(item2);
        dest.writeInt(item3);
        dest.writeInt(item4);
        dest.writeInt(item5);
        dest.writeInt(item6);
        dest.writeString(featured);
        dest.writeString(superadmin);
        dest.writeString(feetrack);
        dest.writeString(mAvatar);
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCrmId() {
        return mCrmId;
    }

    public void setmCrmId(String mCrmId) {
        this.mCrmId = mCrmId;
    }

    public String getmLogo() {
        return mLogo;
    }

    public void setmLogo(String mLogo) {
        this.mLogo = mLogo;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public String getGmt() {
        return gmt;
    }

    public void setGmt(String gmt) {
        this.gmt = gmt;
    }

    public String getSlight() {
        return slight;
    }

    public void setSlight(String slight) {
        this.slight = slight;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmLng() {
        return mLng;
    }

    public void setmLng(String mLng) {
        this.mLng = mLng;
    }

    public String getmLat() {
        return mLat;
    }

    public void setmLat(String mLat) {
        this.mLat = mLat;
    }

    public String getmZipCode() {
        return mZipCode;
    }

    public void setmZipCode(String mZipCode) {
        this.mZipCode = mZipCode;
    }

    public String getmTel() {
        return mTel;
    }

    public void setmTel(String mTel) {
        this.mTel = mTel;
    }

    public String getmSite() {
        return mSite;
    }

    public void setmSite(String mSite) {
        this.mSite = mSite;
    }

    public String getmOpenTime() {
        return mOpenTime;
    }

    public void setmOpenTime(String mOpenTime) {
        this.mOpenTime = mOpenTime;
    }

    public String getmCloseTime() {
        return mCloseTime;
    }

    public void setmCloseTime(String mCloseTime) {
        this.mCloseTime = mCloseTime;
    }

    public Integer getmCredits() {
        return mCredits;
    }

    public void setmCredits(Integer mCredits) {
        this.mCredits = mCredits;
    }

    public int getmActive() {
        return mActive;
    }

    public void setmActive(int mActive) {
        this.mActive = mActive;
    }

    public int getmIs() {
        return mIs;
    }

    public void setmIs(int mIs) {
        this.mIs = mIs;
    }

    public String getStartShift1() {
        return startShift1;
    }

    public void setStartShift1(String startShift1) {
        this.startShift1 = startShift1;
    }

    public String getEndShift1() {
        return endShift1;
    }

    public void setEndShift1(String endShift1) {
        this.endShift1 = endShift1;
    }

    public String getStartShift2() {
        return startShift2;
    }

    public void setStartShift2(String startShift2) {
        this.startShift2 = startShift2;
    }

    public String getEndShift2() {
        return endShift2;
    }

    public void setEndShift2(String endShift2) {
        this.endShift2 = endShift2;
    }

    public String getStartShift3() {
        return startShift3;
    }

    public void setStartShift3(String startShift3) {
        this.startShift3 = startShift3;
    }

    public String getEndShift3() {
        return endShift3;
    }

    public void setEndShift3(String endShift3) {
        this.endShift3 = endShift3;
    }

    public String getStartShift4() {
        return startShift4;
    }

    public void setStartShift4(String startShift4) {
        this.startShift4 = startShift4;
    }

    public String getEndShift4() {
        return endShift4;
    }

    public void setEndShift4(String endShift4) {
        this.endShift4 = endShift4;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public int getBackend() {
        return backend;
    }

    public void setBackend(int backend) {
        this.backend = backend;
    }

    public int getItem1() {
        return item1;
    }

    public void setItem1(int item1) {
        this.item1 = item1;
    }

    public int getItem2() {
        return item2;
    }

    public void setItem2(int item2) {
        this.item2 = item2;
    }

    public int getItem3() {
        return item3;
    }

    public void setItem3(int item3) {
        this.item3 = item3;
    }

    public int getItem4() {
        return item4;
    }

    public void setItem4(int item4) {
        this.item4 = item4;
    }

    public int getItem5() {
        return item5;
    }

    public void setItem5(int item5) {
        this.item5 = item5;
    }

    public int getItem6() {
        return item6;
    }

    public void setItem6(int item6) {
        this.item6 = item6;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getSuperadmin() {
        return superadmin;
    }

    public void setSuperadmin(String superadmin) {
        this.superadmin = superadmin;
    }

    public String getFeetrack() {
        return feetrack;
    }

    public void setFeetrack(String feetrack) {
        this.feetrack = feetrack;
    }

    public String getmAvatar() {
        return mAvatar;
    }

    public void setmAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }
}