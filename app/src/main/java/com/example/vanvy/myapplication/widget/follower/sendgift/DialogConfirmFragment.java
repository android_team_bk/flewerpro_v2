package com.example.vanvy.myapplication.widget.follower.sendgift;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by vynv on 5/18/16.
 */
@EFragment(R.layout.fragment_dialog_cofirm)
public class DialogConfirmFragment extends DialogFragment {
	@ViewById()
	TextView mTvDetail;
	@FragmentArg
	public String mContentDialog;
	@FragmentArg
	boolean isSuccess;
	OnConfirmSendListener mListener;

	@AfterViews
	void afterViews() {
		Log.d("xxxSend", "" + mContentDialog);
		mTvDetail.setText(mContentDialog);
		Dialog dialog = getDialog();
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
	}

	@Click(R.id.mTvOk)
	void onDoneClick() {
		if (isSuccess) {
			mListener.onSendClick();
		} else {
			mListener.onCancel();
		}
	}

	public void setOnDialogListener(OnConfirmSendListener listener) {
		mListener = listener;
	}

	public interface OnConfirmSendListener {
		void onSendClick();
		void onCancel();
	}
}
