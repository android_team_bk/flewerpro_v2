package com.example.vanvy.myapplication.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.widget.dashboard.ScheduleFragment_;

/**
 * Created by vanvy on 9/4/2016.
 */
public class BaseActivity extends AppCompatActivity implements Constants {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setTitle(String title, boolean isShowRight, boolean isShowLeft, boolean isShowTextRight) {
        View v = findViewById(R.id.mHeaderBar);
        ImageView mIvRight = (ImageView) v.findViewById(R.id.mIvRight);
        ImageView mIvLeft = (ImageView) v.findViewById(R.id.mIvLeft);
        TextView mTitleHeader = (TextView) v.findViewById(R.id.mTitleHeader);
        final TextView mTvRight = (TextView) v.findViewById(R.id.mTvRight);
        if (isShowRight) {
            mIvRight.setImageResource(R.mipmap.ic_calendar);
            mIvRight.setVisibility(View.VISIBLE);
            mTvRight.setVisibility(View.GONE);
        } else {
            mTvRight.setVisibility(View.GONE);
            mIvRight.setVisibility(View.INVISIBLE);
        }
        if (isShowLeft) {
            mIvLeft.setVisibility(View.VISIBLE);
        } else {
            mIvLeft.setVisibility(View.INVISIBLE);
        }
        if (isShowTextRight) {
            mTvRight.setVisibility(View.VISIBLE);
        } else {
            mTvRight.setVisibility(View.GONE);
        }
        mTitleHeader.setVisibility(View.VISIBLE);
        mTitleHeader.setText(title);
        mIvRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTitle("Schedules", false, false, false);
                replaceFragment(ScheduleFragment_.builder().build());
            }
        });
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void replaceFragment(Fragment newFragment) {
        android.support.v4.app.FragmentTransaction trasection =
                getSupportFragmentManager().beginTransaction();
        if (!newFragment.isAdded()) {
            try {
                //FragmentTransaction trasection =
                getFragmentManager().beginTransaction();
                trasection.replace(R.id.mFrMainContainer, newFragment);
                trasection.addToBackStack(null);
                trasection.commit();

            } catch (Exception e) {
                // TODO: handle exception
                //AppConstants.printLog(e.getMessage());

            }
        } else
            trasection.show(newFragment);

    }

    public void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
