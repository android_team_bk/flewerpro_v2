package com.example.vanvy.myapplication.widget.more.new_request;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.NewRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asiantech on 10/10/16.
 */
public class NewRequestAdapter extends BaseAdapter {
	private List<NewRequest> mNewquests;
	private Context mContext;
	private ItemGiftAdapter mItemGiftAdapter;
	private ArrayList<Gift> mGiftsArray;

	public NewRequestAdapter(Context context, List<NewRequest> newRequests, @NonNull ArrayList<Gift> gifts) {
		super(context);
		mContext = context;
		mNewquests = newRequests;
		mGiftsArray = gifts;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewRequestHolder(LayoutInflater.from(mContext).inflate(R.layout.item_new_request, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindRequestViewHolder((ViewRequestHolder) holder, position);
	}

	private void onBindRequestViewHolder(ViewRequestHolder holder, int position) {
		Picasso.with(mContext).load(mNewquests.get(position).getSenders().getmAvatar()).into(holder.mIvAvatar);
		holder.mTvName.setText(mNewquests.get(position).getSenders().getmName());
		String[] gifts = mNewquests.get(position).getGifts().split(",");
		List<Gift> giftList = new ArrayList<>();
		for (int i = 0; i < gifts.length; i++) {
			for (Gift gift : mGiftsArray) {
				if (Integer.parseInt(gifts[i]) == gift.getmId()) {
					giftList.add(gift);
				}
			}
		}
		mItemGiftAdapter = new ItemGiftAdapter(mContext, giftList);
		holder.mRcvGift.setAdapter(mItemGiftAdapter);
		holder.mRcvGift.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
		mItemGiftAdapter.notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return mNewquests.size();
	}

	class ViewRequestHolder extends RecyclerView.ViewHolder {
		ImageView mIvAvatar;
		TextView mTvName;
		RecyclerView mRcvGift;
		ImageView mIvCancel;
		ImageView mIvAccept;

		public ViewRequestHolder(View itemView) {
			super(itemView);
			mIvAvatar = (ImageView) itemView.findViewById(R.id.mIvAvatar);
			mIvCancel = (ImageView) itemView.findViewById(R.id.mIvCancel);
			mIvAccept = (ImageView) itemView.findViewById(R.id.mIvAccept);
			mTvName = (TextView) itemView.findViewById(R.id.mTvName);
			mRcvGift = (RecyclerView) itemView.findViewById(R.id.mRcvGift);
		}
	}
}
