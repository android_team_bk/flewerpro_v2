package com.example.vanvy.myapplication.widget.notification.profile;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.annotations.EFragment;

/**
 * Created by asiantech on 10/27/16.
 */
@EFragment(R.layout.fragment_media)
public class MediaProfileStaffFragment extends BaseFragment implements Constants {
}
