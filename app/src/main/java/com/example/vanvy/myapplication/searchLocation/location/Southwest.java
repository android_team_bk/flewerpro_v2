package com.example.vanvy.myapplication.searchLocation.location;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/6/16.
 */
public class Southwest implements Parcelable{
    @SerializedName("lat")
    private Double lat;
    @SerializedName("lng")
    private Double lng;

    protected Southwest(Parcel in) {
    }

    public static final Creator<Southwest> CREATOR = new Creator<Southwest>() {
        @Override
        public Southwest createFromParcel(Parcel in) {
            return new Southwest(in);
        }

        @Override
        public Southwest[] newArray(int size) {
            return new Southwest[size];
        }
    };

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
