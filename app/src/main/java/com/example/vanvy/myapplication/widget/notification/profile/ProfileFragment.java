package com.example.vanvy.myapplication.widget.notification.profile;

import android.support.v4.view.ViewPager;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.customView.TabHost;
import com.example.vanvy.myapplication.object.ProfileDancer;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by asiantech on 10/27/16.
 */
@EFragment(R.layout.fragment_profile_dancer)
public class ProfileFragment extends BaseFragment implements TabHost.OnItemClickListtener {
	@FragmentArg
	ProfileDancer mProfileDancer;
	PageProfileAdapter mPageAdapter;
	@ViewById()
	ViewPager mViewPage;
	@ViewById()
	TabHost mTabStrip;

	@Override
	public void ClickItemTabHost(int position) {

	}
}
