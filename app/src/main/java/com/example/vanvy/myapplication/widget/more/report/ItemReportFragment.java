package com.example.vanvy.myapplication.widget.more.report;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vanvy on 10/23/2016.
 */
@EFragment(R.layout.fragment_item_report)
public class ItemReportFragment extends BaseFragment {
}
