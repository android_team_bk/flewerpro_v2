package com.example.vanvy.myapplication.searchLocation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.searchLocation.location.Result;

import java.util.List;

/**
 * Created by asiantech on 9/6/16.
 */
public class SearchAdapter extends BaseAdapter {
    private List<Result> mResultLocations;
    private ResultLocationListener mListener;

    protected SearchAdapter(Context context, List<Result> resultLocations, ResultLocationListener listener) {
        super(context);
        mResultLocations = resultLocations;
        mListener = listener;
        Log.d("TAG", "SearchAdapter: " + mResultLocations.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ResultSearchViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_search_location, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindResultLocationViewHolder((ResultSearchViewHolder) holder, mResultLocations.get(position).getFormattedAddress());
    }

    private void onBindResultLocationViewHolder(ResultSearchViewHolder holder, String address) {
        Log.d("TAG", "Onbind: " + address);
        holder.mTvAddress.setText(address);
    }

    @Override
    public int getItemCount() {
        return mResultLocations.size();
    }

    class ResultSearchViewHolder extends RecyclerView.ViewHolder {
        TextView mTvAddress;

        public ResultSearchViewHolder(View itemView) {
            super(itemView);
            mTvAddress = (TextView) itemView.findViewById(R.id.mTvAddress);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemResultLocationClick(mResultLocations.get(getLayoutPosition()).getAddressComponents());
                }
            });
        }
    }
}
