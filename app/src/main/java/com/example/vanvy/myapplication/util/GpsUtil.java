package com.example.vanvy.myapplication.util;

import android.content.Context;
import android.location.LocationManager;

/**
 * Utilities for GPS
 */
public final class GpsUtil {

    private GpsUtil() {
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isNetworkEnabled || isGPSEnabled;
    }
}
