package com.example.vanvy.myapplication.widget.more.setting;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.eventBus.BusProvider;
import com.example.vanvy.myapplication.eventBus.SettingEvent;
import com.example.vanvy.myapplication.eventBus.SettingStaffEvent;
import com.example.vanvy.myapplication.object.UserClub;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 9/15/2016.
 */
@EFragment(R.layout.fragment_setting)
public class SettingFragment extends BaseFragment implements Constants {
    @ViewById
    SwitchCompat mSwProfile;
    @ViewById
    SwitchCompat mSwPrivate;
    @ViewById
    EditText mEdtCostGift;
    @ViewById
    EditText mEdtMaxChat;
    @ViewById
    TextView mEdtTicketGift;
    @ViewById
    TextView mTvClubName;
    @ViewById()
    ProgressBar mPrgClub;
    private UserClub mUserClub;
    private Activity mActivity;
    private UserProfile mUserProfile = new UserProfile();
    private boolean isCheckClub = true;
    private ClubListener mListener;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        mUserProfile = SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE);
        setTitle("Settings", false, false, true);
        if (SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmProfile() == 1) {
            mSwProfile.setChecked(true);
        } else {
            mSwProfile.setChecked(false);
        }
        if (SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmPrivate() == 1) {
            mSwPrivate.setChecked(true);
        } else {
            mSwPrivate.setChecked(false);
        }
        if (isCheckClub) {
            if (SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmClub() != null) {
                mTvClubName.setText(SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmClub().getmName());
            } else {
                mTvClubName.setText("Select a club");
            }
            isCheckClub = false;
        }
        mEdtCostGift.setText("" + SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmMinGift());
        mEdtMaxChat.setText("" + SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmMaxSession());
        mEdtTicketGift.setText("" + SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmFeeTicketUsed());
        mUserClub = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmClub();
    }

    @Click(R.id.mTvClubName)
    void onClubClick() {
//        getListClub();
        mListener.clubClick();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE_SETTING) {
            Toast.makeText(mActivity, "Ok aaaa " + data.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void settingUpdate(SettingEvent event) {
        if (event.getClub().getmId() != 0) {
            mTvClubName.setText(event.getClub().getmName());
            mUserClub = event.getClub();
        }
    }

    @Subscribe
    public void settingStaff(SettingStaffEvent event) {
        final int profile = mSwProfile.isChecked() ? 1 : 0;
        final int mPrivate = mSwPrivate.isChecked() ? 1 : 0;
        if (event.isClicked()) {
            RetrofitUtils.buildApiInterface(ApiConnect.class).settingStaff(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmId()
                    , profile, mPrivate, mEdtCostGift.getText().toString(), mEdtMaxChat.getText().toString(), mUserClub.getmId(), new Callback<Void>() {
                        @Override
                        public void success(Void aVoid, Response response) {
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mActivity, "Update Setting success", Toast.LENGTH_SHORT).show();
                                    mUserProfile.setmProfile(profile);
                                    mUserProfile.setmPrivate(mPrivate);
                                    mUserProfile.setmClub(mUserClub);
                                    SharedPreferences.remove(mActivity, KEY_USER_PROFILE);
                                    SharedPreferences.saveUserProfile(mActivity, mUserProfile, KEY_USER_PROFILE);
                                }
                            });
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
        }
    }

    public void setUpListener(ClubListener listener) {
        mListener = listener;
    }

    public interface ClubListener {
        void clubClick();
    }
}
