package com.example.vanvy.myapplication.widget.message;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Message;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_message)
public class MessageFragment extends BaseFragment implements Constants, MessageAdapter.OnItemMessageListener {
    @ViewById
    RecyclerView mRcvListMessage;
    @ViewById
    ProgressBar mPrgMessage;
    private List<Message> mMessages = new ArrayList<>();
    private MessageAdapter mAdapter;
    private Activity mActivity;
    private int uId;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
        }
        mAdapter = new MessageAdapter(mActivity, mMessages, this);
        mRcvListMessage.setAdapter(mAdapter);
        mRcvListMessage.setLayoutManager(new LinearLayoutManager(mActivity));
        getListMessage();
    }

    private void getListMessage() {
        Helpers.disableTouchOnScreen(getActivity(), true);
        mPrgMessage.setVisibility(View.VISIBLE);
        RetrofitUtils.buildApiInterface(ApiConnect.class).getChatList(uId,
                SharedPreferences.getTypeLogin(mActivity, TYPE_LOGIN), new Callback<List<Message>>() {
                    @Override
                    public void success(final List<Message> messages, Response response) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(getActivity(), false);
                                mPrgMessage.setVisibility(View.GONE);
                                mMessages.addAll(messages);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Helpers.disableTouchOnScreen(getActivity(), false);
                                mPrgMessage.setVisibility(View.GONE);
                            }
                        });
                    }
                });
    }

    @Override
    public void onItemMessageClick(int position) {
        Log.d("xxx", "onItemMessageClick: /" + mMessages.get(position).getId());
    }
}
