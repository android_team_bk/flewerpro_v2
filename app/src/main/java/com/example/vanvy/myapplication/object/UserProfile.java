package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserProfile implements Parcelable {
    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("realname")
    private String mRealName;
    @SerializedName("surname")
    private String mSurName;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("age")
    private int mAge;
    @SerializedName("birthday")
    private String mBirthDay;
    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("state")
    private String mState;
    @SerializedName("gender")
    private int mGender;
    @SerializedName("telephone")
    private String mTelephone;
    @SerializedName("lat")
    private Double mLat;
    @SerializedName("lng")
    private Double mLng;
    @SerializedName("credits")
    private String mCredits;
    @SerializedName("last_access")
    private String mLastAccess;
    @SerializedName("app_id")
    private int mAppId;
    @SerializedName("facebook_id")
    private Long mFacebookId;
    @SerializedName("looking")
    private int mLooking;
    @SerializedName("verified")
    private int mVerified;
    @SerializedName("lang")
    private int mLang;
    @SerializedName("admin")
    private int mAdmin;
    @SerializedName("rule")
    private int mRule;
    @SerializedName("profile")
    private int mProfile;
    @SerializedName("private")
    private int mPrivate;
    @SerializedName("min_gift")
    private int mMinGift;
    @SerializedName("max_session")
    private int mMaxSession;
    @SerializedName("approve")
    private int mApprove;
    @SerializedName("fee_ticket_sent")
    private String mFeeTicketSent;
    @SerializedName("club_id")
    private String mClubId;
    @SerializedName("fee_ticket_used")
    private String mFeeTicketUsed;
    @SerializedName("inout")
    private int mInout;
    @SerializedName("token")
    private String mToken;
    @SerializedName("relation_status")
    private int mRelationStatus;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("club")
    private UserClub mClub;

    public UserProfile() {
    }

    public UserProfile(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mRealName = in.readString();
        mSurName = in.readString();
        mEmail = in.readString();
        mAge = in.readInt();
        mBirthDay = in.readString();
        mTelephone = in.readString();
        mCity = in.readString();
        mCountry = in.readString();
        mState = in.readString();
        mGender = in.readInt();
        mCredits = in.readString();
        mLastAccess = in.readString();
        mAppId = in.readInt();
        mLooking = in.readInt();
        mVerified = in.readInt();
        mLang = in.readInt();
        mAdmin = in.readInt();
        mRule = in.readInt();
        mProfile = in.readInt();
        mPrivate = in.readInt();
        mMinGift = in.readInt();
        mMaxSession = in.readInt();
        mApprove = in.readInt();
        mFeeTicketSent = in.readString();
        mClubId = in.readString();
        mFeeTicketUsed = in.readString();
        mInout = in.readInt();
        mToken = in.readString();
        mRelationStatus = in.readInt();
        mAvatar = in.readString();
        mClub = in.readParcelable(UserClub.class.getClassLoader());
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mRealName);
        dest.writeString(mSurName);
        dest.writeString(mEmail);
        dest.writeString(mTelephone);
        dest.writeInt(mAge);
        dest.writeString(mBirthDay);
        dest.writeString(mCity);
        dest.writeString(mCountry);
        dest.writeString(mState);
        dest.writeInt(mGender);
        dest.writeString(mCredits);
        dest.writeString(mLastAccess);
        dest.writeInt(mAppId);
        dest.writeInt(mLooking);
        dest.writeInt(mVerified);
        dest.writeInt(mLang);
        dest.writeInt(mAdmin);
        dest.writeInt(mRule);
        dest.writeInt(mProfile);
        dest.writeInt(mPrivate);
        dest.writeInt(mMinGift);
        dest.writeInt(mMaxSession);
        dest.writeInt(mApprove);
        dest.writeString(mFeeTicketSent);
        dest.writeString(mClubId);
        dest.writeString(mFeeTicketUsed);
        dest.writeInt(mInout);
        dest.writeString(mToken);
        dest.writeInt(mRelationStatus);
        dest.writeString(mAvatar);
        dest.writeParcelable(mClub, flags);
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmRealName() {
        return mRealName;
    }

    public void setmRealName(String mRealName) {
        this.mRealName = mRealName;
    }

    public String getmSurName() {
        return mSurName;
    }

    public void setmSurName(String mSurName) {
        this.mSurName = mSurName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public int getmAge() {
        return mAge;
    }

    public void setmAge(int mAge) {
        this.mAge = mAge;
    }

    public String getmBirthDay() {
        return mBirthDay;
    }

    public void setmBirthDay(String mBirthDay) {
        this.mBirthDay = mBirthDay;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public int getmGender() {
        return mGender;
    }

    public void setmGender(int mGender) {
        this.mGender = mGender;
    }

    public Double getmLat() {
        return mLat;
    }

    public void setmLat(Double mLat) {
        this.mLat = mLat;
    }

    public Double getmLng() {
        return mLng;
    }

    public void setmLng(Double mLng) {
        this.mLng = mLng;
    }

    public String getmCredits() {
        return mCredits;
    }

    public void setmCredits(String mCredits) {
        this.mCredits = mCredits;
    }

    public String getmLastAccess() {
        return mLastAccess;
    }

    public void setmLastAccess(String mLastAccess) {
        this.mLastAccess = mLastAccess;
    }

    public int getmAppId() {
        return mAppId;
    }

    public void setmAppId(int mAppId) {
        this.mAppId = mAppId;
    }

    public Long getmFacebookId() {
        return mFacebookId;
    }

    public void setmFacebookId(Long mFacebookId) {
        this.mFacebookId = mFacebookId;
    }

    public int getmLooking() {
        return mLooking;
    }

    public void setmLooking(int mLooking) {
        this.mLooking = mLooking;
    }

    public int getmVerified() {
        return mVerified;
    }

    public void setmVerified(int mVerified) {
        this.mVerified = mVerified;
    }

    public int getmLang() {
        return mLang;
    }

    public void setmLang(int mLang) {
        this.mLang = mLang;
    }

    public int getmAdmin() {
        return mAdmin;
    }

    public void setmAdmin(int mAdmin) {
        this.mAdmin = mAdmin;
    }

    public int getmRule() {
        return mRule;
    }

    public void setmRule(int mRule) {
        this.mRule = mRule;
    }

    public int getmProfile() {
        return mProfile;
    }

    public void setmProfile(int mProfile) {
        this.mProfile = mProfile;
    }

    public int getmPrivate() {
        return mPrivate;
    }

    public void setmPrivate(int mPrivate) {
        this.mPrivate = mPrivate;
    }

    public int getmMinGift() {
        return mMinGift;
    }

    public void setmMinGift(int mMinGift) {
        this.mMinGift = mMinGift;
    }

    public int getmMaxSession() {
        return mMaxSession;
    }

    public void setmMaxSession(int mMaxSession) {
        this.mMaxSession = mMaxSession;
    }

    public int getmApprove() {
        return mApprove;
    }

    public void setmApprove(int mApprove) {
        this.mApprove = mApprove;
    }

    public String getmFeeTicketSent() {
        return mFeeTicketSent;
    }

    public void setmFeeTicketSent(String mFeeTicketSent) {
        this.mFeeTicketSent = mFeeTicketSent;
    }

    public String getmClubId() {
        return mClubId;
    }

    public void setmClubId(String mClubId) {
        this.mClubId = mClubId;
    }

    public String getmFeeTicketUsed() {
        return mFeeTicketUsed;
    }

    public void setmFeeTicketUsed(String mFeeTicketUsed) {
        this.mFeeTicketUsed = mFeeTicketUsed;
    }

    public int getmInout() {
        return mInout;
    }

    public void setmInout(int mInout) {
        this.mInout = mInout;
    }

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }

    public int getmRelationStatus() {
        return mRelationStatus;
    }

    public void setmRelationStatus(int mRelationStatus) {
        this.mRelationStatus = mRelationStatus;
    }

    public String getmAvatar() {
        return mAvatar;
    }

    public void setmAvatar(String mAvatar) {
        this.mAvatar = mAvatar;
    }

    public UserClub getmClub() {
        return mClub;
    }

    public void setmClub(UserClub mClub) {
        this.mClub = mClub;
    }

    public String getmTelephone() {
        return mTelephone;
    }

    public void setmTelephone(String mTelephone) {
        this.mTelephone = mTelephone;
    }
}
