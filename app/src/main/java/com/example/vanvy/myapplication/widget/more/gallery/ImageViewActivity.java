package com.example.vanvy.myapplication.widget.more.gallery;

import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.Picture;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.ScreenSize;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Random;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by vynv on 4/19/16.
 */
@EActivity(R.layout.activity_image_view)
public class ImageViewActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {
	@ViewById
	ImageView mIvLeft;
	@ViewById
	ImageView mIvRight;
	@ViewById
	TextView mTitleHeader;
	@ViewById
	TextView mTvRight;
	@ViewById()
	ImageView mIvView;
	@Extra()
	Picture picture;

	private int heightBitmap;
	private int widthBitmap;
	private File folder;

	@AfterViews
	void afterViews() {
		initHeader();
		final int mWidth = ScreenSize.getScreenWidth(getBaseContext()) - 40;
		Picasso.with(this)
				.load(picture.getmPhoto())
				.error(R.mipmap.ic_avatar)
				.into(new Target() {
					@Override
					public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
						heightBitmap = bitmap.getHeight();
						widthBitmap = bitmap.getWidth();
						mIvView.getLayoutParams().width = mWidth;
						mIvView.getLayoutParams().height = mWidth * heightBitmap / widthBitmap;
						mIvView.setImageBitmap(bitmap);
					}

					@Override
					public void onBitmapFailed(Drawable errorDrawable) {

					}

					@Override
					public void onPrepareLoad(Drawable placeHolderDrawable) {

					}
				});
	}

	private void initHeader() {
		mTvRight.setVisibility(View.GONE);
		mIvRight.setVisibility(View.VISIBLE);
		mIvLeft.setVisibility(View.VISIBLE);
		mTitleHeader.setText("");
		mIvRight.setImageResource(R.mipmap.ic_menu);
	}

	@Click(R.id.mIvRight)
	void onMenuClick(View v) {
		PopupMenu popup = new PopupMenu(this, v);
		MenuInflater inflater = popup.getMenuInflater();
		inflater.inflate(R.menu.menu_image_gallery, popup.getMenu());
		popup.show();
		popup.setOnMenuItemClickListener(this);
	}

	@Click(R.id.mIvLeft)
	void onLeftClick() {
		onBackPressed();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.edit:
				editGallery();
				return true;
			case R.id.delete:
				onDelete();
				return true;
			case R.id.wallpaper:
				setWallpaper();
				return true;
			case R.id.download:
				onDownload();
				return true;
			default:
				return false;
		}
	}

	private void onDownload() {
		folder = new File(Environment.getExternalStorageDirectory() +
				File.separator + "Flewer_Pro");
		boolean success = true;
		if (!folder.exists()) {
			success = folder.mkdirs();
		}
		if (success) {
			myAsyncTask downloadTask = new myAsyncTask();
			downloadTask.execute();
		} else {
			// Do something else on failure
			Toast.makeText(this, "Created folder error", Toast.LENGTH_SHORT).show();
		}
	}

	private void setWallpaper() {
		Bitmap bitmap = ((BitmapDrawable) mIvView.getDrawable()).getBitmap();
		WallpaperManager myWallpaperManager
				= WallpaperManager.getInstance(getApplicationContext());
		try {
			myWallpaperManager.setBitmap(bitmap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void editGallery() {
		EditGalleryActivity_.intent(this).picture(picture).start();
	}

	private void onDelete() {
		RestAdapter adapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
		ApiConnect api = adapter.create(ApiConnect.class);
		api.deleteGallery(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(), Integer.parseInt(picture.getmId()),
				new Callback<Logout>() {
					@Override
					public void success(Logout logout, Response response) {
						Toast.makeText(ImageViewActivity.this, "Delete successful.", Toast.LENGTH_SHORT).show();
						onBackPressed();
					}

					@Override
					public void failure(RetrofitError error) {

					}
				});
	}

	class myAsyncTask extends AsyncTask<Void, Void, Void> {
		TextView tv;
		public ProgressDialog dialog;

		myAsyncTask() {
			dialog = new ProgressDialog(ImageViewActivity.this);
			dialog.setMessage("Loading news...");
			dialog.setCancelable(true);
			dialog.setIndeterminate(true);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Toast.makeText(ImageViewActivity.this, "Image downloaded in folder Flewer_Pro", Toast.LENGTH_SHORT).show();
			dialog.dismiss();

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.show();
		}

		protected Void doInBackground(Void... arg0) {
			try {
				//set the download URL, a url that points to a file on the internet
				//this is the file to be downloaded
				URL url = new URL(picture.getmPhoto());

				//create the new connection
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

				//set up some things on the connection
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(true);

				//and connect!
				urlConnection.connect();

				//set the path where we want to save the file
				//in this case, going to save it on the root directory of the
				//sd card.
//				File SDCardRoot = Environment.getExternalStorageDirectory()+"/Flewer_Pro";
				//create a new file, specifying the path, and the filename
				//which we want to save the file as.
				Long tsLong = System.currentTimeMillis()/1000;
				File file = new File(folder, "image-" + tsLong + ".jpg");

				//this will be used to write the downloaded data into the file we created
				FileOutputStream fileOutput = new FileOutputStream(file);

				//this will be used in reading the data from the internet
				InputStream inputStream = urlConnection.getInputStream();

				//this is the total size of the file
				int totalSize = urlConnection.getContentLength();
				//variable to store total downloaded bytes
				int downloadedSize = 0;

				//create a buffer...
				byte[] buffer = new byte[1024];
				int bufferLength = 0; //used to store a temporary size of the buffer

				//now, read through the input buffer and write the contents to the file
				while ((bufferLength = inputStream.read(buffer)) > 0) {
					//add the data in the buffer to the file in the file output stream (the file on the sd card
					fileOutput.write(buffer, 0, bufferLength);
					//add up the size so we know how much is downloaded
					downloadedSize += bufferLength;
					//this is where you would do something to report the prgress, like this maybe
					//updateProgress(downloadedSize, totalSize);

				}
				//close the output stream when done
				fileOutput.close();

				//catch some possible errors...
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
