package com.example.vanvy.myapplication.widget.follower.sendTicket;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.register.ChooseCameraDialog;
import com.example.vanvy.myapplication.register.ChooseCameraDialog_;
import com.example.vanvy.myapplication.util.ProgressedTypedFile;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment_;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.vanvy.myapplication.util.Helpers.disableTouchOnScreen;
import static com.example.vanvy.myapplication.util.Helpers.getImageOrientation;
import static com.example.vanvy.myapplication.util.Helpers.getOutputMediaFileUri;
import static com.example.vanvy.myapplication.util.Helpers.getTime;

/**
 * Created by asiantech on 9/19/16.
 */
@EActivity(R.layout.activity_send_ticket)
public class SendTicketActivity extends BaseActivity implements Constants, ChooseCameraDialog.onDialogDismiss,
        DialogConfirmFragment.OnConfirmSendListener {

    @ViewById
    ImageView mIvAvatarFollow;
    @ViewById
    TextView mTvNameFollower;
    @ViewById
    CheckBox mCbChoose;
    @ViewById
    TextView mTvDate;
    @ViewById
    TextView mTvTime;
    @ViewById
    TextView mNameClub;
    @ViewById
    ImageView mIvCamera;
    @ViewById
    EditText mEdtMessage;
    @ViewById
    ImageView mIvAvatarTicket;
    @ViewById
    ProgressBar mPrgSendTicket;

    @Extra()
    int idFollow;
    @Extra
    String avatar;
    @Extra
    String nameFollow;

    private int yearNow;
    private int yearAge;
    private int month;
    private int day;
    private int hour;
    private int min;
    private TimePicker timepicker;
    private ChooseCameraDialog mChooseCameraDialog;
    private Uri mImageUri;
    private String mBase64Avatar = "";
    private ProgressedTypedFile fileAvatar = null;
    private DialogConfirmFragment mDialog;
    private int uId;

    @AfterViews
    void afterViews() {
        if (SharedPreferences.getTypeLogin(this, TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId();
        } else {
            uId = SharedPreferences.getClubProfile(this, KEY_CLUB_PROFILE).getmId();
        }
        setTitle("Send ticket", false, true, false);
        View v = findViewById(R.id.mHeaderBar);
        ImageView mIvLeft = (ImageView) v.findViewById(R.id.mIvLeft);
        mIvLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Toast.makeText(SendTicketActivity.this, "" + idFollow, Toast.LENGTH_SHORT).show();
        Picasso.with(this).load(avatar).into(mIvAvatarFollow);
        mTvNameFollower.setText(nameFollow);
        if (SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClub() != null) {
            mNameClub.setText(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClub().getmName());
        }
        final Calendar c = Calendar.getInstance();
        yearNow = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hour = c.getTime().getHours();
        min = c.getTime().getMinutes();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Click(R.id.mTvDate)
    void onDateClick() {
        showDialog(DATE_DIALOG_ID);
    }

    @Click(R.id.mEdtMessage)
    void onTextMessageClick(View v) {
    }

    @Click(R.id.mTvTime)
    void onTimeClick() {
        showDialog(TIME_DIALOG_ID);
    }

    @Click(R.id.mIvCamera)
    void onAvatarClick() {
        mChooseCameraDialog = ChooseCameraDialog_.builder().build();
        mChooseCameraDialog.show(getSupportFragmentManager(), "");
    }

//    @Click(R.id.mIvSendTicket)
//    void onSendTicket() {
//        if (mCbChoose.isChecked()) {
//            mPrgSendTicket.setVisibility(View.VISIBLE);
//            disableTouchOnScreen(this, true);
//            RetrofitUtils.buildApiInterface(ApiConnect.class).sendTicket(uId,
//                    String.valueOf(idFollow), Integer.parseInt(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmClubId()), mTvDate.getText().toString() + " " + mTvTime.getText().toString(),
//                    fileAvatar, mEdtMessage.getText().toString(), new Callback<Void>() {
//                        @Override
//                        public void success(Void aVoid, Response response) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    disableTouchOnScreen(SendTicketActivity.this, false);
//                                    mPrgSendTicket.setVisibility(View.GONE);
//                                    mDialog = DialogConfirmFragment_.builder().mContentDialog("Send ticket successfully").isSuccess(true).build();
//                                    mDialog.setOnDialogListener(SendTicketActivity.this);
//                                    mDialog.show(getSupportFragmentManager(), "");
//                                    Log.d("xxx", "success");
//                                }
//                            });
//                        }
//
//                        @Override
//                        public void failure(final RetrofitError error) {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    disableTouchOnScreen(SendTicketActivity.this, false);
//                                    mPrgSendTicket.setVisibility(View.GONE);
//                                    mDialog = DialogConfirmFragment_.builder().mContentDialog("Send ticket fail: " + error.toString()).isSuccess(false).build();
//                                    mDialog.setOnDialogListener(SendTicketActivity.this);
//                                    mDialog.show(getSupportFragmentManager(), "");
//                                    Log.d("xxx+", "failure: ");
//                                }
//                            });
//                        }
//                    });
//        } else {
//            mDialog = DialogConfirmFragment_.builder().mContentDialog("Must choose follower").isSuccess(false).build();
//            mDialog.setOnDialogListener(SendTicketActivity.this);
//            mDialog.show(getSupportFragmentManager(), "");
//        }
//    }

    @OnActivityResult(Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
    void onResult(int resultCode, Intent data) {
        Log.d("xxx", "da ve");
        if (resultCode == RESULT_OK) {
            previewCapturedImage();

        }
    }

    @OnActivityResult(Constants.RESULT_LOAD_IMAGE)
    void onGalleryResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
//            fileAvatar = new File(selectedImage.getPath());
            Log.d("xxx", "onGalleryResult: " + fileAvatar.toString());
            mIvAvatarTicket.setVisibility(View.VISIBLE);
            mIvAvatarTicket.setImageURI(selectedImage);
        }
    }

    // how to rotaition thumbnail on picasso
    private void previewCapturedImage() {
//        fileAvatar = new File(mImageUri.getPath());
        Log.d("xxx", "previewCapturedImage: " + fileAvatar.toString());

//        // bimatp factory
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeFile(mImageUri.getPath(), options);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        mIvAvatar.setImageBitmap(bitmap);
        Matrix matrix = new Matrix();
        matrix.postRotate(getImageOrientation(mImageUri.getPath()));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);
        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        mBase64Avatar = Base64.encodeToString(byteArray, Base64.DEFAULT);
        mIvAvatarTicket.setVisibility(View.VISIBLE);
        mIvAvatarTicket.setImageBitmap(rotatedBitmap);
    }

    /**
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImageUri = getOutputMediaFileUri(Constants.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        // start the image capture Intent
        Log.d("xxx", "captureImage: ");
        startActivityForResult(intent, Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void galleryCaptureImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        try {

            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onDismissCameraListener() {
        mChooseCameraDialog.dismiss();
        captureImage();
    }

    @Override
    public void onDismissGalleryListener() {
        mChooseCameraDialog.dismiss();
        galleryCaptureImage();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        yearNow, month, day);
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, timePickListener, hour, min, true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            yearAge = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE");
            Date date = new Date(selectedYear, selectedMonth, selectedDay - 1);
            String dayOfWeek = simpledateformat.format(date);
            mTvDate.setText(dayOfWeek + " " + (month + 1) + "/" + day + "," + yearAge);
            // set selected date into datepicker also
            view.init(yearNow, month, day, null);
        }
    };

    private TimePickerDialog.OnTimeSetListener timePickListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String ampm = "";
            if (hourOfDay > 12) {
                ampm = "PM";
            } else {
                ampm = "AM";
            }
            mTvTime.setText(getTime(hourOfDay) + ":" + getTime(minute) + " " + ampm);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mImageUri != null) {
            outState.putString("cameraImageUri", mImageUri.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            mImageUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }

    @Override
    public void onSendClick() {
        mDialog.dismiss();
        onBackPressed();
    }

    @Override
    public void onCancel() {
        mDialog.dismiss();
    }
}
