package com.example.vanvy.myapplication.widget.more.setting.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vanvy on 10/16/2016.
 */
@EFragment(R.layout.fragment_dialog_cofirm_performing)
public class DialogConfirmPerformFragment extends DialogFragment implements Constants, View.OnClickListener {
    @ViewById()
    TextView mTvDetail;
    @ViewById
    TextView mTvOk;
    @ViewById
    TextView mTvNo;
    @FragmentArg
    public String mContentDialog;
    @FragmentArg
    public int active;
    private OnConfirmPerFormListener mListener;

    @AfterViews
    void afterViews() {
        mTvDetail.setText(mContentDialog);
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        mTvOk.setOnClickListener(this);
        mTvNo.setOnClickListener(this);
    }

    public void setOnDialogListener(OnConfirmPerFormListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTvOk:
                mListener.onOkClick(active);
                break;
            case R.id.mTvNo:
                mListener.onNoClick(active);
                break;
        }
    }

    public interface OnConfirmPerFormListener {
        void onOkClick(int active);
        void onNoClick(int active);
    }
}
