package com.example.vanvy.myapplication.customView;

/**
 * Created by asiantech on 9/29/16.
 */
public interface OnItemGalleryListener {
	void onItemClick(int position);
}
