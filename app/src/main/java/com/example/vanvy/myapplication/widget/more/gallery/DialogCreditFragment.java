package com.example.vanvy.myapplication.widget.more.gallery;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.common.Constants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vanvy on 4/20/2016.
 */
@EFragment(R.layout.fragment_dialog_credit)
public class DialogCreditFragment extends DialogFragment implements Constants {

    @ViewById()
    TextView mTvTitle;
    @FragmentArg
    String mCredit;
    @FragmentArg
    int mPosition;
    @AfterViews
    void afterViews(){
        mTvTitle.setText("Are you buying this photo for " + mCredit + " credit ?");
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }
    @Click(R.id.mTvOk)
    void onOkClick(){
        dismiss();
        getParentFragment().onActivityResult(REQUEST_CODE_CREDIT_PHOTO, RESULT_CODE_CREDIT, getActivity().getIntent());
    }
    @Click(R.id.mTvNo)
    void onNoClick(){
        dismiss();
    }
}

