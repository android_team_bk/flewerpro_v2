package com.example.vanvy.myapplication.widget.more;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.example.vanvy.myapplication.LoginActivity_;
import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.HeaderSetting;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.MenuItem;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.follower.FollowerFragment_;
import com.example.vanvy.myapplication.widget.more.adapter.Adapter;
import com.example.vanvy.myapplication.widget.more.adapter.Adapter.OnItemSettingClickListener;
import com.example.vanvy.myapplication.widget.more.block_list.BlockFragment_;
import com.example.vanvy.myapplication.widget.more.credit.CreditFragment_;
import com.example.vanvy.myapplication.widget.more.gallery.GalleryActivity_;
import com.example.vanvy.myapplication.widget.more.invitation.InvitationFragment_;
import com.example.vanvy.myapplication.widget.more.new_request.NewRequestFragment_;
import com.example.vanvy.myapplication.widget.more.profile.ProfileFragment_;
import com.example.vanvy.myapplication.widget.more.report.ReportFragment_;
import com.example.vanvy.myapplication.widget.more.setting.SettingFragment;
import com.example.vanvy.myapplication.widget.more.setting.SettingFragment_;
import com.example.vanvy.myapplication.widget.more.staff.StaffFragment_;
import com.example.vanvy.myapplication.widget.more.staff_lookup.StaffLookupFragment_;
import com.example.vanvy.myapplication.widget.qr.ScanQrCodeActivity_;
import com.example.vanvy.myapplication.widget.ticket.TicketFragment_;
import com.facebook.login.LoginManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_more)
public class MoreFragment extends BaseFragment implements Constants, SettingFragment.ClubListener, OnItemSettingClickListener {

    @ViewById()
    RecyclerView mRcvMore;

    @FragmentArg
    ArrayList<Gift> mGifts1;

    HeaderSetting headerSetting;
    Adapter mAdapter;

    private LogoutListener mListener;
    private Activity mContext;
    private int typeLogin;
    private List<MenuItem> menuItems = new ArrayList<>();
    private ItemMoreListener moreListener;
    private ClubSettingListener mClubListener;
    private SettingFragment mSettingFragment;

    @AfterViews
    void afterViews() {
        mContext = getActivity();
        typeLogin = SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN);
        initMenuItems();
    }

    public void initMenuItems() {
        menuItems.add(new MenuItem(getResources().getString(R.string.credit), null, getResources().getDrawable(R.mipmap.ic_more_credit)));
        if (typeLogin == 1) {
            menuItems.add(new MenuItem(getResources().getString(R.string.staff), null, getResources().getDrawable(R.mipmap.ic_more_request)));
            menuItems.add(new MenuItem(getResources().getString(R.string.staff_lookup), null, getResources().getDrawable(R.mipmap.ic_more_request)));
            menuItems.add(new MenuItem(getResources().getString(R.string.follower), null, getResources().getDrawable(R.mipmap.ic_more_request)));
        }
        menuItems.add(new MenuItem(getResources().getString(R.string.request), null, getResources().getDrawable(R.mipmap.ic_more_request)));
        if (typeLogin == 1) {
            menuItems.add(new MenuItem(getResources().getString(R.string.invitation), null, getResources().getDrawable(R.mipmap.ic_more_request)));
            menuItems.add(new MenuItem(getResources().getString(R.string.scan), null, getResources().getDrawable(R.mipmap.ic_more_request)));
        }
        if (typeLogin == 0) {
            menuItems.add(new MenuItem(getResources().getString(R.string.ticket), null, getResources().getDrawable(R.mipmap.ic_more_ticket)));
            menuItems.add(new MenuItem(getResources().getString(R.string.gallery), null, getResources().getDrawable(R.mipmap.ic_more_gallery)));
            menuItems.add(new MenuItem(getResources().getString(R.string.report), null, getResources().getDrawable(R.mipmap.ic_more_gallery)));
            menuItems.add(new MenuItem(getResources().getString(R.string.setting), null, getResources().getDrawable(R.mipmap.ic_more_setting)));
        }
        menuItems.add(new MenuItem("", null, null));
        menuItems.add(new MenuItem(getResources().getString(R.string.block), null, getResources().getDrawable(R.mipmap.ic_more_block)));
        menuItems.add(new MenuItem(getResources().getString(R.string.about), null, getResources().getDrawable(R.mipmap.ic_more_about_1)));
        menuItems.add(new MenuItem(getResources().getString(R.string.help), null, getResources().getDrawable(R.mipmap.ic_more_help)));
        menuItems.add(new MenuItem("", null, null));
        menuItems.add(new MenuItem(getResources().getString(R.string.text_logout), null, getResources().getDrawable(R.mipmap.ic_more_logout)));
        headerSetting = new HeaderSetting(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmName(), SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmAvatar());
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = li.inflate(R.layout.header_setting, null);
        header.setClickable(true);
        mAdapter = new Adapter(getActivity(), menuItems, headerSetting, this);
        mRcvMore.setAdapter(mAdapter);
        mRcvMore.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter.notifyDataSetChanged();
    }

    public void setupListener(ItemMoreListener listener) {
        moreListener = listener;
    }

    private void logout() {
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect mApi = restAdapter.create(ApiConnect.class);
        mApi.getLogout(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE).getmToken(), new Callback<Logout>() {
            @Override
            public void success(final Logout logout, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        SharedPreferences.clear(mContext);
        LoginManager.getInstance().logOut();
        LoginActivity_.intent(mContext).start();
        mContext.finish();
    }

    @Override
    public void clubClick() {
        mClubListener.clubClick();
    }

    @Override
    public void onItemSettingClick(int position) {
        if (position >= 0) {
            if (position == 8 && SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0
                    || (position == 10 && SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 1)) {
                moreListener.itemClickListener();
            } else {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                fm.popBackStack();
                BaseFragment f = null;
                mAdapter.notifyDataSetChanged();
                Log.d("xxx", "position: " + position);

                if (position < 0 || position >= mAdapter.getItemCount()) {
                    return;
                }
                try {
                    if (position == mAdapter.getItemCount() - 1) {
                        logout();
                        return;
                    }
                    if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
                        if (position == 0) {
                            f = ProfileFragment_.builder().build();
                        } else if (position == 1) {
                            f = CreditFragment_.builder().build();
                        } else if (position == 2) {
                            f = NewRequestFragment_.builder().gifts(mGifts1).build();
                        } else if (position == 3) {
                            f = TicketFragment_.builder().build();
                        } else if (position == 4) {
                            GalleryActivity_.intent(getActivity()).startForResult(REQUEST_CODE_MORE);
                        } else if (position == 5) {
                            f = ReportFragment_.builder().build();
                        } else if (position == 6) {
                            mSettingFragment = SettingFragment_.builder().build();
                            mSettingFragment.setUpListener(this);
                            f = mSettingFragment;
                        } else if (position == 8) {
                            f = BlockFragment_.builder().build();
                        } else if (position == 7) {
                            return;
                        } else if (position == 10) {
                            f = HelpSupportFragment_.builder().build();
                        }
                        //end staff
                    } else {
                        //club
                        if (position == 0 || position == 9 || position == 12) {
                            return;
                        } else if (position == 1) {
                            f = CreditFragment_.builder().build();
                        } else if (position == 2) {
                            f = StaffFragment_.builder().build();
                        } else if (position == 3) {
                            f = StaffLookupFragment_.builder().build();
                        } else if (position == 4) {
                            FollowerFragment_.builder().build();
                        } else if (position == 5) {
                            f = NewRequestFragment_.builder().build();
                        } else if (position == 6) {
                            f = InvitationFragment_.builder().build();
                        } else if (position == 7) {
                            ScanQrCodeActivity_.intent(getActivity()).start();
                        } else if (position == 9) {
                            f = BlockFragment_.builder().build();
                        } else if (position == 11) {
                            f = HelpSupportFragment_.builder().build();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (f != null) {
                    ft.replace(R.id.mFrMainContainer, f, "setting_content");
                    ft.addToBackStack("setting_fragment");
                    ft.commit();
                }
            }
        }
    }

    public interface ItemMoreListener {
        void itemClickListener();
    }

    public void setupClubListener(ClubSettingListener listener) {
        mClubListener = listener;
    }

    public interface ClubSettingListener {
        void clubClick();
    }
}
