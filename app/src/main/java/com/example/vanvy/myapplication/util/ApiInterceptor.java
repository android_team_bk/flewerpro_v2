package com.example.vanvy.myapplication.util;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ApiInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        Response.Builder newResponse = response.newBuilder();
        String data = response.body().string();
        try {
            JSONObject jsonObj = new JSONObject(data);
            int code = jsonObj.getInt("status");
            newResponse.code(code);
            newResponse.message(jsonObj.getString("message"));
            if (code == 200) {
                newResponse.body(ResponseBody.create(response.body().contentType(), jsonObj.get("data").toString()));
            } else {
                newResponse.body(ResponseBody.create(response.body().contentType(), jsonObj.get("data").toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newResponse.build();
    }
}
