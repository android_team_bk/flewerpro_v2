package com.example.vanvy.myapplication.base;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by vynv on 4/7/16.
 */
public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private final Context mContext;

    protected BaseAdapter(Context context) {
        mContext = context;
    }



    protected Context getContext() {
        return mContext;
    }

    protected Resources getResources() {
        return mContext.getResources();
    }
}
