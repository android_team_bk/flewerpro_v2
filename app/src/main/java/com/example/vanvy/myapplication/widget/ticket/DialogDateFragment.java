package com.example.vanvy.myapplication.widget.ticket;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vanvy on 9/22/2016.
 */
public class DialogDateFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {
    private OnDateListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @SuppressLint("SimpleDateFormat")
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Log.w("DatePicker", "Date = " + year);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE");
        Date date = new Date(year, month, day - 1);
        String dayOfWeek = simpledateformat.format(date);
        String strDay = dayOfWeek + " " + (month + 1) + "/" + day + "," + year;
        mListener.onDateListener(strDay,day,month+1,year);
    }

    public void setUpDateListener(OnDateListener listener) {
        mListener = listener;
    }

    public interface OnDateListener {
        void onDateListener(String date,int day,int month, int year);
    }
}
