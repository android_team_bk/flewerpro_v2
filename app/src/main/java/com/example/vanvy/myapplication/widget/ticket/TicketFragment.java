package com.example.vanvy.myapplication.widget.ticket;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.ClubPerforming;
import com.example.vanvy.myapplication.object.ETicket;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.register.ChooseCameraDialog;
import com.example.vanvy.myapplication.register.ChooseCameraDialog_;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.CameraUtil1;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.ImageUtil;
import com.example.vanvy.myapplication.util.ProgressedTypedFile;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment_;
import com.example.vanvy.myapplication.widget.more.setting.PerformingPlaceActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.vanvy.myapplication.util.Helpers.disableTouchOnScreen;

/**
 * Created by asiantech on 9/12/16.
 */
@EFragment(R.layout.fragment_ticket)
public class TicketFragment extends BaseFragment implements Constants, ETicketAdapter.ETicketListener,
        DialogConfirmFragment.OnConfirmSendListener, ChooseCameraDialog.onDialogDismiss,
        DialogTimeFragment.OnTimeListener, DialogDateFragment.OnDateListener, ChooseClubDialogFragment.ItemClubChoose {

    @ViewById
    RecyclerView mRcvFollower;
    @ViewById
    Button mDeselectAll;
    @ViewById
    Button mSelectAll;
    @ViewById
    TextView mTvDate;
    @ViewById
    TextView mTvTime;
    @ViewById
    TextView mNameClub;
    @ViewById
    ImageView mIvCamera;
    @ViewById
    ImageView mIvAvatarTicket;
    @ViewById
    EditText mEdtMessage;
    @ViewById
    ProgressBar mPrgSendTicket;
    private String TAG = getClass().getName();
    private Activity mActivity;
    private List<ETicket> mETickets = new ArrayList<>();
    private ETicketAdapter mAdapter;
    private ProgressedTypedFile fileAvatar;
    private DialogConfirmFragment mDialog;
    private ChooseCameraDialog mChooseCameraDialog;
    private Uri mImageUri;
    private DialogDateFragment mDialogDate;
    private DialogTimeFragment mDialogTime;
    private String mIdFollow = "";
    private String mSelectedImagePath = "";
    String uId;
    String clubId;
    private int mDay;
    private int mMonth;
    private int mYear;
    private int mHour;
    private int mMinute;
    private Date mDate;
    private String mDateTime;
    private ArrayList<ClubPerforming> mClubPerformings = new ArrayList<>();
    private ChooseClubDialogFragment mDialogChooseClub;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        uId = String.valueOf(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmId());
        clubId = String.valueOf(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmClubId());
        mAdapter = new ETicketAdapter(mActivity, mETickets, this);
        mRcvFollower.setAdapter(mAdapter);
        mRcvFollower.setLayoutManager(new LinearLayoutManager(mActivity));
        getData();
        getDataClub();
    }

    @Click(R.id.mDeselectAll)
    void onDeselectAllClick() {
        showHideCheckbox(false);
    }

    @Click(R.id.mSelectAll)
    void onSelectAllClick() {
        showHideCheckbox(true);
    }

    @Click(R.id.mTvDate)
    void onDateClick() {
        mDialogDate = new DialogDateFragment();
        mDialogDate.setUpDateListener(this);
        mDialogDate.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mTvTime)
    void onTimeClick() {
        mDialogTime = new DialogTimeFragment();
        mDialogTime.setUpTimeListener(this);
        mDialogTime.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mIvCamera)
    void onCameraClick() {
        mChooseCameraDialog = ChooseCameraDialog_.builder().build();
        mChooseCameraDialog.setUpCameraListener(this);
        mChooseCameraDialog.show(getChildFragmentManager(), "");
    }

    @Click(R.id.mIvSendTicketFragment)
    void onSendTicketClick() {
        mIdFollow = "";
        new SendFileTask().start(new SendFileTask.WorkerListener() {
            @Override
            public void onPreExecute() {
            }

            @Override
            public String getTask() {
                return mSelectedImagePath.isEmpty() ? "" : mSelectedImagePath;
            }

            @Override
            public void onPostExecute(ProgressedTypedFile result) {
                fileAvatar = result;
                for (int i = 0; i < mETickets.size(); i++) {
                    if (mETickets.get(i).getIsChecked() == 1) {
                        mIdFollow = mIdFollow + "," + mETickets.get(i).getId();
                    }
                }
                mIdFollow = mIdFollow.substring(1, mIdFollow.length());
                try {
                    DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy hh:mm");
                    mDate = formatter.parse(mMonth + "-" + mDay + "-" + mYear + " " + mHour + ":" + mMinute);
                    mDateTime = String.valueOf(mDate.getTime() / 1000L);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sendTicket();
            }
        });

    }

    @Click(R.id.mNameClub)
    void onClubClick() {
        Log.d(TAG, "onClubClick: ");
        mDialogChooseClub = ChooseClubDialogFragment_.builder().performings(mClubPerformings).build();
        mDialogChooseClub.setUpListener(this);
        mDialogChooseClub.show(getChildFragmentManager(), "");
    }

    private void sendTicket() {
        Log.d(TAG, "onSendTicketClick: " + uId + "-" + clubId + "-" + mIdFollow);
        disableTouchOnScreen(mActivity, true);
        mPrgSendTicket.setVisibility(View.VISIBLE);
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect mApi = restAdapter.create(ApiConnect.class);
        mApi.sendTicket(uId, mIdFollow, clubId, mDateTime,
                fileAvatar, mEdtMessage.getText().toString(), new Callback<Logout>() {
                    @Override
                    public void success(final Logout logout, Response response) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                disableTouchOnScreen(mActivity, false);
                                mPrgSendTicket.setVisibility(View.GONE);
                                if (logout.getmMessage().equals("Success")) {
                                    mDialog = DialogConfirmFragment_.builder().mContentDialog("Send ticket successfully.").isSuccess(true).build();
                                    mDialog.setOnDialogListener(TicketFragment.this);
                                    mDialog.show(getChildFragmentManager(), "");
                                } else {
                                    mDialog = DialogConfirmFragment_.builder().mContentDialog(logout.getData()).isSuccess(false).build();
                                    mDialog.setOnDialogListener(TicketFragment.this);
                                    mDialog.show(getChildFragmentManager(), "");
                                }
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "error: " + error.toString());
                                disableTouchOnScreen(mActivity, false);
                                mPrgSendTicket.setVisibility(View.GONE);
                                mDialog = DialogConfirmFragment_.builder().mContentDialog("Send ticket fail.").isSuccess(false).build();
                                mDialog.setOnDialogListener(TicketFragment.this);
                                mDialog.show(getChildFragmentManager(), "");
                            }
                        });
                    }
                });
    }

    private void showHideCheckbox(boolean isShow) {
        for (ETicket eTicket : mETickets) {
            if (isShow) {
                eTicket.setIsChecked(1);
            } else {
                eTicket.setIsChecked(0);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void getData() {
        disableTouchOnScreen(mActivity, true);
        mPrgSendTicket.setVisibility(View.VISIBLE);
        if (mETickets != null) {
            mETickets.clear();
        }
        RetrofitUtils.buildApiInterface(ApiConnect.class).getETicket(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmId(), new Callback<List<ETicket>>() {
            @Override
            public void success(final List<ETicket> eTickets, Response response) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mETickets.addAll(eTickets);
                        for (ETicket eTicket : mETickets) {
                            eTicket.setIsChecked(0);
                        }
                        mAdapter.notifyDataSetChanged();
                        mPrgSendTicket.setVisibility(View.GONE);
                        Helpers.disableTouchOnScreen(mActivity, false);
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        disableTouchOnScreen(mActivity, false);
                        mPrgSendTicket.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    private void getDataClub() {
        if (mClubPerformings.size() > 0) {
            mClubPerformings.clear();
        }
        RetrofitUtils.buildApiInterface(ApiConnect.class).getListClubPerforming(SharedPreferences.getUserProfile(mActivity, KEY_USER_PROFILE).getmId(),
                new Callback<List<ClubPerforming>>() {
                    @Override
                    public void success(final List<ClubPerforming> clubPerformings, Response response) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (ClubPerforming performing : clubPerformings) {
                                    if (performing.getIsActive() == 1 || performing.getApprove() == 3) {
                                        mClubPerformings.add(performing);
                                        if (performing.getIsActive() == 1) {
                                            mNameClub.setText(performing.getClub().getmName());
                                        }
                                    }
                                }
                                mClubPerformings.addAll(clubPerformings);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(mActivity, "error: " + error.toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
    }

    @Override
    public void itemTicketCheck(int position, int isCheck) {
        mETickets.get(position).setIsChecked(isCheck);
    }

    @Override
    public void onSendClick() {
        mDialog.dismiss();
    }

    @Override
    public void onCancel() {
        mDialog.dismiss();
    }

    @OnActivityResult(Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
    void onResult(int resultCode, Intent data) {
        Log.d("xxx", "da ve");
        if (resultCode == getActivity().RESULT_OK) {
            try {
                mSelectedImagePath = getPath(mImageUri);
                int rotateImage = getRotationImage(mImageUri);
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), mImageUri);
                bitmap = Helpers.rotateBitmap(bitmap, rotateImage);
                mIvAvatarTicket.setVisibility(View.VISIBLE);
                mIvAvatarTicket.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.d("xxx", e.getMessage());
            }
        }
    }

    @OnActivityResult(Constants.RESULT_LOAD_IMAGE)
    void onGalleryResult(int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            Uri selectedImage = data.getData();
            mSelectedImagePath = getPath(selectedImage);
            mIvAvatarTicket.setVisibility(View.VISIBLE);
            mIvAvatarTicket.setImageURI(selectedImage);
        }
    }

    /**
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        mImageUri = ImageUtil.getPhotoUri(this.getContext());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void galleryCaptureImage() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        try {
            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onDismissCameraListener() {
        Log.d("xxx", "onDismissCamera");
        mChooseCameraDialog.dismiss();
        captureImage();
    }

    @Override
    public void onDismissGalleryListener() {
        Log.d("xxx", "onDismissGallery");
        mChooseCameraDialog.dismiss();
        galleryCaptureImage();
    }

    @Override
    public void onTimeListener(String time, int hour, int min) {
        mHour = hour;
        mMinute = min;
        mTvTime.setText(time);
    }

    @Override
    public void onDateListener(String date, int day, int month, int year) {
        mDay = day;
        mMonth = month;
        mYear = year;
        mTvDate.setText(date);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private int getRotationImage(Uri uri) {
        int photoRotate = CameraUtil1.getInstance().getOrientationFromMediaStore(getContext(), uri);
        Log.d("xxxx", "resultTakePhoto: " + photoRotate);
        if (photoRotate == 0) {
            String path = CameraUtil1.getInstance().getPath(getContext(), uri);
            photoRotate = CameraUtil1.getInstance().getOrientationFromExif(path);
        }
        return photoRotate;
    }

    @Override
    public void itemClubChoose(String name) {
        mDialogChooseClub.dismiss();
        mNameClub.setText(name);
    }


    public static class SendFileTask extends AsyncTask<String, Integer, ProgressedTypedFile> {

        public interface WorkerListener {
            void onPreExecute();

            String getTask();

            void onPostExecute(ProgressedTypedFile result);
        }

        private WorkerListener mListener;

        public void start(WorkerListener callBack) {
            mListener = callBack;
            execute();
        }

        @Override
        protected ProgressedTypedFile doInBackground(final String... params) {
            ProgressedTypedFile.Listener listener = new ProgressedTypedFile.Listener() {
                @Override
                public void onUpdateProgress(int percentage) {
                    Log.d("TicketFragment", "" + percentage);
                }
            };
            return mListener.getTask().isEmpty() ? null : new ProgressedTypedFile("multipart/form-data", new File(mListener.getTask()), listener);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Log.d("TicketFragment", String.format("progress[%d]", values[0]) + "");
        }

        @Override
        protected void onPostExecute(ProgressedTypedFile imageObservable) {
            super.onPostExecute(imageObservable);
            mListener.onPostExecute(imageObservable);
        }
    }
}

