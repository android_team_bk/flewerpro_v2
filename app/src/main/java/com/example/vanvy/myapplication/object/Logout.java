package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vanvy on 4/3/2016.
 */
public class Logout implements Parcelable{
   @SerializedName("status")
    private int mStatus;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("data")
    private String data;


    public Logout(Parcel in) {
        mStatus = in.readInt();
        mMessage = in.readString();
        data = in.readString();
    }

    public static final Creator<Logout> CREATOR = new Creator<Logout>() {
        @Override
        public Logout createFromParcel(Parcel in) {
            return new Logout(in);
        }

        @Override
        public Logout[] newArray(int size) {
            return new Logout[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mStatus);
        dest.writeString(mMessage);
        dest.writeString(data);
    }

    public int getmStatus() {
        return mStatus;
    }

    public String getmMessage() {
        return mMessage;
    }

    public String getData() {
        return data;
    }
}
