package com.example.vanvy.myapplication.widget.follower;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by asiantech on 9/21/16.
 */
@EFragment(R.layout.fragment_dialog_report_confirm)
public class DialogReportConfirmFragment extends DialogFragment {

	@ViewById
	TextView mTvSuccess;

	@FragmentArg
	String stringNoteConfirm;

	@AfterViews
	void afterViews() {
		mTvSuccess.setText(stringNoteConfirm);
		Dialog dialog = getDialog();
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
	}

	@Click(R.id.mBtnOk)
	void onConfirmClick() {
		dismiss();
	}
}
