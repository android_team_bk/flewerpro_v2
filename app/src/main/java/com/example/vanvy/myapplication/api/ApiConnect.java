package com.example.vanvy.myapplication.api;

import com.example.vanvy.myapplication.object.Block;
import com.example.vanvy.myapplication.object.Checkout;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.object.ClubPerforming;
import com.example.vanvy.myapplication.object.ComingEvent;
import com.example.vanvy.myapplication.object.ETicket;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.Message;
import com.example.vanvy.myapplication.object.NewRequest;
import com.example.vanvy.myapplication.object.ProfileDancer;
import com.example.vanvy.myapplication.object.UserClub;
import com.example.vanvy.myapplication.object.UserInfo;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.searchLocation.location.ResultLocation;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.object.FollowPerformers;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.NotificationObject;
import com.squareup.okhttp.Call;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;

/**
 * Created by vanvy on 4/3/2016.
 */
public interface ApiConnect {
    @FormUrlEncoded
    @POST(ApiUtils.REGISTER)
    void getUseProfile(@Field("device_type") String device_type,
                       @Field("device_token") String device_token,
                       @Field("device_id") String device_id,
                       @Field("name") String name, @Field("name") String realname, @Field("realname") String surname,
                       @Field("email") String email,
                       @Field("password") String password,
                       @Field("month") int month,
                       @Field("day") int day,
                       @Field("year") int year,
                       @Field("city") String city,
                       @Field("state") String state,
                       @Field("country") String country,
                       @Field("lng") Double lng,
                       @Field("lat") Double lat,
                       @Field("gender") String gender,
                       @Field("telephone") String telephone,
                       @Field("avatar") String avatar,
                       @Field("rule") int rule,
                       Callback<UserProfile> cb);

    @FormUrlEncoded
    @POST(ApiUtils.LOGOUT)
    void getLogout(@Field("token") String token, Callback<Logout> cb);

    @FormUrlEncoded
    @POST(ApiUtils.LOGIN)
    void getLogin(@Field("email") String email,
                  @Field("password") String password,
                  @Field("device_type") String device_type,
                  @Field("device_token") String device_token,
                  @Field("device_id") String device_id,
                  Callback<UserProfile> cb);

    //    @GET(ApiUtils.NEARBY + "/{uid}/{page}/{range}")
//    void getNearby(@Path("uid") int id, @Path("page") int page, @Path("range") int range, Callback<List<Nearby>> cb);
//
//    @GET(ApiUtils.CLUB + "/{uid}/nearest")
//    void getClub(@Path("uid") int id, Callback<List<UserClub>> cb);
//
    @GET(ApiUtils.FOLLOW + "/{uid}")
    void getFollow(@Path("uid") int id, Callback<List<FollowPerformers>> cb);

    //
//    @GET(ApiUtils.INVITE + "/{uid}")
//    void getInvite(@Path("uid") int id, Callback<List<Invitation>> cb);
//
    @GET(ApiUtils.PROFILE_DANCER + "/{did}/{uid}")
    void getDancerProfile(@Path("did") int did, @Path("uid") int id, Callback<ProfileDancer> cb);

    //
//    @FormUrlEncoded
//    @POST(ApiUtils.PURCHASE)
//    void getPurchase(@Field("user_id") int user_id, @Field("item_id") int item_id, Callback<Logout> cb);
//
//    @FormUrlEncoded
//    @POST(ApiUtils.FOLLOW_CLUB)
//    void getFollowClub(@Field("user_id") int user_id, @Field("club_id") int club_id, Callback<Logout> cb);
//
//    @GET(ApiUtils.STAFF + "/staff/{club_id}/{type}")
//    void getStaffClub(@Path("club_id") int club_id, @Path("type") int type, Callback<StaffClub> cb);
//
//    @GET("/club-feed/get-post/{club_id}/{user_id}/{page}")
//    void getFeedClub(@Path("club_id") int club_id, @Path("user_id") int user_id, @Path("page") int page, Callback<List<FeedClub>> cb);
//
//    @GET("/club-feed/get-comments/{comment_id}/{userID}/{is_club}")
//    void getDataComment(@Path("comment_id") int comment_id, @Path("userID") int userID, @Path("is_club") int isClub, Callback<List<Comment>> cb);
//
//    @FormUrlEncoded
//    @POST("/club-feed/add-comment")
//    void sendComment(@Field("user_id") int user_id, @Field("is_club") int is_club, @Field("post_id") int post_id, @Field("comment") String comment, Callback<Comment> cb);
//
//    @FormUrlEncoded
//    @POST(ApiUtils.FAVORITE)
//    void sendLike(@Field("user_id") int user_id, @Field("post_id") int post_id, Callback<Logout> cb);
//
//    @FormUrlEncoded
//    @POST(ApiUtils.START_CHAT)
//    void getDataStartChat(@Field("from") int from, @Field("to") int to, @Field("gift") int gift, @Field("from_type") int from_type, @Field("to_type") int to_type
//            , Callback<StartChat> cb);
//
//    @FormUrlEncoded
//    @POST("/chat/synch")
//    void getDataMessage(@Field("user_id") int user_id, @Field("last_message_id") int last_message_id, @Field("is_club") int is_club
//            , Callback<List<Message>> cb);
//
    @GET("/gifts")
    void getDataGift(Callback<List<Gift>> cb);

    @FormUrlEncoded
    @POST("/credit/transfer")
    void sendCredit(@Field("sender_id") int user_id, @Field("receiver_id") int dancer_id, @Field("credit") String gifts, @Field("is_club") int isClub, Callback<Void> cb);

    //
    @FormUrlEncoded
    @POST("/chat/list")
    void getChatList(@Field("uid") int user_id, @Field("is_club") int is_club, Callback<List<Message>> cb);

    //
//    @FormUrlEncoded
//    @POST("/login-facebook")
//    void getChatList(@Field("facebookID") String facebookID, @Field("device_type") String deviceType, @Field("device_token") String deviceToken, @Field("device_id") String deviceId, Callback<List<ChatList>> cb);
//
    @GET("/notification/list/{uid}/{is_club}/{page}")
    void getNotification(@Path("uid") int user_id, @Path("is_club") int is_club, @Path("page") int page, Callback<List<NotificationObject>> cb);

    //
//    @GET("/invite/detail/{content_id}")
//    void getTicketDetail(@Path("content_id") int id, Callback<ETicket> cb);
//
//    @FormUrlEncoded
//    @POST("/user/report")
//    void sendReport(@Field("reporter_id") String reporter_id, @Field("is_club") int isClub, @Field("reason") String reason, @Field("type") int type);
//
    @FormUrlEncoded
    @POST("/user/updateLocation")
    void updateLocation(@Field("lat") double lat, @Field("lng") double lng, @Field("user_id") String user_id, Callback<Logout> cb);

    @GET("/maps/api/geocode/json")
    void getLocation(@Query("address") String address, Callback<ResultLocation> cb);

    @FormUrlEncoded
    @POST("/user/update")
    void updateProfile(@Field("id") int id, @Field("month") int month, @Field("day") int day, @Field("year") int year,
                       @Field("city") String city, @Field("country") String country, @Field("state") String state,
                       @Field("gender") int idGender, @Field("telephone") String telephone, @Field("surname") String surname,
                       @Field("realname") String realname, @Field("avatar") String avatar, Callback<UserProfile> cb);

    @Multipart
    @POST("/invite/send")
    void sendTicket(@Part("from_id") String fromId, @Part("to_id") String toId, @Part("club_id") String clubId, @Part("datetime") String dateTime,
                    @Part("picture") TypedFile picture,
                    @Part("message") String message, Callback<Logout> cb);

    @POST("/invite/send")
    void sendTicket1(@Body MultipartTypedOutput attachment, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/user/report")
    void sendReport(@Field("reporter_id") int reporterId, @Field("is_club") int isClub, @Field("reported_id") int reportedId,
                    @Field("reason") String reason, @Field("type") int type, Callback<String> cb);

    @FormUrlEncoded
    @POST("/user/block")
    void sendBock(@Field("from") int from, @Field("to") int to, @Field("from_type") int fromType,
                  @Field("to_type") int toType, Callback<Void> cb);

    @GET("/user/blockedList/{uid}/{is_club}")
    void getBlockList(@Path("uid") int userId, @Path("is_club") int isClub, Callback<List<Block>> cb);

    @FormUrlEncoded
    @POST("/user/unblock")
    void unlockUser(@Field("from") String from, @Field("to") String to, @Field("from_type") int fromType, @Field("to_type") int toType,
                    Callback<Void> cb);

    @GET(ApiUtils.FOLLOW + "/{uid}")
    void getETicket(@Path("uid") int id, Callback<List<ETicket>> cb);

    @GET("/user/info/{did}/{uid}")
    void getDashBoard(@Path("did") int did, @Path("uid") int id, Callback<UserInfo> cb);

    @GET("/user/info/{did}/{uid}")
    void getDashBoardClub(@Path("did") int did, @Path("uid") int id, Callback<Club> cb);

    @GET("/request/pending/{uid}")
    void getRequestPending(@Path("uid") int id, Callback<List<NewRequest>> cb);

    @GET("/user/commingSchedule/{uid}/{timestamp}")
    void getComingEvent(@Path("uid") int id, @Path("timestamp") String timeStamp, Callback<List<ComingEvent>> cb);

    @GET("/user/listSchedule/{uid}/{type}")
    void getComingAlarm(@Path("uid") int id, @Path("type") int type, Callback<List<ComingEvent>> cb);

    @GET("/clubs/{uid}/nearest")
    void getListClub(@Path("uid") int id, Callback<ArrayList<UserClub>> cb);

    @GET("/users-club/get/{uid}")
    void getListClubPerforming(@Path("uid") int id, Callback<List<ClubPerforming>> cb);

    @FormUrlEncoded
    @POST("/club-manage/login")
    void loginClub(@Field("club_name") String name, @Field("password") String password,
                   @Field("device_type") String deviceType,
                   @Field("device_token") String deviceToken, @Field("device_id") String deviceId,
                   Callback<List<Club>> cb);

    @FormUrlEncoded
    @POST("/users-club/updateNew")
    void updateNewPlace(@Field("user_id") int uid, @Field("club_id") int clubId, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/users-club/updateRemove")
    void updateRemove(@Field("id") int uid, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/users-club/setActive")
    void setActive(@Field("id") int uid, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/dancer/setting")
    void settingStaff(@Field("uid") int uid, @Field("profile") int profile, @Field("private") int mPrivate,
                      @Field("min_gift") String min_gift,
                      @Field("max_session") String max_session, @Field("club_id") int clubId, Callback<Void> cb);

    @Multipart
    @POST("/gallery/add")
    void getAddGallery(@Part("user_id") int uId, @Part("picture") TypedFile picture, @Part("type") int type,
                       @Part("price") int price, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/gallery/update")
    void getUpdateGallery(@Field("user_id") int uId, @Field("item_id") int itemId, @Field("type") int type, @Field("price") int price, Callback<String> cb);

    @FormUrlEncoded
    @POST("/user/change_password")
    void deleteGallery(@Field("user_id") int uId, @Field("item_id") int itemId, Callback<Logout> cb);

    @FormUrlEncoded
    @POST("/club-manage/staffForceCheckout")
    void checkout(@Field("uid") int uId, @Field("club_id") int clubId, @Field("lat") Double lat, @Field("lng") Double lng,
                  Callback<Checkout> cb);

    @GET("/club-manage/staffCheckOutList/{user_id}")
    void getCheckout(@Path("user_id") int id, Callback<List<Checkout>> cb);

    @FormUrlEncoded
    @POST("/user/change_password")
    void updatePassword(@Field("user_id") int uId, @Field("old_pass") String oldPass, @Field("new_pass") String newPass, Callback<Logout> cb);
}
