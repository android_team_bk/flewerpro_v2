package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asiantech on 10/6/16.
 */
public class NewRequest implements Parcelable {
	@SerializedName("id")
	private String id;
	@SerializedName("u1")
	private String u1;
	@SerializedName("u2")
	private String u2;
	@SerializedName("status")
	private String status;
	@SerializedName("gifts")
	private String gifts;
	@SerializedName("deleted_at")
	private String deleted_at;
	@SerializedName("created_at")
	private String createdAt;
	@SerializedName("updated_at")
	private String updatedAt;
	@SerializedName("seen")
	private String seen;
	@SerializedName("sender")
	private Sender senders;


	protected NewRequest(Parcel in) {
		id = in.readString();
		u1 = in.readString();
		u2 = in.readString();
		status = in.readString();
		gifts = in.readString();
		deleted_at = in.readString();
		createdAt = in.readString();
		updatedAt = in.readString();
		seen = in.readString();
		senders = in.readParcelable(Sender.class.getClassLoader());
	}

	public static final Creator<NewRequest> CREATOR = new Creator<NewRequest>() {
		@Override
		public NewRequest createFromParcel(Parcel in) {
			return new NewRequest(in);
		}

		@Override
		public NewRequest[] newArray(int size) {
			return new NewRequest[size];
		}
	};

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getU1() {
		return u1;
	}

	public void setU1(String u1) {
		this.u1 = u1;
	}

	public String getU2() {
		return u2;
	}

	public void setU2(String u2) {
		this.u2 = u2;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGifts() {
		return gifts;
	}

	public void setGifts(String gifts) {
		this.gifts = gifts;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getSeen() {
		return seen;
	}

	public void setSeen(String seen) {
		this.seen = seen;
	}

	public Sender getSenders() {
		return senders;
	}

	public void setSenders(Sender senders) {
		this.senders = senders;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(u1);
		dest.writeString(u2);
		dest.writeString(status);
		dest.writeString(gifts);
		dest.writeString(deleted_at);
		dest.writeString(createdAt);
		dest.writeString(updatedAt);
		dest.writeString(seen);
		dest.writeParcelable(senders, flags);
	}
}
