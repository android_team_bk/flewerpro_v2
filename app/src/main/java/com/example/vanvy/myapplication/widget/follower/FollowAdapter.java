package com.example.vanvy.myapplication.widget.follower;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.FollowPerformers;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 9/15/16.
 */
public class FollowAdapter extends BaseAdapter {
    private List<FollowPerformers> mFollowPerformerses;
    private Context mContext;
    OnFollowItemListener mListener;
    public FollowAdapter(Context context, List<FollowPerformers> followPerformerses, OnFollowItemListener listener) {
        super(context);
        mContext = context;
        mFollowPerformerses = followPerformerses;
        mListener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewFollowPerformerHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_follow, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindViewFollowHolder((ViewFollowPerformerHolder) holder, position);
    }

    private void onBindViewFollowHolder(ViewFollowPerformerHolder holder, int position) {
        holder.mTvAddressFollow.setText(mFollowPerformerses.get(position).getCity());
        holder.mTvName.setText(mFollowPerformerses.get(position).getName());
        holder.mTvAgeFollow.setText(mFollowPerformerses.get(position).getAge()+"");
        Picasso.with(mContext)
                .load(mFollowPerformerses.get(position).getAvatar())
                .into(holder.mIvAvatarFollow);
    }

    @Override
    public int getItemCount() {
        return mFollowPerformerses.size();
    }

    class ViewFollowPerformerHolder extends RecyclerView.ViewHolder {
        private ImageView mIvOption;
        private ImageView mIvAvatarFollow;
        private TextView mTvAddressFollow;
        private TextView mTvAgeFollow;
        private TextView mTvName;

        public ViewFollowPerformerHolder(View itemView) {
            super(itemView);
            mIvOption = (ImageView) itemView.findViewById(R.id.ic_mOption);
            mIvAvatarFollow = (ImageView) itemView.findViewById(R.id.mIvAvatarFollow);
            mTvAddressFollow = (TextView) itemView.findViewById(R.id.mTvAddressFollow);
            mTvAgeFollow = (TextView) itemView.findViewById(R.id.mTvAgeFollow);
            mTvName = (TextView) itemView.findViewById(R.id.mTvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFollowClick(getLayoutPosition());
                }
            });
        }
    }
    public interface OnFollowItemListener{
        void onFollowClick(int position);
    }
}
