package com.example.vanvy.myapplication.widget.more.report;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.object.ProfileDancer;
import com.example.vanvy.myapplication.widget.more.gallery.MediaFragment_;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanvy on 4/15/2016.
 */
public class PageReportAdapter extends FragmentPagerAdapter {
	private List<BaseFragment> mTabItems = new ArrayList<>();

	public PageReportAdapter(FragmentManager fragmentManager,
							 List<BaseFragment> fragments) {
		super(fragmentManager);
		mTabItems = fragments;
	}

	@Override
	public int getCount() {
		return mTabItems.size();
	}

	@Override
	public Fragment getItem(int position) {
		return ItemReportFragment_.builder().build();
	}

}
