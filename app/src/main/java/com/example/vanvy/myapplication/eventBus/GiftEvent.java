package com.example.vanvy.myapplication.eventBus;

import android.util.Log;

import com.example.vanvy.myapplication.object.Gift;

import java.util.List;

/**
 * Created by vanvy on 10/9/2016.
 */
public class GiftEvent {
    public List<Gift> gifts;

    public GiftEvent(List<Gift> gifts) {
        this.gifts = gifts;
        Log.d("xxxx", "GiftEvent: "+this.gifts);
    }

    public List<Gift> getGifts() {
        return gifts;
    }

    public void setGifts(List<Gift> gifts) {
        this.gifts = gifts;
    }
}
