package com.example.vanvy.myapplication.searchLocation.location;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asiantech on 9/6/16.
 */
public class ResultLocation implements Parcelable {
    @SerializedName("results")
    private List<Result> results = new ArrayList<>();
    @SerializedName("status")
    private String status;

    public ResultLocation(){}
    protected ResultLocation(Parcel in) {
        results = in.createTypedArrayList(Result.CREATOR);
        status = in.readString();
    }

    public static final Creator<ResultLocation> CREATOR = new Creator<ResultLocation>() {
        @Override
        public ResultLocation createFromParcel(Parcel in) {
            return new ResultLocation(in);
        }

        @Override
        public ResultLocation[] newArray(int size) {
            return new ResultLocation[size];
        }
    };

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(results);
        parcel.writeString(status);
    }
}
