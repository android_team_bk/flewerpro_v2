package com.example.vanvy.myapplication.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.vanvy.myapplication.R;

/**
 * Created by asiantech on 9/22/16.
 */
public class ProgressWheel extends View {

	//Sizes (with defaults)
	private int mLayoutHeight = 0;
	private int mLayoutWidth = 0;
	private int mBarLength = 100;
	private int mBarWidth = 20;
	private int mCircleBackGroundWidth = 40;

	//Colors (with defaults)
	private int mBarColorStandard = 0xAAFFCC00;
	private int mCircleBackGroundColor = 0xAADDDDDD;

	private int[] mBarColors = new int[]{mBarColorStandard};

	//Paints
	private Paint mBarPaint = new Paint();
	private Paint mCircleBackgroundPaint = new Paint();

	//Rectangles
	private RectF mCircleBounds = new RectF();

	//Animation
	private float progress = 0;

	/**
	 * The constructor for the ProgressWheel
	 *
	 * @param context context
	 * @param attrs   attrs
	 */
	public ProgressWheel(Context context, AttributeSet attrs) {
		super(context, attrs);
		parseAttributes(context.obtainStyledAttributes(attrs,
				R.styleable.ProgressWheel));
	}

	/**
	 * Use onSizeChanged instead of onAttachedToWindow to get the dimensions of the view,
	 * because this method is called after measuring the dimensions of MATCH_PARENT & WRAP_CONTENT.
	 * Use this dimensions to setup the bounds and paints.
	 */
	@Override
	protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
		super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
		mLayoutWidth = newWidth;
		mLayoutHeight = newHeight;
		setupBounds();
		setupPaints();
		invalidate();
	}

	/**
	 * Set the properties of the paints we're using to
	 * draw the progress wheel
	 */
	private void setupPaints() {
		mBarPaint.setAntiAlias(true);
		mBarPaint.setStyle(Style.STROKE);
		mBarPaint.setStrokeWidth(mBarWidth);
		if (mBarColors.length > 1) {
			mBarPaint.setShader(new SweepGradient(mCircleBounds.centerX(), mCircleBounds.centerY(), mBarColors, null));
			Matrix matrix = new Matrix();
			mBarPaint.getShader().getLocalMatrix(matrix);
			matrix.postTranslate(-mCircleBounds.centerX(), -mCircleBounds.centerY());
			matrix.postRotate(-90);
			matrix.postTranslate(mCircleBounds.centerX(), mCircleBounds.centerY());
			mBarPaint.getShader().setLocalMatrix(matrix);
			mBarPaint.setColor(mBarColorStandard);
		} else {
			mBarPaint.setColor(mBarColors[0]);
			mBarPaint.setShader(null);
		}

		mCircleBackgroundPaint.setColor(mCircleBackGroundColor);
		mCircleBackgroundPaint.setAntiAlias(true);
		mCircleBackgroundPaint.setStyle(Style.STROKE);
		mCircleBackgroundPaint.setStrokeWidth(mCircleBackGroundWidth);


	}

	/**
	 * Set the bounds of the component
	 */
	private void setupBounds() {
		// Width should equal to Height, find the min value to setup the circle
		int minValue = Math.min(mLayoutWidth, mLayoutHeight);

		// Calc the Offset if needed
		int xOffset = mLayoutWidth - minValue;
		int yOffset = mLayoutHeight - minValue;

		// Add the offset
		int paddingTop = this.getPaddingTop() + (yOffset / 2);
		int paddingBottom = this.getPaddingBottom() + (yOffset / 2);
		int paddingLeft = this.getPaddingLeft() + (xOffset / 2);
		int paddingRight = this.getPaddingRight() + (xOffset / 2);

		int width = getWidth();
		int height = getHeight();
		mCircleBounds = new RectF(
				paddingLeft + mBarWidth,
				paddingTop + mBarWidth,
				width - paddingRight - mBarWidth,
				height - paddingBottom - mBarWidth);

	}

	/**
	 * Parse the attributes passed to the view from the XML
	 *
	 * @param a the attributes to parse
	 */
	private void parseAttributes(TypedArray a) {
		mBarWidth = (int) a.getDimension(R.styleable.ProgressWheel_pwBarWidth, mBarWidth);
		mCircleBackGroundWidth = (int) a.getDimension(R.styleable.ProgressWheel_pwCircleBackGroundWidth, mCircleBackGroundWidth);
		mBarLength = (int) a.getDimension(R.styleable.ProgressWheel_pwBarLength, mBarLength);
		Log.d("xxxxA", "parseAttributes: "+ mBarLength);
		mBarColorStandard = a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard);
		mCircleBackGroundColor = a.getColor(R.styleable.ProgressWheel_pwCircleBackGroundColor, mCircleBackGroundColor);

		if (a.hasValue(R.styleable.ProgressWheel_pwBarColor)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor1)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor2)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor3)) {
			mBarColors = new int[]{a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor1, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor2, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor3, mBarColorStandard)};

		} else if (a.hasValue(R.styleable.ProgressWheel_pwBarColor)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor1)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor2)) {

			mBarColors = new int[]{a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor1, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor2, mBarColorStandard)};

		} else if (a.hasValue(R.styleable.ProgressWheel_pwBarColor)
				&& a.hasValue(R.styleable.ProgressWheel_pwBarColor1)) {

			mBarColors = new int[]{a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor1, mBarColorStandard)};

		} else {
			mBarColors = new int[]{a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard),
					a.getColor(R.styleable.ProgressWheel_pwBarColor, mBarColorStandard)};
		}

		a.recycle();
	}

	/**
	 * Draw view
	 *
	 * @param canvas canvas
	 */
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		//Draw the circleBackGround
		canvas.drawArc(mCircleBounds, 360, 360, false, mCircleBackgroundPaint);
		//Draw the bar
		canvas.drawArc(mCircleBounds, -90, progress, false, mBarPaint);
	}


	/**
	 * Set the progress to a specific value
	 */
	@SuppressWarnings("unused")
	public void setProgress(int i) {
		progress = i;
		postInvalidate();
	}
}

