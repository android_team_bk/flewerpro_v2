package com.example.vanvy.myapplication.widget.more.gallery;

import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.ProfileDancer;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 9/15/2016.
 */
@EActivity(R.layout.activity_gallery)
public class GalleryActivity extends BaseActivity implements Constants, View.OnClickListener, ViewPager.OnPageChangeListener {

	public static final int REQUEST_DELETE = 1231;

	@ViewById
	ImageView mIvLeft;
	@ViewById
	ImageView mIvRight;
	@ViewById
	TextView mTitleHeader;
	@ViewById
	TextView mTvRight;
	@ViewById
	View mTabStrip;
	@ViewById
	ViewPager mViewPage;
	@ViewById
	ProgressBar mPrgGallery;
	private LinearLayout mLlTab1;
	private LinearLayout mLlTab2;
	private LinearLayout mLlTab3;
	private LinearLayout mLlTab4;
	private PageAdapter mPageAdapter;
	private ProfileDancer mProfileDancer;
	private boolean isReload = false;
	private int mPositionSelectedPage;

	@AfterViews
	void afterViews() {
		mLlTab1 = (LinearLayout) mTabStrip.findViewById(R.id.tab1);
		mLlTab2 = (LinearLayout) mTabStrip.findViewById(R.id.tab2);
		mLlTab3 = (LinearLayout) mTabStrip.findViewById(R.id.tab3);
		mLlTab4 = (LinearLayout) mTabStrip.findViewById(R.id.tab4);
		initHeader();
		setDefaultViewBottom();
		setOnListener();
		onTabSelected(0);
		getProfileGallery();
	}

	@Click(R.id.mIvLeft)
	void onBackClick() {
		onBackPressed();
	}

	@Click(R.id.mIvRight)
	void onAddGallery() {
		AddGalleryActivity_.intent(this).startForResult(REQUEST_CODE_GALLERY);
	}

	@OnActivityResult(REQUEST_CODE_GALLERY)
	void onResultGallery() {
		isReload = true;
		getProfileGallery();

	}

	@OnActivityResult(REQUEST_DELETE)
	void onResultDelete() {
		isReload = true;
		getProfileGallery();
	}

	private void init() {
		ArrayList<BaseFragment> tabItems = new ArrayList<>();
		tabItems.add(new MediaFragment_());
		tabItems.add(new MediaFragment_());
		tabItems.add(new MediaFragment_());
		tabItems.add(new MediaFragment_());
		mPageAdapter = new PageAdapter(getSupportFragmentManager(), tabItems, mProfileDancer);
		mViewPage.setAdapter(mPageAdapter);
		mPageAdapter.notifyDataSetChanged();
		mViewPage.addOnPageChangeListener(this);
	}

	private void initHeader() {
		mTvRight.setVisibility(View.GONE);
		mIvRight.setVisibility(View.VISIBLE);
		mIvLeft.setVisibility(View.VISIBLE);
		mTitleHeader.setText("Gallery");
		mIvRight.setBackgroundResource(R.drawable.ic_add_club);
	}

	private void setOnListener() {
		mLlTab1.setOnClickListener(this);
		mLlTab2.setOnClickListener(this);
		mLlTab3.setOnClickListener(this);
		mLlTab4.setOnClickListener(this);
	}

	private void setDefaultViewBottom() {
		mLlTab1.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
		mLlTab2.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
		mLlTab3.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
		mLlTab4.setBackgroundColor(getResources().getColor(R.color.colorHeaderBar));
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tab1:
				onTabSelected(0);
				break;
			case R.id.tab2:
				onTabSelected(1);
				break;
			case R.id.tab3:
				onTabSelected(2);
				break;
			case R.id.tab4:
				onTabSelected(3);
				break;
		}
	}

	private void onTabSelected(int position) {
		setDefaultViewBottom();
		mViewPage.setCurrentItem(position);
		switch (position) {
			case 0:
				mLlTab1.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
				break;
			case 1:
				mLlTab2.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
				break;
			case 2:
				mLlTab3.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
				break;
			case 3:
				mLlTab4.setBackgroundColor(getResources().getColor(R.color.colorTabMain));
				break;
			default:
				break;
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		mPositionSelectedPage = position;
		onTabSelected(position);
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	private void getProfileGallery() {
		mPrgGallery.setVisibility(View.VISIBLE);
		Helpers.disableTouchOnScreen(this, true);
		RetrofitUtils.buildApiInterface(ApiConnect.class).getDancerProfile(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
				SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(), new Callback<ProfileDancer>() {
					@Override
					public void success(final ProfileDancer profileDancer, Response response) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mPrgGallery.setVisibility(View.GONE);
								Helpers.disableTouchOnScreen(GalleryActivity.this, false);
								mProfileDancer = new ProfileDancer();
								mProfileDancer = profileDancer;
								init();
								if (isReload) {
									MediaFragment fragment = (MediaFragment) mPageAdapter.instantiateItem(mViewPage, mViewPage.getCurrentItem());
									fragment.reloadData(profileDancer);
									onTabSelected(mPositionSelectedPage);
								}
							}
						});
					}

					@Override
					public void failure(final RetrofitError error) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mPrgGallery.setVisibility(View.GONE);
								Helpers.disableTouchOnScreen(GalleryActivity.this, false);
							}
						});
					}
				});
	}
}
