package com.example.vanvy.myapplication.widget.more.setting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.object.UserClub;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanvy on 10/14/2016.
 */
public class ItemClubAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<UserClub> mClubs;
    private ItemClubListener mListener;

    public ItemClubAdapter(Context context, ArrayList<UserClub> clubs, ItemClubListener listener) {
        super(context);
        mContext = context;
        mClubs = clubs;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewItemClubHolder(LayoutInflater.from(mContext).inflate(R.layout.item_club_setting, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindItemClub((ViewItemClubHolder) holder, position);
    }

    private void onBindItemClub(ViewItemClubHolder holder, int position) {
        holder.mClubName.setText(mClubs.get(position).getmName());
    }

    @Override
    public int getItemCount() {
        return mClubs.size();
    }

    class ViewItemClubHolder extends RecyclerView.ViewHolder {
        TextView mClubName;

        public ViewItemClubHolder(View itemView) {
            super(itemView);
            mClubName = (TextView) itemView.findViewById(R.id.mTvClubName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClubClick(getLayoutPosition());
                }
            });
        }
    }

    public interface ItemClubListener {
        void onItemClubClick(int position);
    }
}
