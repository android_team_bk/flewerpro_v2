package com.example.vanvy.myapplication.widget.dashboard;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.customView.ProgressWheel;
import com.example.vanvy.myapplication.object.Club;
import com.example.vanvy.myapplication.object.ComingEvent;
import com.example.vanvy.myapplication.object.UserInfo;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.more.setting.PerformingPlaceActivity_;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/12/16.
 */

@EFragment(R.layout.fragment_dashboard)
public class DashboardFragment extends BaseFragment implements Constants {

	@ViewById
	ProgressWheel prgDashboard;
	@ViewById
	TextView mTvClubName;
	@ViewById
	TextView mTvBalance;
	@ViewById
	ImageView mIvAvatarClub;
	@ViewById
	TextView mTvChangeWorkPlace;
	@ViewById
	RelativeLayout mRlVerifyInfo;
	@ViewById
	RelativeLayout mRlMore;
	@ViewById
	RecyclerView mRcvComingEvent;
	private Activity mContext;
	private DashboardAdapter mAdapter;
	private List<ComingEvent> mComingEvents = new ArrayList<>();
	private int percentBalance;
	private int uId;

	@AfterViews
	void afterViews() {
		setTitle("Dashboard", true, false, false);
		if (SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE) != null) {
			if (SharedPreferences.getTypeLogin(getActivity(), TYPE_LOGIN) == 0) {
				uId = SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId();
				getDataDashboardUser();
			} else {
				uId = SharedPreferences.getClubProfile(getActivity(), KEY_CLUB_PROFILE).getmId();
				getDataDashboardClub();
			}
		}
		mContext = getActivity();
		mAdapter = new DashboardAdapter(mContext, mComingEvents);
		mRcvComingEvent.setAdapter(mAdapter);
		mRcvComingEvent.setLayoutManager(new LinearLayoutManager(mContext));
		getEvent();
	}

	@Click(R.id.mTvChangeWorkPlace)
	void onChangeClub() {
		PerformingPlaceActivity_.intent(getActivity()).keyRequestCode(REQUEST_CODE_CHANGE_CLUB).startForResult(RESULT_CODE_SETTING);
	}

	private void getDataDashboardUser() {
		RetrofitUtils.buildApiInterface(ApiConnect.class).getDashBoard(uId, uId, new Callback<UserInfo>() {
			@Override
			public void success(final UserInfo userInfo, Response response) {
				mContext.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						percentBalance = (int) ((Float.valueOf(userInfo.getmCredits()) * 360) / 4000);
						prgDashboard.setProgress(percentBalance);
						mTvClubName.setText(userInfo.getmClub().getmName());
						mTvBalance.setText(userInfo.getmCredits());
						Picasso.with(mContext).load(userInfo.getmClub().getmAvatar()).into(mIvAvatarClub);
						if (Integer.parseInt(userInfo.getmVerifyDocument()) >= 0) {
							mRlVerifyInfo.setVisibility(View.GONE);
						} else {
							mRlVerifyInfo.setVisibility(View.VISIBLE);
						}
						if (Integer.parseInt(userInfo.getmGalleryCount()) >= 10) {
							mRlMore.setVisibility(View.GONE);
						} else {
							mRlMore.setVisibility(View.VISIBLE);
						}
					}
				});
			}

			@Override
			public void failure(RetrofitError error) {

			}
		});
	}

	private void getDataDashboardClub() {
		RetrofitUtils.buildApiInterface(ApiConnect.class).getDashBoardClub(uId, uId, new Callback<Club>() {
			@Override
			public void success(final Club club, Response response) {
				mContext.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						percentBalance = (int) ((Float.valueOf(club.getmCredits()) * 360) / 4000);
						prgDashboard.setProgress(percentBalance);
						mTvClubName.setText(club.getmName());
						mTvBalance.setText(club.getmCredits());
						Picasso.with(mContext).load(club.getmAvatar()).into(mIvAvatarClub);
//                        if (Integer.parseInt(club.getmVerifyDocument()) >= 0) {
//                            mRlVerifyInfo.setVisibility(View.GONE);
//                        } else {
//                            mRlVerifyInfo.setVisibility(View.VISIBLE);
//                        }
//                        if (Integer.parseInt(userInfo.getmGalleryCount()) >= 10) {
//                            mRlMore.setVisibility(View.GONE);
//                        } else {
//                            mRlMore.setVisibility(View.VISIBLE);
//                        }
					}
				});
			}

			@Override
			public void failure(RetrofitError error) {

			}
		});
	}

	private void getEvent() {
		Long tsLong = System.currentTimeMillis() / 1000;
		final String ts = tsLong.toString();
		Log.d("xxx12", "getEvent: " + ts);
		RetrofitUtils.buildApiInterface(ApiConnect.class).getComingEvent(SharedPreferences.getUserProfile(mContext, KEY_USER_PROFILE)
				.getmId(), ts, new Callback<List<ComingEvent>>() {
			@Override
			public void success(final List<ComingEvent> comingEvents, Response response) {
				mContext.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mComingEvents.size() > 0) {
							mComingEvents.clear();
						}
						Log.d("xxx1", "run: " + comingEvents.size());
						mComingEvents.addAll(comingEvents);
						mAdapter.notifyDataSetChanged();
					}
				});
			}

			@Override
			public void failure(RetrofitError error) {
				Log.d("xxxx", "getEvent: " + error.toString() + "-" + ts);
			}
		});
	}
}
