package com.example.vanvy.myapplication.widget.more.credit;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseFragment;

import org.androidannotations.annotations.EFragment;

/**
 * Created by asiantech on 9/20/16.
 */
@EFragment(R.layout.fragment_credit)
public class CreditFragment extends BaseFragment {
}
