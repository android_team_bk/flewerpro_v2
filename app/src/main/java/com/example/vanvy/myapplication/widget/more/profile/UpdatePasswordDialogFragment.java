package com.example.vanvy.myapplication.widget.more.profile;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by vanvy on 11/5/2016.
 */
@EFragment(R.layout.fragment_update_password)
public class UpdatePasswordDialogFragment extends DialogFragment implements Constants {

    @ViewById
    TextView mEdtCurrentPassword;
    @ViewById
    TextView mEdtNewPassword;
    @ViewById
    TextView mEdtConfirmNewPassword;

    private Activity mActivity;
    private ProgressBar mPrgUpdatePassword;

    @AfterViews
    void afterViews() {
        mActivity = getActivity();
        Dialog dialog = getDialog();
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    @Click(R.id.mTvOk)
    void onChangePassword() {
        if (mEdtNewPassword.getText().toString().equals(mEdtConfirmNewPassword.getText().toString())) {
            updatePassword();
        }
    }

    private void updatePassword() {
        Helpers.disableTouchOnScreen(getActivity(), true);
        mPrgUpdatePassword.setVisibility(View.VISIBLE);
        RestAdapter adapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect api = adapter.create(ApiConnect.class);
        api.updatePassword(SharedPreferences.getUserProfile(getActivity(), KEY_USER_PROFILE).getmId(),
                mEdtCurrentPassword.getText().toString(), mEdtNewPassword.getText().toString(), new Callback<Logout>() {
                    @Override
                    public void success(Logout logout, Response response) {
                        getDialog().dismiss();
                        Helpers.disableTouchOnScreen(getActivity(), false);
                        mPrgUpdatePassword.setVisibility(View.GONE);
                        Toast.makeText(mActivity, logout.getmMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        getDialog().dismiss();
                        Helpers.disableTouchOnScreen(getActivity(), false);
                        mPrgUpdatePassword.setVisibility(View.GONE);
                        Toast.makeText(mActivity, RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
