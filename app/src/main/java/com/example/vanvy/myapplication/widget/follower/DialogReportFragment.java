package com.example.vanvy.myapplication.widget.follower;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment;
import com.example.vanvy.myapplication.widget.follower.sendgift.DialogConfirmFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by asiantech on 9/21/16.
 */
@EFragment(R.layout.fragment_dialog_report)
public class DialogReportFragment extends DialogFragment implements Constants {
	private Activity mActivity;
	private DialogConfirmFragment mDialog;

	@ViewById
	TextView mTvReportMessage;
	@ViewById
	TextView mTvReportPhoto;
	@ViewById
	TextView mTvReportSpam;
	@ViewById
	TextView mTvReportOther;

	@FragmentArg
	int idFollow;
	private SendReportListener mListener;
	@AfterViews
	void afterViews() {
		Log.d("xxx", "afterViews: " + idFollow);
		Dialog dialog = getDialog();
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
		mActivity = getActivity();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar);
	}

	@Click(R.id.mTvReportMessage)
	void onReportMessage() {
		Log.d("xxx", "onReportMessage: ");
		mListener.sendReport(mTvReportMessage.getText().toString());
	}

	@Click(R.id.mTvReportPhoto)
	void onReportPhoto() {
		Log.d("xxx", "onReportpho: ");
		mListener.sendReport(mTvReportPhoto.getText().toString());
	}

	@Click(R.id.mTvReportSpam)
	void onReportSpam() {
		Log.d("xxx", "2: ");
		mListener.sendReport(mTvReportSpam.getText().toString());
	}
	@Click(R.id.mTvReportOther)
	void onReportOther(){
		mListener.sendReportOhter();
	}
	public void setUpSendReportListener(SendReportListener listener){
		mListener=listener;
	}
	public interface SendReportListener {
		void sendReport(String reason);
		void sendReportOhter();
	}
}
