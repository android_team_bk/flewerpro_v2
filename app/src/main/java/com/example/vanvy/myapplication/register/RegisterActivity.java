package com.example.vanvy.myapplication.register;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.LoginActivity_;
import com.example.vanvy.myapplication.MainActivity_;
import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.searchLocation.SearchLocationActivity_;
import com.example.vanvy.myapplication.util.Helpers;
import com.example.vanvy.myapplication.util.Keyboard;
import com.example.vanvy.myapplication.util.LocationHelper;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.google.android.gms.common.api.GoogleApiClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.vanvy.myapplication.util.Helpers.encodeToBase64;
import static com.example.vanvy.myapplication.util.Helpers.getImageOrientation;
import static com.example.vanvy.myapplication.util.Helpers.getOutputMediaFileUri;

/**
 * Created by asiantech on 9/5/16.
 */
@EActivity(R.layout.activity_register)
public class RegisterActivity extends BaseActivity implements onJobClickListener, ChooseCameraDialog.onDialogDismiss, onGenderListener, Constants {
	private int yearNow;
	private int yearAge;
	private int month;
	private int day;
	private String mCity = "";
	private String mState = "";
	private String mCountry = "";
	@ViewById()
	ImageView mIvAvatar;
	@ViewById()
	TextView mTvBirthDay;
	@ViewById()
	TextView mTvLocation;
	@ViewById()
	TextView mTvGender;
	@ViewById()
	EditText mEdtUserName;
	@ViewById()
	EditText mEdtRealName;
	@ViewById()
	EditText mEdtSurName;
	@ViewById()
	EditText mEdtEmail;
	@ViewById()
	EditText mEdtPassword;
	@ViewById()
	EditText mEdtConfirmPassword;
	@ViewById()
	EditText mEdtTelephone;
	@ViewById()
	TextView mTvJob;
	DialogFragmentJob mJobDialog;
	@ViewById()
	TextView mTvAccept;
	@ViewById()
	CheckBox mCbAccept;
	@ViewById()
	LinearLayout mLlRegister;
	private Uri mImageUri;
	private String mAvatar;
	private ChooseCameraDialog mChooseCameraDialog;
	private DialogFragmentGender mDialogGender;
	private int mIdJob;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;
	private LocationHelper locationHelper;
	private double latitude;
	private double longitude;

	@AfterViews
	void afterViews() {
		locationHelper = new LocationHelper(this);
		locationHelper.addRequestListener(new LocationHelper.OnLocationUpdate() {
			@Override
			public void onLocationUpdated(final Location location) {
				latitude = location.getLatitude();
				longitude = location.getLongitude();
			}
		});
		Log.d("xxx", "afterViews: " + latitude + "-" + longitude);
		setTitle("Register an Account", false, true, false);
		mTvAccept.setPaintFlags(mTvAccept.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		final Calendar c = Calendar.getInstance();
		yearNow = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		View v = findViewById(R.id.mHeaderBar);
		ImageView mIvLeft = (ImageView) v.findViewById(R.id.mIvLeft);
		mIvLeft.setImageResource(R.mipmap.ic_back);
		mIvLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity_.intent(RegisterActivity.this).start();
				finish();
			}
		});
	}

	@Click(R.id.mTvAccept)
	void onAcceptClick() {
		String url = "http://192.241.206.110/terms/Flewertermsandconditions.htm";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	@Click(R.id.mLlRegister)
	void onLayoutClick() {
		Keyboard.hideKeyboard(this, mLlRegister);
	}

	@Click(R.id.mTvJob)
	void onJobClick() {
		mJobDialog = DialogFragmentJob_.builder().build();
		mJobDialog.onJobDialogListener(this);
		mJobDialog.show(getFragmentManager(), "");
	}

	@Click(R.id.mTvBirthDay)
	void onBirthdayClick() {
		Keyboard.hideKeyboard(this, mTvBirthDay);
		showDialog(DATE_DIALOG_ID);
	}

	@Click(R.id.mTvGender)
	void onGenderClick() {
		mDialogGender = DialogFragmentGender_.builder().build();
		mDialogGender.onGenderListener(this);
		mDialogGender.show(getFragmentManager(), "");
	}

	@Click(R.id.mBtnRegister)
	void onRegisterClick() {
		if (!checkAllField().equals("")) {
			Toast.makeText(this, checkAllField(), Toast.LENGTH_SHORT).show();
		} else {
			onRegister();
		}
	}

	@Click(R.id.mTvLocation)
	void onSearchLocationClick() {
		Keyboard.hideKeyboard(this, mLlRegister);
		SearchLocationActivity_.intent(this).startForResult(SEARCH_LOCATION);
	}

	@Click(R.id.mIvAvatar)
	void onAvatarClick() {
		mChooseCameraDialog = ChooseCameraDialog_.builder().build();
		mChooseCameraDialog.setUpCameraListener(this);
		mChooseCameraDialog.show(getSupportFragmentManager(), "");
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
			case DATE_DIALOG_ID:
				// set date picker as current date
				return new DatePickerDialog(this, datePickerListener,
						yearNow - 18, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener
			= new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
		                      int selectedMonth, int selectedDay) {
			yearAge = selectedYear;
			month = selectedMonth;
			day = selectedDay;
			mTvBirthDay.setText(yearAge + "-" + (month + 1) + "-" + day);
			// set selected date into datepicker also
			view.init(yearNow, month, day, null);
		}
	};

	private String checkAllField() {
		String strFieldNull = "Please Enter the ";
		String strCheck = "";
		if (mEdtUserName.getText().toString().equals("")) {
			strCheck = "Username";
			return strFieldNull + strCheck;
		} else if (mEdtRealName.getText().toString().equals("")) {
			strCheck = "Realname";
			return strFieldNull + strCheck;
		} else if (mEdtSurName.getText().toString().equals("")) {
			strCheck = "Surname";
			return strFieldNull + strCheck;
		} else if (mEdtEmail.getText().toString().equals("")) {
			strCheck = "Email";
			return strFieldNull + strCheck;
		} else if (!Helpers.isEmailValid(mEdtEmail.getText().toString())) {
			strCheck = "Invalid Email";
			return strCheck;
		} else if (mEdtPassword.getText().toString().equals("")) {
			strCheck = "Password";
			return strFieldNull + strCheck;
		} else if (mEdtConfirmPassword.getText().toString().equals("")) {
			strCheck = "ConfirmPassword";
			return strFieldNull + strCheck;
		} else if (!mEdtPassword.getText().toString().equals(mEdtConfirmPassword.getText().toString())) {
			strCheck = "Password and confirm password not matched !";
			return strCheck;
		} else if (mTvBirthDay.getText().toString().equals("")) {
			strCheck = "birthday";
			return strFieldNull + strCheck;
		} else if (yearNow - yearAge < 18) {
			strCheck = "Not enough 18 years";
			return strCheck;
		} else if (mEdtTelephone.getText().toString().equals("")) {
			strCheck = "telephone number";
			return strFieldNull + strCheck;
		} else if (mTvGender.getText().toString().equals("")) {
			strCheck = "Gender";
			return strFieldNull + strCheck;
		} else if (!mCbAccept.isChecked()) {
			strCheck = "Please accept our term and conditions first !";
			return strCheck;
		}
		return "";
	}

	@Override
	public void onJobClick(String job, int id) {
		mJobDialog.dismiss();
		mTvJob.setText(job);
		mIdJob = id;
	}

	@OnActivityResult(SEARCH_LOCATION)
	void onLocationResult(int resultCode, Intent data) {
		if (resultCode == SEARCH_LOCATION) {
			mCity = data.getExtras().getString("city");
			mState = data.getExtras().getString("state");
			mCountry = data.getExtras().getString("country");
			Log.d("TAG", "onLocationResult: ");
			String address = mCity + ", " + mState + ", " + mCountry;
			mTvLocation.setText(address);
		}
	}

	@OnActivityResult(Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
	void onResult(int resultCode, Intent data) {
		Log.d("xxx", "da ve");
		if (resultCode == RESULT_OK) {
			previewCapturedImage();

		}
	}

	@OnActivityResult(Constants.RESULT_LOAD_IMAGE)
	void onGalleryResult(int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Uri selectedImage = data.getData();
			mIvAvatar.setImageURI(selectedImage);
		}
	}

	// how to rotaition thumbnail on picasso
	private void previewCapturedImage() {
		Log.d("xxx", "previewCapturedImage: ");

//        // bimatp factory
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 8;
		Bitmap bitmap = BitmapFactory.decodeFile(mImageUri.getPath(), options);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        mIvAvatar.setImageBitmap(bitmap);
		Matrix matrix = new Matrix();
		matrix.postRotate(getImageOrientation(mImageUri.getPath()));
		Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);
		rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
		byte[] byteArray = byteArrayOutputStream.toByteArray();
		mAvatar = Base64.encodeToString(byteArray, Base64.DEFAULT);
		mIvAvatar.setImageBitmap(rotatedBitmap);
	}

	/**
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		mImageUri = getOutputMediaFileUri(Constants.MEDIA_TYPE_IMAGE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
		// start the image capture Intent
		Log.d("xxx", "captureImage: ");
		startActivityForResult(intent, Constants.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	private void galleryCaptureImage() {
		Intent intent = new Intent(Intent.ACTION_PICK,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		try {

			startActivityForResult(intent, RESULT_LOAD_IMAGE);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	@Override
	public void onDismissCameraListener() {
		mChooseCameraDialog.dismiss();
		captureImage();
	}

	@Override
	public void onDismissGalleryListener() {
		mChooseCameraDialog.dismiss();
		galleryCaptureImage();
	}


	@Override
	public void onGenderClick(String gender) {
		mDialogGender.dismiss();
		mTvGender.setText(gender);
	}

	private void onRegister() {
		Log.d("xxx", "onRegister: " + mCity + mState + mCountry);
		Bitmap bitmap = ((BitmapDrawable) mIvAvatar.getDrawable()).getBitmap();
		mAvatar = encodeToBase64(bitmap, Bitmap.CompressFormat.PNG, 90);
		try {
			RetrofitUtils.buildApiInterface(ApiConnect.class).getUseProfile("Android", SharedPreferences.getDeviceToken(this),
					SharedPreferences.getDeviceId(this), mEdtUserName.getText().toString().trim(),
					mEdtRealName.getText().toString().trim(), mEdtSurName.getText().toString().trim(),
					mEdtEmail.getText().toString().trim(), mEdtPassword.getText().toString().trim(),
					month, day, yearNow, mCity, mState, mCountry, longitude,
					latitude, mTvGender.getText().toString().trim(), mEdtTelephone.getText().toString(), mAvatar, mIdJob,
					new Callback<UserProfile>() {
						@Override
						public void success(final UserProfile userProfile, Response response) {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Log.d("xxx", "success: " + userProfile.getmName());
									SharedPreferences.saveInt(RegisterActivity.this, TYPE_LOGIN, 0);
									Toast.makeText(RegisterActivity.this, "rule: " + userProfile.getmRule(), Toast.LENGTH_SHORT).show();
									SharedPreferences.clear(RegisterActivity.this);
									SharedPreferences.saveUserProfile(RegisterActivity.this, userProfile, KEY_USER_PROFILE);
									MainActivity_.intent(RegisterActivity.this).start();
									finish();
								}
							});
						}

						@Override
						public void failure(final RetrofitError error) {
							Log.d("xxx", "failure: " + error);
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(RegisterActivity.this, "" + error.toString(), Toast.LENGTH_SHORT).show();
								}
							});
						}
					});
		} catch (Exception e) {
			Log.w("xxx", "onRegister: " + e.getMessage());
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mImageUri != null) {
			outState.putString("cameraImageUri", mImageUri.toString());
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.containsKey("cameraImageUri")) {
			mImageUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
		}
	}
}
