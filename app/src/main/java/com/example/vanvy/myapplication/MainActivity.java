package com.example.vanvy.myapplication;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanvy.myapplication.api.ApiConnect;
import com.example.vanvy.myapplication.base.BaseActivity;
import com.example.vanvy.myapplication.base.BaseFragment;
import com.example.vanvy.myapplication.checkout.DialogCheckoutFragment_;
import com.example.vanvy.myapplication.common.Constants;
import com.example.vanvy.myapplication.customView.CustomMainTab;
import com.example.vanvy.myapplication.eventBus.BusProvider;
import com.example.vanvy.myapplication.eventBus.SettingEvent;
import com.example.vanvy.myapplication.eventBus.SettingStaffEvent;
import com.example.vanvy.myapplication.object.Checkout;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.object.Logout;
import com.example.vanvy.myapplication.object.UserClub;
import com.example.vanvy.myapplication.object.UserProfile;
import com.example.vanvy.myapplication.util.ApiUtils;
import com.example.vanvy.myapplication.util.CustomViewPager;
import com.example.vanvy.myapplication.util.LocationHelper;
import com.example.vanvy.myapplication.util.RetrofitUtils;
import com.example.vanvy.myapplication.util.SharedPreferences;
import com.example.vanvy.myapplication.widget.dashboard.DashboardClubFragment_;
import com.example.vanvy.myapplication.widget.dashboard.DashboardFragment_;
import com.example.vanvy.myapplication.widget.follower.FollowerFragment_;
import com.example.vanvy.myapplication.widget.message.MessageFragment_;
import com.example.vanvy.myapplication.widget.more.MoreFragment;
import com.example.vanvy.myapplication.widget.more.MoreFragment_;
import com.example.vanvy.myapplication.widget.more.gallery.MediaFragment;
import com.example.vanvy.myapplication.widget.more.setting.PerformingPlaceActivity_;
import com.example.vanvy.myapplication.widget.notification.NotificationFragment_;
import com.example.vanvy.myapplication.widget.qr.QrCodeFragment_;
import com.example.vanvy.myapplication.widget.ticket.TicketFragment_;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.example.vanvy.myapplication.common.Constants.RESULT_CODE_SETTING;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements Constants, CustomMainTab.OnItemTabMainClickListener,
        MoreFragment.ItemMoreListener, MoreFragment.ClubSettingListener {

    @ViewById()
    CustomViewPager mViewPageMain;
    @ViewById()
    CustomMainTab mTabStripMain;
    @ViewById()
    FrameLayout mFrMainContainer;
    @ViewById
    TextView mTvRight;
    private boolean isRule;
    private LocationHelper locationHelper;
    private double latitude;
    private double longitude;
    private ArrayList<Gift> mGifts = new ArrayList<>();
    private int uId;
    private MoreFragment moreFragment;
    private boolean isSave = false;
    private UserClub mClub = new UserClub();
    private int mType;

    @AfterViews()
    void afterViews() {
        if (SharedPreferences.getTypeLogin(this, TYPE_LOGIN) == 0) {
            uId = SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId();
            mType = 0;
        } else {
            uId = SharedPreferences.getClubProfile(this, KEY_CLUB_PROFILE).getmId();
            mType = 1;
        }
        Log.d("xxx", "afterViews: " + uId);
        getCheckout();
        getListGift();
        getLocation();
        setTitle("Dashboard", false, false, false);
        mTabStripMain.setItemClickListener(this);
        setUpTab(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmRule());
        selectedTab(0);
    }

    @Click(R.id.mTvRight)
    void onSaveClick() {
        isSave = true;
        BusProvider.getInstance().post(settingStaffEvent());
    }

    public void getListGift() {
        RetrofitUtils.buildApiInterface(ApiConnect.class).getDataGift(new Callback<List<Gift>>() {
            @Override
            public void success(final List<Gift> gifts, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mGifts.addAll(gifts);
                        Log.d("xxxMain2", "getDataGift: " + gifts.size());
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Get gift error: " + RetrofitUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCheckout() {
        RetrofitUtils.buildApiInterface(ApiConnect.class).getCheckout(SharedPreferences.getUserProfile(this, KEY_USER_PROFILE).getmId(),
                new Callback<List<Checkout>>() {
                    @Override
                    public void success(List<Checkout> checkout, Response response) {
                        if (checkout.size() > 0) {
                            DialogCheckoutFragment_.builder().checkout(checkout.get(0)).build().show(getSupportFragmentManager(), "");
                        } else {
                            Log.d("xxx", "not data: ");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("xxx", "failure: " + error.toString());
                    }
                });
    }

    private void getLocation() {
        locationHelper = new LocationHelper(this);
        locationHelper.addRequestListener(new LocationHelper.OnLocationUpdate() {
            @Override
            public void onLocationUpdated(final Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.d("xxxUser2", "lat" + latitude + "-" + longitude);
                if (latitude > 0 && longitude > 0) {
                    SharedPreferences.saveDouble(MainActivity.this, Constants.LNG, longitude);
                    SharedPreferences.saveDouble(MainActivity.this, Constants.LAT, latitude);
                    updateLocation(String.valueOf(uId));
                } else {
                    Log.d("xxx", "Location not Update");
                }
            }
        });
    }

    private void updateLocation(String uID) {
        RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(ApiUtils.HOST).build();
        ApiConnect mApi = restAdapter.create(ApiConnect.class);
        mApi.updateLocation(latitude, longitude, uID, new Callback<Logout>() {
            @Override
            public void success(final Logout logout, Response response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (logout.getmStatus() == 200) {
                            Log.d("xxxLocation", "Success");
                        }
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("xxxLocation", "error");
            }
        });
    }

    private void setUpTab(int rule) {
        switch (rule) {
            case DANCER:
            case OTHER:
            case DRIVER:
            case DJ:
            case WAITER:
            case PROMOTER:
            case BARTENDER:
                isRule = true;
                mTabStripMain.setupTab(true, 0);
                break;
            case MANAGER:
            case SECURITY:
                isRule = false;
                mTabStripMain.setupTab(false, 1);
                break;
            default:
                mTabStripMain.setupTab(false, 1);
                break;
        }
    }

    public void selectedTab(int position) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
//		fm.popBackStack();
        BaseFragment f = null;
        switch (position) {
            case 0:
                if (SharedPreferences.getTypeLogin(this, TYPE_LOGIN) == 0) {
                    f = DashboardFragment_.builder().build();
                } else {
                    f = DashboardClubFragment_.builder().build();
                }
                break;
            case 1:
                setTitle("Follower", false, false, false);
                f = FollowerFragment_.builder().mGifts1(mGifts).build();
                break;
            case 2:
                if (isRule && mType == 0) {
                    setTitle("E-Ticket", false, false, false);
                    f = TicketFragment_.builder().build();
                } else {
                    setTitle("Scan QR Code", false, false, false);
                    f = QrCodeFragment_.builder().build();
                }
                break;
            case 3:
                setTitle("Notification", false, false, false);
                f = NotificationFragment_.builder().build();
                break;
            case 4:
                setTitle("Message", false, false, false);
                f = MessageFragment_.builder().build();
                break;
            case 5:
                setTitle("More", false, false, false);
                moreFragment = MoreFragment_.builder().mGifts1(mGifts).build();
                moreFragment.setupListener(this);
                moreFragment.setupClubListener(this);
                f = moreFragment;
                break;
        }
        ft.add(R.id.mFrMainContainer, f, "setting_content");
        ft.addToBackStack("setting_fragment");
        ft.commit();
        mTabStripMain.onItemClickEvent(position);
        mViewPageMain.setCurrentItem(position, false);
    }

    @Override
    public void ClickItemTabHost(int position) {
        selectedTab(position);
    }

    @OnActivityResult(REQUEST_CODE_MORE)
    void onResultCode() {
        selectedTab(5);
    }

    @Override
    public void itemClickListener() {
        String url = "https://flewer.site/terms/Flewertermsandconditions.htm";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register ourselves so that we can provide the initial value.
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Always unregister when an object no longer should be on the bus.
        BusProvider.getInstance().unregister(this);
    }

    @Produce
    public SettingStaffEvent settingStaffEvent() {
        return new SettingStaffEvent(isSave);
    }

    @Produce
    public SettingEvent settingEvent() {
        return new SettingEvent(mClub);
    }

    @Override
    public void clubClick() {
        PerformingPlaceActivity_.intent(this).keyRequestCode(11).startForResult(RESULT_CODE_SETTING);
    }

    @OnActivityResult(RESULT_CODE_SETTING)
    void onResultSetting(int resultCode, Intent data) {
        if (resultCode == 2) {
            mClub = data.getExtras().getParcelable(KEY_SETTING);
            if (mClub != null) {
                BusProvider.getInstance().post(settingEvent());
            }
        } else if (resultCode == 3) {
            selectedTab(0);
        }
    }
}
