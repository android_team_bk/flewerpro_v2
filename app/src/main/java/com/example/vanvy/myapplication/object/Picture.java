package com.example.vanvy.myapplication.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by asiantech on 9/29/16.
 */
public class Picture implements Parcelable {
	@SerializedName("id")
	private String mId;
	@SerializedName("user_id")
	private String mUserId;
	@SerializedName("photo")
	private String mPhoto;
	@SerializedName("description")
	private String mDescription;
	@SerializedName("approved")
	private String mApprove;
	@SerializedName("blocked")
	private String mBlocked;
	@SerializedName("thumb")
	private String mThumb;
	@SerializedName("profile")
	private String mProfile;
	@SerializedName("private")
	private String mPrivate;
	@SerializedName("deleted_at")
	private String mDeletedAt;
	@SerializedName("created_at")
	private String mCreatedAt;
	@SerializedName("updated_at")
	private String mUpdatedAt;
	@SerializedName("price")
	private String mPrice;
	@SerializedName("is_video")
	private String mIsVideo;
	@SerializedName("bought")
	private String mBought;

	public Picture() {
	}

	protected Picture(Parcel in) {
		mId = in.readString();
		mUserId = in.readString();
		mPhoto = in.readString();
		mDescription = in.readString();
		mApprove = in.readString();
		mBlocked = in.readString();
		mThumb = in.readString();
		mProfile = in.readString();
		mPrivate = in.readString();
		mDeletedAt = in.readString();
		mCreatedAt = in.readString();
		mUpdatedAt = in.readString();
		mPrice = in.readString();
		mIsVideo = in.readString();
		mBought = in.readString();
	}

	public static final Creator<Picture> CREATOR = new Creator<Picture>() {
		@Override
		public Picture createFromParcel(Parcel in) {
			return new Picture(in);
		}

		@Override
		public Picture[] newArray(int size) {
			return new Picture[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(mUserId);
		dest.writeString(mPhoto);
		dest.writeString(mDescription);
		dest.writeString(mApprove);
		dest.writeString(mBlocked);
		dest.writeString(mThumb);
		dest.writeString(mProfile);
		dest.writeString(mPrivate);
		dest.writeString(mDeletedAt);
		dest.writeString(mCreatedAt);
		dest.writeString(mUpdatedAt);
		dest.writeString(mPrice);
		dest.writeString(mIsVideo);
		dest.writeString(mBought);
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmUserId() {
		return mUserId;
	}

	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}

	public String getmPhoto() {
		return mPhoto;
	}

	public void setmPhoto(String mPhoto) {
		this.mPhoto = mPhoto;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public String getmApprove() {
		return mApprove;
	}

	public void setmApprove(String mApprove) {
		this.mApprove = mApprove;
	}

	public String getmBlocked() {
		return mBlocked;
	}

	public void setmBlocked(String mBlocked) {
		this.mBlocked = mBlocked;
	}

	public String getmThumb() {
		return mThumb;
	}

	public void setmThumb(String mThumb) {
		this.mThumb = mThumb;
	}

	public String getmProfile() {
		return mProfile;
	}

	public void setmProfile(String mProfile) {
		this.mProfile = mProfile;
	}

	public String getmPrivate() {
		return mPrivate;
	}

	public void setmPrivate(String mPrivate) {
		this.mPrivate = mPrivate;
	}

	public String getmDeletedAt() {
		return mDeletedAt;
	}

	public void setmDeletedAt(String mDeletedAt) {
		this.mDeletedAt = mDeletedAt;
	}

	public String getmCreatedAt() {
		return mCreatedAt;
	}

	public void setmCreatedAt(String mCreatedAt) {
		this.mCreatedAt = mCreatedAt;
	}

	public String getmUpdatedAt() {
		return mUpdatedAt;
	}

	public void setmUpdatedAt(String mUpdatedAt) {
		this.mUpdatedAt = mUpdatedAt;
	}

	public String getmPrice() {
		return mPrice;
	}

	public void setmPrice(String mPrice) {
		this.mPrice = mPrice;
	}

	public String getmIsVideo() {
		return mIsVideo;
	}

	public void setmIsVideo(String mIsVideo) {
		this.mIsVideo = mIsVideo;
	}

	public String getmBought() {
		return mBought;
	}

	public void setmBought(String mBought) {
		this.mBought = mBought;
	}
}
