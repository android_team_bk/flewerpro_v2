package com.example.vanvy.myapplication.widget.more.new_request;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseAdapter;
import com.example.vanvy.myapplication.object.Gift;
import com.example.vanvy.myapplication.util.ScreenSize;
import com.example.vanvy.myapplication.widget.more.adapter.Adapter;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by asiantech on 10/10/16.
 */
public class ItemGiftAdapter extends BaseAdapter {
	private List<Gift> mGifts;
	private Context mContext;
	private int mItemSize;
	protected ItemGiftAdapter(Context context, List<Gift> gifts) {
		super(context);
		mContext = context;
		mGifts = gifts;
		mItemSize = (ScreenSize.getScreenWidth(context) - ScreenSize.convertDPToPixels(context, 25)) / 10;
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewItemGiftRequestHolder(LayoutInflater.from(mContext).inflate(R.layout.item_gift_request, parent, false));
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		onBindGiftViewHolder((ViewItemGiftRequestHolder) holder, mGifts.get(position));
	}

	private void onBindGiftViewHolder(ViewItemGiftRequestHolder holder, Gift gift) {
		holder.mTvPrice.setText(gift.getmPrice());
		holder.mLlGift.setBackgroundResource(gift.isChoose() ? R.color.color_request : R.color.color_white);
		Picasso.with(getContext())
				.load(gift.getmUrl())
				.into(holder.mIvGift);
	}

	@Override
	public int getItemCount() {
		return mGifts.size();
	}

	class ViewItemGiftRequestHolder extends RecyclerView.ViewHolder {
		ImageView mIvGift;
		TextView mTvPrice;
		LinearLayout mLlGift;

		public ViewItemGiftRequestHolder(View itemView) {
			super(itemView);
			ViewGroup.LayoutParams params = itemView.getLayoutParams();
			params.width = mItemSize;
			mIvGift = (ImageView) itemView.findViewById(R.id.mIvGift);
			mTvPrice = (TextView) itemView.findViewById(R.id.mTvPrice);
			mLlGift = (LinearLayout) itemView.findViewById(R.id.mLlGift);
		}
	}
}
