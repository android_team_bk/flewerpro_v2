package com.example.vanvy.myapplication.widget.more.gallery;


import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.vanvy.myapplication.R;
import com.example.vanvy.myapplication.base.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vanvy on 4/19/2016.
 */
@EActivity(R.layout.activity_video_view)
public class VideoViewActivity extends BaseActivity {
    @ViewById()
    VideoView mVideoView;
    @Extra()
    String mPath;

    MediaController mediaController;
    private int position = 0;
    private ProgressDialog progressDialog;

    @AfterViews
    void afterViews() {
        progressDialog=new ProgressDialog(this);
        if (mediaController == null) {
            mediaController = new MediaController(VideoViewActivity.this);
        }
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            mVideoView.setMediaController(mediaController);
            mVideoView.setVideoURI(Uri.parse(mPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mVideoView.requestFocus();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressDialog.dismiss();
                mVideoView.seekTo(position);
                if (position == 0) {
                    mVideoView.start();
                } else {
                    mVideoView.pause();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        mVideoView.seekTo(position);
    }
}
